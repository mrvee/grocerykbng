package com.webkype.gogiagroceryking.model.api_models.products_response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductsResponse {

@SerializedName("status")
@Expose
private String status;
@SerializedName("msg")
@Expose
private String msg;
@SerializedName("catid")
@Expose
private String catid;
@SerializedName("categoryname")
@Expose
private String categoryname;
@SerializedName("catbanner")
@Expose
private String catbanner;
@SerializedName("productdetails")
@Expose
private List<Productdetail> productdetails = null;
@SerializedName("totalcartqty")
@Expose
private Object totalcartqty;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getMsg() {
return msg;
}

public void setMsg(String msg) {
this.msg = msg;
}

public String getCatid() {
return catid;
}

public void setCatid(String catid) {
this.catid = catid;
}

public String getCategoryname() {
return categoryname;
}

public void setCategoryname(String categoryname) {
this.categoryname = categoryname;
}

public String getCatbanner() {
return catbanner;
}

public void setCatbanner(String catbanner) {
this.catbanner = catbanner;
}

public List<Productdetail> getProductdetails() {
return productdetails;
}

public void setProductdetails(List<Productdetail> productdetails) {
this.productdetails = productdetails;
}

public Object getTotalcartqty() {
return totalcartqty;
}

public void setTotalcartqty(Object totalcartqty) {
this.totalcartqty = totalcartqty;
}

}