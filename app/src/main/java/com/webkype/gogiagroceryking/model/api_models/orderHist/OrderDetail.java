package com.webkype.gogiagroceryking.model.api_models.orderHist;

public class OrderDetail {

    private String orderid;
    private String ordertotal;
    private String shippingcost;
    private String quantity;
    private String orderdate;
    private String orderstatus;
    private String regwallet_discount;
    private String userwallet_discount;

    public String getOrderid() { return orderid; }

    public String getOrdertotal() { return ordertotal; }

    public String getShippingcost() { return shippingcost; }

    public String getQuantity() { return quantity; }

    public String getOrderdate() { return orderdate; }

    public String getOrderstatus() { return orderstatus; }

    public String getRegwallet_discount() { return regwallet_discount; }

    public String getUserwallet_discount() { return userwallet_discount; }
}
