package com.webkype.gogiagroceryking.model.api_models.cart_qntty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartQuantitysponse {

@SerializedName("status")
@Expose
private String status;
@SerializedName("totalcartqty")
@Expose
private String totalcartqty;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getTotalcartqty() {
return totalcartqty;
}

public void setTotalcartqty(String totalcartqty) {
this.totalcartqty = totalcartqty;
}

}