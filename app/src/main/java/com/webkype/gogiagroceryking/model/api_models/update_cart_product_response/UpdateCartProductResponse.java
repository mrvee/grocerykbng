package com.webkype.gogiagroceryking.model.api_models.update_cart_product_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateCartProductResponse {

@SerializedName("status")
@Expose
private String status;
@SerializedName("msg")
@Expose
private String msg;
@SerializedName("qty")
@Expose
private Integer qty;
@SerializedName("totalcartqty")
@Expose
private String totalcartqty;
@SerializedName("totalcartprice")
@Expose
private String totalcartprice;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getMsg() {
return msg;
}

public void setMsg(String msg) {
this.msg = msg;
}

public Integer getQty() {
return qty;
}

public void setQty(Integer qty) {
this.qty = qty;
}

public String getTotalcartqty() {
return totalcartqty;
}

public void setTotalcartqty(String totalcartqty) {
this.totalcartqty = totalcartqty;
}

public String getTotalcartprice() {
return totalcartprice;
}

public void setTotalcartprice(String totalcartprice) {
this.totalcartprice = totalcartprice;
}

}