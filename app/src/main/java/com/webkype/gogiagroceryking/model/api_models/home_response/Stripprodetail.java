package com.webkype.gogiagroceryking.model.api_models.home_response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stripprodetail {

@SerializedName("stripname")
@Expose
private String stripname;
@SerializedName("Productdetails")
@Expose
private List<Productdetail> productdetails = null;

public String getStripname() {
return stripname;
}

public void setStripname(String stripname) {
this.stripname = stripname;
}

public List<Productdetail> getProductdetails() {
return productdetails;
}

public void setProductdetails(List<Productdetail> productdetails) {
this.productdetails = productdetails;
}

}