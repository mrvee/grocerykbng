package com.webkype.gogiagroceryking.model.api_models.orderReview;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderReview {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("bid")
    @Expose
    private String bid;
    @SerializedName("shippingtype")
    @Expose
    private List<Shippingtype> shippingtype = null;
    @SerializedName("shippingaddress")
    @Expose
    private List<ShippingaddressReview> shippingaddress = null;
    @SerializedName("totalqty")
    @Expose
    private String totalqty;
    @SerializedName("productdetails")
    @Expose
    private List<ProductdetailReview> productdetails = null;
    @SerializedName("subtotal")
    @Expose
    private String subtotal;
    @SerializedName("shippingprice")
    @Expose
    private String shippingprice;
    @SerializedName("reg_wallet_discount")
    @Expose
    private String regWalletDiscount;
    @SerializedName("user_wallet")
    @Expose
    private String userWallet;
    @SerializedName("coupondiscount")
    @Expose
    private String coupondiscount;
    @SerializedName("totalcost")
    @Expose
    private String totalcost;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public List<Shippingtype> getShippingtype() {
        return shippingtype;
    }

    public void setShippingtype(List<Shippingtype> shippingtype) {
        this.shippingtype = shippingtype;
    }

    public List<ShippingaddressReview> getShippingaddress() {
        return shippingaddress;
    }

    public void setShippingaddress(List<ShippingaddressReview> shippingaddress) {
        this.shippingaddress = shippingaddress;
    }

    public String getTotalqty() {
        return totalqty;
    }

    public void setTotalqty(String totalqty) {
        this.totalqty = totalqty;
    }

    public List<ProductdetailReview> getProductdetails() {
        return productdetails;
    }

    public void setProductdetails(List<ProductdetailReview> productdetails) {
        this.productdetails = productdetails;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getShippingprice() {
        return shippingprice;
    }

    public void setShippingprice(String shippingprice) {
        this.shippingprice = shippingprice;
    }

    public String getRegWalletDiscount() {
        return regWalletDiscount;
    }

    public void setRegWalletDiscount(String regWalletDiscount) {
        this.regWalletDiscount = regWalletDiscount;
    }

    public String getUserWallet() {
        return userWallet;
    }

    public void setUserWallet(String userWallet) {
        this.userWallet = userWallet;
    }

    public String getCoupondiscount() {
        return coupondiscount;
    }

    public void setCoupondiscount(String coupondiscount) {
        this.coupondiscount = coupondiscount;
    }

    public String getTotalcost() {
        return totalcost;
    }

    public void setTotalcost(String totalcost) {
        this.totalcost = totalcost;
    }
}
