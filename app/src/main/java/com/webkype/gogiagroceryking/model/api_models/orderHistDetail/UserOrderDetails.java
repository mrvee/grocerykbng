package com.webkype.gogiagroceryking.model.api_models.orderHistDetail;

import com.webkype.gogiagroceryking.model.api_models.orderReview.ProductdetailReview;

import java.util.List;

public class UserOrderDetails {

    private String orderid;
    private String ordertotal;
    private String shippingcost;
    private String quantity;
    private String orderdate;
    private String orderstatus;
    private String regwallet_discount;
    private String userwallet_discount;
    private String emailid;
    private String sfname;
    private String slname;
    private String saddress;
    private String slankmark;
    private String scity;
    private String sstate;
    private String spincode;
    private String sphone;
    private List<ProductdetailReview> prodetails;

    public String getOrderid() { return orderid; }

    public String getOrdertotal() { return ordertotal; }

    public String getShippingcost() { return shippingcost; }

    public String getQuantity() { return quantity; }

    public String getOrderdate() { return orderdate; }

    public String getOrderstatus() { return orderstatus; }

    public String getRegwallet_discount() { return regwallet_discount; }

    public String getUserwallet_discount() { return userwallet_discount; }

    public String getEmailid() { return emailid; }

    public String getSfname() { return sfname; }

    public String getSlname() { return slname; }

    public String getSaddress() { return saddress; }

    public String getSlankmark() { return slankmark; }

    public String getScity() { return scity; }

    public String getSstate() { return sstate; }

    public String getSpincode() { return spincode; }

    public String getSphone() { return sphone; }

    public List<ProductdetailReview> getProdetails() { return prodetails; }

}
