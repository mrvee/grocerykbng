package com.webkype.gogiagroceryking.model.api_models.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserAddress {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("shippingaddress")
    @Expose
    private List<Shippingaddress> shippingaddress = null;
    @SerializedName("billingaddress")
    @Expose
    private List<Billingaddress> billingaddress = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Shippingaddress> getShippingaddress() {
        return shippingaddress;
    }

    public void setShippingaddress(List<Shippingaddress> shippingaddress) {
        this.shippingaddress = shippingaddress;
    }

    public List<Billingaddress> getBillingaddress() {
        return billingaddress;
    }

    public void setBillingaddress(List<Billingaddress> billingaddress) {
        this.billingaddress = billingaddress;
    }
}
