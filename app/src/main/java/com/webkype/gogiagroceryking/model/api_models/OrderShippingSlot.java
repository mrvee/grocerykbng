package com.webkype.gogiagroceryking.model.api_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderShippingSlot {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("bid")
    @Expose
    private String bid;
    @SerializedName("totalqty")
    @Expose
    private String totalqty;
    @SerializedName("subtotal")
    @Expose
    private String subtotal;
    @SerializedName("shippingprice")
    @Expose
    private String shippingprice;
    @SerializedName("reg_wallet_discount")
    @Expose
    private String regWalletDiscount;
    @SerializedName("user_wallet")
    @Expose
    private String userWallet;
    @SerializedName("coupondiscount")
    @Expose
    private String coupondiscount;
    @SerializedName("totalcost")
    @Expose
    private String totalcost;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getTotalqty() {
        return totalqty;
    }

    public void setTotalqty(String totalqty) {
        this.totalqty = totalqty;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getShippingprice() {
        return shippingprice;
    }

    public void setShippingprice(String shippingprice) {
        this.shippingprice = shippingprice;
    }

    public String getRegWalletDiscount() {
        return regWalletDiscount;
    }

    public void setRegWalletDiscount(String regWalletDiscount) {
        this.regWalletDiscount = regWalletDiscount;
    }

    public String getUserWallet() {
        return userWallet;
    }

    public void setUserWallet(String userWallet) {
        this.userWallet = userWallet;
    }

    public String getCoupondiscount() {
        return coupondiscount;
    }

    public void setCoupondiscount(String coupondiscount) {
        this.coupondiscount = coupondiscount;
    }

    public String getTotalcost() {
        return totalcost;
    }

    public void setTotalcost(String totalcost) {
        this.totalcost = totalcost;
    }
}
