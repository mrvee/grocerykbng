package com.webkype.gogiagroceryking.model.api_models;

public class EditProfile {

    private String status;
    private String msg;
    private String user_id;
    private String fname;
    private String lname;
    private String email;
    private String mobile;
    private String wallet;
    private String regwallet;
    private String referal_code;

    public String getStatus() { return status; }

    public String getMsg() { return msg; }

    public String getUser_id() { return user_id; }

    public String getFname() { return fname; }

    public String getLname() { return lname; }

    public String getEmail() { return email; }

    public String getMobile() { return mobile; }

    public String getWallet() { return wallet; }

    public String getRegwallet() { return regwallet; }

    public String getReferal_code() { return referal_code; }
}
