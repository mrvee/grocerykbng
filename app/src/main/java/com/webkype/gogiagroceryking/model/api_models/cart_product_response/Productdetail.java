package com.webkype.gogiagroceryking.model.api_models.cart_product_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Productdetail {

@SerializedName("productname")
@Expose
private String productname;
@SerializedName("sellingprice")
@Expose
private String sellingprice;
@SerializedName("costprice")
@Expose
private String costprice;
@SerializedName("discount")
@Expose
private String discount;
@SerializedName("imageurl")
@Expose
private String imageurl;
@SerializedName("pid")
@Expose
private String pid;
@SerializedName("qty")
@Expose
private String qty;
@SerializedName("size")
@Expose
private String size;

public String getProductname() {
return productname;
}

public void setProductname(String productname) {
this.productname = productname;
}

public String getSellingprice() {
return sellingprice;
}

public void setSellingprice(String sellingprice) {
this.sellingprice = sellingprice;
}

public String getCostprice() {
return costprice;
}

public void setCostprice(String costprice) {
this.costprice = costprice;
}

public String getDiscount() {
return discount;
}

public void setDiscount(String discount) {
this.discount = discount;
}

public String getImageurl() {
return imageurl;
}

public void setImageurl(String imageurl) {
this.imageurl = imageurl;
}

public String getPid() {
return pid;
}

public void setPid(String pid) {
this.pid = pid;
}

public String getQty() {
return qty;
}

public void setQty(String qty) {
this.qty = qty;
}

public String getSize() {
return size;
}

public void setSize(String size) {
this.size = size;
}

}