package com.webkype.gogiagroceryking.model.api_models.city;

import java.util.List;

public class CityApi {

    private String status;
    private String msg;
    private List<CityModel> cityName;

    public String getStatus() { return status; }

    public String getMsg() { return msg; }

    public List<CityModel> getCityName() { return cityName; }
}
