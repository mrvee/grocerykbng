package com.webkype.gogiagroceryking.model.api_models.single_product_detail_resp;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

@SerializedName("id")
@Expose
private String id;
@SerializedName("productname")
@Expose
private String productname;
@SerializedName("costprice")
@Expose
private String costprice;
@SerializedName("sellingprice")
@Expose
private String sellingprice;
@SerializedName("discount")
@Expose
private String discount;
@SerializedName("cashback")
@Expose
private String cashback;
@SerializedName("totalrating")
@Expose
private String totalrating;
@SerializedName("qty")
@Expose
private String qty;
@SerializedName("slug")
@Expose
private String slug;
@SerializedName("longdescription")
@Expose
private String longdescription;
@SerializedName("cartqty")
@Expose
private String cartqty;
@SerializedName("imagepath")
@Expose
private String imagepath;
@SerializedName("variantdetails")
@Expose
private List<Variantdetail> variantdetails = null;
@SerializedName("shortdescription")
@Expose
private String shortdescription;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getProductname() {
return productname;
}

public void setProductname(String productname) {
this.productname = productname;
}

public String getCostprice() {
return costprice;
}

public void setCostprice(String costprice) {
this.costprice = costprice;
}

public String getSellingprice() {
return sellingprice;
}

public void setSellingprice(String sellingprice) {
this.sellingprice = sellingprice;
}

public String getDiscount() {
return discount;
}

public void setDiscount(String discount) {
this.discount = discount;
}

public String getCashback() {
return cashback;
}

public void setCashback(String cashback) {
this.cashback = cashback;
}

public String getTotalrating() {
return totalrating;
}

public void setTotalrating(String totalrating) {
this.totalrating = totalrating;
}

public String getQty() {
return qty;
}

public void setQty(String qty) {
this.qty = qty;
}

public String getSlug() {
return slug;
}

public void setSlug(String slug) {
this.slug = slug;
}

public String getLongdescription() {
return longdescription;
}

public void setLongdescription(String longdescription) {
this.longdescription = longdescription;
}

public String getCartqty() {
return cartqty;
}

public void setCartqty(String cartqty) {
this.cartqty = cartqty;
}

public String getImagepath() {
return imagepath;
}

public void setImagepath(String imagepath) {
this.imagepath = imagepath;
}

public List<Variantdetail> getVariantdetails() {
return variantdetails;
}

public void setVariantdetails(List<Variantdetail> variantdetails) {
this.variantdetails = variantdetails;
}

public String getShortdescription() {
return shortdescription;
}

public void setShortdescription(String shortdescription) {
this.shortdescription = shortdescription;
}

}