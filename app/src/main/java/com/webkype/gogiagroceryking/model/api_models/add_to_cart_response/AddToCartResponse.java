package com.webkype.gogiagroceryking.model.api_models.add_to_cart_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddToCartResponse {

@SerializedName("status")
@Expose
private String status;
@SerializedName("msg")
@Expose
private String msg;
@SerializedName("bid")
@Expose
private Integer bid;
@SerializedName("totalcartqty")
@Expose
private String totalcartqty;
@SerializedName("cartqty")
@Expose
private String cartqty;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getMsg() {
return msg;
}

public void setMsg(String msg) {
this.msg = msg;
}

public Integer getBid() {
return bid;
}

public void setBid(Integer bid) {
this.bid = bid;
}

public String getTotalcartqty() {
return totalcartqty;
}

public void setTotalcartqty(String totalcartqty) {
this.totalcartqty = totalcartqty;
}

public String getCartqty() {
return cartqty;
}

public void setCartqty(String cartqty) {
this.cartqty = cartqty;
}

}