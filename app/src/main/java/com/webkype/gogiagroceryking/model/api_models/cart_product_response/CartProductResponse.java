package com.webkype.gogiagroceryking.model.api_models.cart_product_response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartProductResponse {

@SerializedName("status")
@Expose
private String status;
@SerializedName("msg")
@Expose
private String msg;
@SerializedName("bid")
@Expose
private String bid;
@SerializedName("totalqty")
@Expose
private Integer totalqty;
@SerializedName("productdetails")
@Expose
private List<Productdetail> productdetails = null;
@SerializedName("subtotal")
@Expose
private Integer subtotal;
@SerializedName("shippingprice")
@Expose
private Integer shippingprice;
@SerializedName("coupondiscount")
@Expose
private Integer coupondiscount;
@SerializedName("totalcost")
@Expose
private Integer totalcost;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getMsg() {
return msg;
}

public void setMsg(String msg) {
this.msg = msg;
}

public String getBid() {
return bid;
}

public void setBid(String bid) {
this.bid = bid;
}

public Integer getTotalqty() {
return totalqty;
}

public void setTotalqty(Integer totalqty) {
this.totalqty = totalqty;
}

public List<Productdetail> getProductdetails() {
return productdetails;
}

public void setProductdetails(List<Productdetail> productdetails) {
this.productdetails = productdetails;
}

public Integer getSubtotal() {
return subtotal;
}

public void setSubtotal(Integer subtotal) {
this.subtotal = subtotal;
}

public Integer getShippingprice() {
return shippingprice;
}

public void setShippingprice(Integer shippingprice) {
this.shippingprice = shippingprice;
}

public Integer getCoupondiscount() {
return coupondiscount;
}

public void setCoupondiscount(Integer coupondiscount) {
this.coupondiscount = coupondiscount;
}

public Integer getTotalcost() {
return totalcost;
}

public void setTotalcost(Integer totalcost) {
this.totalcost = totalcost;
}

}