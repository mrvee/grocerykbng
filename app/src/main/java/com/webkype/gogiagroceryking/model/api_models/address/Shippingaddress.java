package com.webkype.gogiagroceryking.model.api_models.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shippingaddress {

    @SerializedName("snickname")
    @Expose
    private String snickname;
    @SerializedName("sfname")
    @Expose
    private String sfname;
    @SerializedName("slname")
    @Expose
    private String slname;
    @SerializedName("saddress")
    @Expose
    private String saddress;
    @SerializedName("scity")
    @Expose
    private String scity;
    @SerializedName("sstate")
    @Expose
    private String sstate;
    @SerializedName("spincode")
    @Expose
    private String spincode;
    @SerializedName("smobile")
    @Expose
    private String smobile;
    @SerializedName("slandmark")
    @Expose
    private String slandmark;
    @SerializedName("sarea")
    @Expose
    private String sarea;

    public String getSnickname() {
        return snickname;
    }

    public void setSnickname(String snickname) {
        this.snickname = snickname;
    }

    public String getSfname() {
        return sfname;
    }

    public void setSfname(String sfname) {
        this.sfname = sfname;
    }

    public String getSlname() {
        return slname;
    }

    public void setSlname(String slname) {
        this.slname = slname;
    }

    public String getSaddress() {
        return saddress;
    }

    public void setSaddress(String saddress) {
        this.saddress = saddress;
    }

    public String getScity() {
        return scity;
    }

    public void setScity(String scity) {
        this.scity = scity;
    }

    public String getSstate() {
        return sstate;
    }

    public void setSstate(String sstate) {
        this.sstate = sstate;
    }

    public String getSpincode() {
        return spincode;
    }

    public void setSpincode(String spincode) {
        this.spincode = spincode;
    }

    public String getSmobile() {
        return smobile;
    }

    public void setSmobile(String smobile) {
        this.smobile = smobile;
    }

    public String getSlandmark() {
        return slandmark;
    }

    public void setSlandmark(String slandmark) {
        this.slandmark = slandmark;
    }

    public String getSarea() {
        return sarea;
    }

    public void setSarea(String sarea) {
        this.sarea = sarea;
    }
}
