package com.webkype.gogiagroceryking.model.api_models.category_response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryResponse {

@SerializedName("status")
@Expose
private String status;
@SerializedName("msg")
@Expose
private String msg;
@SerializedName("topcategory")
@Expose
private List<Topcategory> topcategory = null;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getMsg() {
return msg;
}

public void setMsg(String msg) {
this.msg = msg;
}

public List<Topcategory> getTopcategory() {
return topcategory;
}

public void setTopcategory(List<Topcategory> topcategory) {
this.topcategory = topcategory;
}

}