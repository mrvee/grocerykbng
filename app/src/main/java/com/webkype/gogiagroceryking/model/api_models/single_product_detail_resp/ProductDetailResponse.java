package com.webkype.gogiagroceryking.model.api_models.single_product_detail_resp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailResponse {

@SerializedName("msg")
@Expose
private String msg;
@SerializedName("product")
@Expose
private Product product;
@SerializedName("totalcartqty")
@Expose
private Object totalcartqty;

public String getMsg() {
return msg;
}

public void setMsg(String msg) {
this.msg = msg;
}

public Product getProduct() {
return product;
}

public void setProduct(Product product) {
this.product = product;
}

public Object getTotalcartqty() {
return totalcartqty;
}

public void setTotalcartqty(Object totalcartqty) {
this.totalcartqty = totalcartqty;
}

}