package com.webkype.gogiagroceryking.model.api_models.home_response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Productdetail {

@SerializedName("productname")
@Expose
private String productname;
@SerializedName("sellingprice")
@Expose
private String sellingprice;
@SerializedName("imageurl")
@Expose
private String imageurl;
@SerializedName("pid")
@Expose
private String pid;
@SerializedName("costprice")
@Expose
private String costprice;
@SerializedName("discount")
@Expose
private Double discount;
@SerializedName("cartqty")
@Expose
private String cartqty;
@SerializedName("variantdetails")
@Expose
private List<Variantdetail> variantdetails = null;
@SerializedName("variant_type")
@Expose
private String variantType;

public String getProductname() {
return productname;
}

public void setProductname(String productname) {
this.productname = productname;
}

public String getSellingprice() {
return sellingprice;
}

public void setSellingprice(String sellingprice) {
this.sellingprice = sellingprice;
}

public String getImageurl() {
return imageurl;
}

public void setImageurl(String imageurl) {
this.imageurl = imageurl;
}

public String getPid() {
return pid;
}

public void setPid(String pid) {
this.pid = pid;
}

public String getCostprice() {
return costprice;
}

public void setCostprice(String costprice) {
this.costprice = costprice;
}

public Double getDiscount() {
return discount;
}

public void setDiscount(Double discount) {
this.discount = discount;
}

public String getCartqty() {
return cartqty;
}

public void setCartqty(String cartqty) {
this.cartqty = cartqty;
}

public List<Variantdetail> getVariantdetails() {
return variantdetails;
}

public void setVariantdetails(List<Variantdetail> variantdetails) {
this.variantdetails = variantdetails;
}

public String getVariantType() {
return variantType;
}

public void setVariantType(String variantType) {
this.variantType = variantType;
}

}