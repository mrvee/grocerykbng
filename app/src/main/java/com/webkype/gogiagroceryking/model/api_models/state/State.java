package com.webkype.gogiagroceryking.model.api_models.state;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class State {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("statename")
    @Expose
    private List<Statename> statename = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Statename> getStatename() {
        return statename;
    }

    public void setStatename(List<Statename> statename) {
        this.statename = statename;
    }


}
