package com.webkype.gogiagroceryking.model.api_models.orderReview;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShippingaddressReview {

    @SerializedName("customername")
    @Expose
    private String customername;
    @SerializedName("customeraddress")
    @Expose
    private String customeraddress;
    @SerializedName("customermobile")
    @Expose
    private String customermobile;

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomeraddress() {
        return customeraddress;
    }

    public void setCustomeraddress(String customeraddress) {
        this.customeraddress = customeraddress;
    }

    public String getCustomermobile() {
        return customermobile;
    }

    public void setCustomermobile(String customermobile) {
        this.customermobile = customermobile;
    }
}
