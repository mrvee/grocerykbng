package com.webkype.gogiagroceryking.model.api_models.pinCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PinCode {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("pincode")
    @Expose
    private List<PincodeName> pincode = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<PincodeName> getPincode() {
        return pincode;
    }

    public void setPincode(List<PincodeName> pincode) {
        this.pincode = pincode;
    }
}
