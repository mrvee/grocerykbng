package com.webkype.gogiagroceryking.model.api_models.pinCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PincodeName {

    @SerializedName("pincode")
    @Expose
    private String pincode;

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    @Override
    public String toString() {
        return pincode;
    }
}
