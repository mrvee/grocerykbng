package com.webkype.gogiagroceryking.model.api_models.orderHist;

import java.util.List;

public class CustomerOrder {

    private String status;
    private String msg;
    private List<OrderDetail> orderdetails;

    public String getStatus() { return status; }

    public String getMsg() { return msg; }

    public List<OrderDetail> getOrderdetails() { return orderdetails; }
}
