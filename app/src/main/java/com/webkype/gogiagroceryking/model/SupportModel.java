package com.webkype.gogiagroceryking.model;

public class SupportModel {
    String id,question;

    public SupportModel() {
    }

    public SupportModel(String id, String question) {
        this.id = id;
        this.question = question;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
