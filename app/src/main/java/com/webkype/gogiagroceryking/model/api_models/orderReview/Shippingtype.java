package com.webkype.gogiagroceryking.model.api_models.orderReview;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shippingtype {

    @SerializedName("shipid")
    @Expose
    private String shipid;
    @SerializedName("shiptype")
    @Expose
    private String shiptype;
    @SerializedName("shipcheck")
    @Expose
    private String shipcheck;

    public String getShipid() {
        return shipid;
    }

    public void setShipid(String shipid) {
        this.shipid = shipid;
    }

    public String getShiptype() {
        return shiptype;
    }

    public void setShiptype(String shiptype) {
        this.shiptype = shiptype;
    }

    public String getShipcheck() {
        return shipcheck;
    }

    public void setShipcheck(String shipcheck) {
        this.shipcheck = shipcheck;
    }

}
