package com.webkype.gogiagroceryking.model.api_models.category_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Topcategory {

@SerializedName("catid")
@Expose
private String catid;
@SerializedName("categoryname")
@Expose
private String categoryname;
@SerializedName("categorydesc")
@Expose
private String categorydesc;
@SerializedName("imageurl")
@Expose
private String imageurl;

public String getCatid() {
return catid;
}

public void setCatid(String catid) {
this.catid = catid;
}

public String getCategoryname() {
return categoryname;
}

public void setCategoryname(String categoryname) {
this.categoryname = categoryname;
}

public String getCategorydesc() {
return categorydesc;
}

public void setCategorydesc(String categorydesc) {
this.categorydesc = categorydesc;
}

public String getImageurl() {
return imageurl;
}

public void setImageurl(String imageurl) {
this.imageurl = imageurl;
}

}