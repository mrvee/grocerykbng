package com.webkype.gogiagroceryking.model.api_models.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserWallet {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("wallet")
    @Expose
    private String wallet;
    @SerializedName("regwallet")
    @Expose
    private String regwallet;
    @SerializedName("walletdetails")
    @Expose
    private List<Walletdetail> walletdetails = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getRegwallet() {
        return regwallet;
    }

    public void setRegwallet(String regwallet) {
        this.regwallet = regwallet;
    }

    public List<Walletdetail> getWalletdetails() {
        return walletdetails;
    }

    public void setWalletdetails(List<Walletdetail> walletdetails) {
        this.walletdetails = walletdetails;
    }

}
