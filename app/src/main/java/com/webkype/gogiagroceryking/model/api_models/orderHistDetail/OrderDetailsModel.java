package com.webkype.gogiagroceryking.model.api_models.orderHistDetail;

import java.util.List;

public class OrderDetailsModel {

    private String status;
    private String msg;
    private List<UserOrderDetails> orderdetails;

    public String getStatus() { return status; }

    public String getMsg() { return msg; }

    public List<UserOrderDetails> getOrderdetails() { return orderdetails; }
}
