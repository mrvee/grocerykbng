package com.webkype.gogiagroceryking.model.api_models.home_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SecondBanner {

@SerializedName("imageurl")
@Expose
private String imageurl;
@SerializedName("externalLink")
@Expose
private String externalLink;

public String getImageurl() {
return imageurl;
}

public void setImageurl(String imageurl) {
this.imageurl = imageurl;
}

public String getExternalLink() {
return externalLink;
}

public void setExternalLink(String externalLink) {
this.externalLink = externalLink;
}

}