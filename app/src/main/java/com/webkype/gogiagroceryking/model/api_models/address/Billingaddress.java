package com.webkype.gogiagroceryking.model.api_models.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Billingaddress {

    @SerializedName("bnickname")
    @Expose
    private String bnickname;
    @SerializedName("bfname")
    @Expose
    private String bfname;
    @SerializedName("blname")
    @Expose
    private String blname;
    @SerializedName("baddress")
    @Expose
    private String baddress;
    @SerializedName("bcity")
    @Expose
    private String bcity;
    @SerializedName("bstate")
    @Expose
    private String bstate;
    @SerializedName("bpincode")
    @Expose
    private String bpincode;
    @SerializedName("bmobile")
    @Expose
    private String bmobile;
    @SerializedName("blandmark")
    @Expose
    private String blandmark;
    @SerializedName("barea")
    @Expose
    private String barea;

    public String getBnickname() {
        return bnickname;
    }

    public void setBnickname(String bnickname) {
        this.bnickname = bnickname;
    }

    public String getBfname() {
        return bfname;
    }

    public void setBfname(String bfname) {
        this.bfname = bfname;
    }

    public String getBlname() {
        return blname;
    }

    public void setBlname(String blname) {
        this.blname = blname;
    }

    public String getBaddress() {
        return baddress;
    }

    public void setBaddress(String baddress) {
        this.baddress = baddress;
    }

    public String getBcity() {
        return bcity;
    }

    public void setBcity(String bcity) {
        this.bcity = bcity;
    }

    public String getBstate() {
        return bstate;
    }

    public void setBstate(String bstate) {
        this.bstate = bstate;
    }

    public String getBpincode() {
        return bpincode;
    }

    public void setBpincode(String bpincode) {
        this.bpincode = bpincode;
    }

    public String getBmobile() {
        return bmobile;
    }

    public void setBmobile(String bmobile) {
        this.bmobile = bmobile;
    }

    public String getBlandmark() {
        return blandmark;
    }

    public void setBlandmark(String blandmark) {
        this.blandmark = blandmark;
    }

    public String getBarea() {
        return barea;
    }

    public void setBarea(String barea) {
        this.barea = barea;
    }

}
