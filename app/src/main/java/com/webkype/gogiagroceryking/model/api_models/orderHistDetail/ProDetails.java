package com.webkype.gogiagroceryking.model.api_models.orderHistDetail;

public class ProDetails {

    private String productname;
    private String sellingprice;
    private String costprice;
    private String discount;
    private String imageurl;
    private String pid;
    private String qty;
    private String size;

    public String getProductname() { return productname; }

    public String getSellingprice() { return sellingprice; }

    public String getCostprice() { return costprice; }

    public String getDiscount() { return discount; }

    public String getImageurl() { return imageurl; }

    public String getPid() { return pid; }

    public String getQty() { return qty; }

    public String getSize() { return size; }
}
