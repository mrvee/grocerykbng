package com.webkype.gogiagroceryking.model.api_models.home_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Testimonial {

@SerializedName("imageurl")
@Expose
private String imageurl;
@SerializedName("city")
@Expose
private String city;
@SerializedName("description")
@Expose
private String description;
@SerializedName("clientName")
@Expose
private String clientName;

public String getImageurl() {
return imageurl;
}

public void setImageurl(String imageurl) {
this.imageurl = imageurl;
}

public String getCity() {
return city;
}

public void setCity(String city) {
this.city = city;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getClientName() {
return clientName;
}

public void setClientName(String clientName) {
this.clientName = clientName;
}

}
