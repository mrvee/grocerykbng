package com.webkype.gogiagroceryking.model.api_models.city;

public class CityModel {

    private String city;

    public String getCity() {  return city;  }

    @Override
    public String toString() {
        return city;
    }
}
