package com.webkype.gogiagroceryking.model.api_models.home_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("topbanners")
    @Expose
    private List<Banner> topbanners = null;
    @SerializedName("secondBanner")
    @Expose
    private List<Banner> secondBanner = null;
    @SerializedName("thirdBanner")
    @Expose
    private List<Banner> thirdBanner = null;
    @SerializedName("fourthBanner")
    @Expose
    private List<Banner> fourthBanner = null;
    @SerializedName("stripprodetails")
    @Expose
    private List<Stripprodetail> stripprodetails = null;

    @SerializedName("testimonial")
    @Expose
    private List<Testimonial> testimonial = null;

        public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Banner> getTopbanners() {
        return topbanners;
    }

    public void setTopbanners(List<Banner> topbanners) {
        this.topbanners = topbanners;
    }

    public List<Banner> getSecondBanner() {
        return secondBanner;
    }

    public void setSecondBanner(List<Banner> secondBanner) {
        this.secondBanner = secondBanner;
    }

    public List<Banner> getThirdBanner() {
        return thirdBanner;
    }

    public void setThirdBanner(List<Banner> thirdBanner) {
        this.thirdBanner = thirdBanner;
    }

    public List<Banner> getFourthBanner() {
        return fourthBanner;
    }

    public void setFourthBanner(List<Banner> fourthBanner) {
        this.fourthBanner = fourthBanner;
    }

    public List<Stripprodetail> getStripprodetails() {
        return stripprodetails;
    }

    public void setStripprodetails(List<Stripprodetail> stripprodetails) {
        this.stripprodetails = stripprodetails;
    }


    public List<Testimonial> getTestimonial() {
        return testimonial;
    }

    public void setTestimonial(List<Testimonial> testimonial) {
        this.testimonial = testimonial;
    }

}