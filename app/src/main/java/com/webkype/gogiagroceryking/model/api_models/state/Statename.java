package com.webkype.gogiagroceryking.model.api_models.state;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Statename {

    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("stateId")
    @Expose
    private String stateId;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    @Override
    public String toString() {
        return state;
    }
}
