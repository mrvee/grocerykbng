package com.webkype.gogiagroceryking.model.api_models.home_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Variantdetail {

@SerializedName("productname")
@Expose
private String productname;
@SerializedName("costprice")
@Expose
private String costprice;
@SerializedName("sellingprice")
@Expose
private String sellingprice;
@SerializedName("size")
@Expose
private String size;
@SerializedName("pid")
@Expose
private String pid;
@SerializedName("cartqty")
@Expose
private String cartqty;

public String getProductname() {
return productname;
}

public void setProductname(String productname) {
this.productname = productname;
}

public String getCostprice() {
return costprice;
}

public void setCostprice(String costprice) {
this.costprice = costprice;
}

public String getSellingprice() {
return sellingprice;
}

public void setSellingprice(String sellingprice) {
this.sellingprice = sellingprice;
}

public String getSize() {
return size;
}

public void setSize(String size) {
this.size = size;
}

public String getPid() {
return pid;
}

public void setPid(String pid) {
this.pid = pid;
}

public String getCartqty() {
return cartqty;
}

public void setCartqty(String cartqty) {
this.cartqty = cartqty;
}

    @Override
    public String toString() {
        return getSize();
    }
}