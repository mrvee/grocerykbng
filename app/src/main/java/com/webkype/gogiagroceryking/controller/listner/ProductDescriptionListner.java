package com.webkype.gogiagroceryking.controller.listner;



import java.util.List;

public interface ProductDescriptionListner {

    public void getTotalCartQty(String totalCartQty);
    public void getProductId(String productId);
    public void getProductImage(String productImage);
    public void getProductName(String productName);
    public void getCostPrice(String costPrice);
    public void getSellingPrice(String sellingPrice);
    public void getLongDesc(String longDesc);
    public void getCartQty(String cartQty);
//    public void getVariantDetails(List<VariantdetailDesc> variantdetailDescList);

    /*NOT REQ*/
    public void getSize(String size);
    public void getColor(String color);

//    public void getVariantDetailsColor(List<Variantdetailscolor> variantdetailscolorList);
//    public void getVariantDetailsSize(List<Variantdetailssize> variantdetailssizeList);

}
