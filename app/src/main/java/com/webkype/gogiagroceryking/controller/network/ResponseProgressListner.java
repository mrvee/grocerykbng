package com.webkype.gogiagroceryking.controller.network;

public interface ResponseProgressListner<T> {

        void onResponseInProgress();

        void onResponseCompleted(T response);

        void onResponseFailed(FailureCodes code);
}