package com.webkype.gogiagroceryking.controller.listner;


import com.webkype.gogiagroceryking.model.api_models.wallet.Walletdetail;

import java.util.List;

public interface UserWalletListner {

    public void getWallet(String wallet);
    public void getRegWallet(String regwallet);
    public void getWalletDetail(List<Walletdetail> walletdetailList);

}
