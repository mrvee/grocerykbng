package com.webkype.gogiagroceryking.controller.listner;

public interface UserAccountListner {

    public void getUserFName(String fname);
    public void getUserLName(String lname);
    public void getUserMail(String email);
    public void getUserMobile(String mobile);
    public void getWallet(String wallet);
    public void getReferCode(String referCode);
}
