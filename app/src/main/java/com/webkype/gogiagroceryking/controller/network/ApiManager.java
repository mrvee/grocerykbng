package com.webkype.gogiagroceryking.controller.network;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


import com.webkype.gogiagroceryking.controller.listner.UserAccountListner;
import com.webkype.gogiagroceryking.controller.listner.UserWalletListner;
import com.webkype.gogiagroceryking.model.api_models.SignUp;
import com.webkype.gogiagroceryking.model.api_models.UserAccount;
import com.webkype.gogiagroceryking.model.api_models.wallet.UserWallet;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiManager {

    public static void userSignUp(final Context context, String first_name, String last_name, String email, String mobile_no, String password,
                                  String referalcode, final ResponseProgressListner listner) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            Api api = RestManager.instanceOf();
            Call<SignUp> call = api.signUp(first_name, last_name, email, mobile_no, password, referalcode);
            listner.onResponseInProgress();
            call.enqueue(new Callback<SignUp>() {
                @Override
                public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                    SignUp signUp = response.body();
                    if (signUp.getStatus().equals("200")) {

                        listner.onResponseCompleted(1);
                    } else {
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL);
                        Toast.makeText(context, signUp.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SignUp> call, Throwable t) {
                    listner.onResponseFailed(FailureCodes.API_ERROR);
                    Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            listner.onResponseFailed(FailureCodes.NO_INTERNET);
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    /*Profile APIManager*/
    public static void getUserAccount(final Context context, String user_id, final ResponseProgressListner listner, final UserAccountListner userAccountListner) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            Api api = RestManager.instanceOf();
            Call<UserAccount> call = api.getUserAccount(user_id);
            listner.onResponseInProgress();
            call.enqueue(new Callback<UserAccount>() {
                @Override
                public void onResponse(Call<UserAccount> call, Response<UserAccount> response) {
                    UserAccount userAccount = response.body();
                    if (userAccount.getStatus().equals("200")) {
                        userAccountListner.getUserFName(userAccount.getFname());
                        userAccountListner.getUserLName(userAccount.getLname());
                        userAccountListner.getUserMail(userAccount.getEmail());
                        userAccountListner.getUserMobile(userAccount.getMobile());
                        userAccountListner.getWallet(userAccount.getWallet());
                        userAccountListner.getReferCode(userAccount.getReferalCode());
                        listner.onResponseCompleted(1);
                    } else {
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL);
                    }
                }

                @Override
                public void onFailure(Call<UserAccount> call, Throwable t) {
                    listner.onResponseFailed(FailureCodes.API_ERROR);
                    Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            listner.onResponseFailed(FailureCodes.NO_INTERNET);
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    /*Wallet APIManager*/
    public static void getWallet(final Context context, String user_id, final ResponseProgressListner listner, final UserWalletListner userWalletListner) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            Api api = RestManager.instanceOf();
            Call<UserWallet> call = api.getUserWallet(user_id);
            listner.onResponseInProgress();
            call.enqueue(new Callback<UserWallet>() {
                @Override
                public void onResponse(Call<UserWallet> call, Response<UserWallet> response) {
                    UserWallet userWallet = response.body();
                    if (userWallet.getStatus().equals("200")) {
                        userWalletListner.getWallet(userWallet.getWallet());
                        userWalletListner.getRegWallet(userWallet.getRegwallet());
                        userWalletListner.getWalletDetail(userWallet.getWalletdetails());
                        listner.onResponseCompleted(1);
                    } else {
                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL);
                    }
                }

                @Override
                public void onFailure(Call<UserWallet> call, Throwable t) {
                    listner.onResponseFailed(FailureCodes.API_ERROR);
                    Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            listner.onResponseFailed(FailureCodes.NO_INTERNET);
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


//    public static void getLoginWithEmailIdAndPassword(final Context context, String email, String password, final ResponseProgressListner listner, final LoginListner loginListner) {
//        if (InternetConnectionCheck.haveNetworkConnection(context)) {
//            Api api = RestManager.instanceOf();
//            Call<Object> call = api.getUserLogin(email, password);
//            listner.onResponseInProgress();
//            call.enqueue(new Callback<Object>() {
//                @Override
//                public void onResponse(Call<Object> call, retrofit2.Response<Object> response) {
////                    UserLogin userLogin = response.body();
////                    if (userLogin.getStatus().equals("200") && userLogin.getMsg().equals("Success")) {
////                        loginListner.getId(userLogin.getUser().getId());
////                        loginListner.getFirstName(userLogin.getUser().getFirstName());
////                        loginListner.getLastName(userLogin.getUser().getLastName());
////                        loginListner.getEmail(userLogin.getUser().getEmail());
////                        loginListner.getPhone(userLogin.getUser().getPhone());
////                        /* UserData.setId(context,userLogin.getUser().getId());*/
//                        listner.onResponseCompleted(1);
////                        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
////                    } else {
////                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL);
////                        Toast.makeText(context, userLogin.getMsg(), Toast.LENGTH_SHORT).show();
////                    }
//                }
//
//                @Override
//                public void onFailure(Call<Object> call, Throwable t) {
//                    listner.onResponseFailed(FailureCodes.API_ERROR);
//                }
//            });
//        } else {
//            listner.onResponseFailed(FailureCodes.NO_INTERNET);
//        }
//    }

//    public static final String TAG = "ApiManager";
//
//    public static void createUser(final Context context, Bundle bundle, String user_image, final ResponseProgressListner listner) {
//        if (InternetConnectionCheck.haveNetworkConnection(context)) {
//            Api api = RestManager.instanceOf();
//            Call<RegResponse> call = null;
//            call = api.userRegister(
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("fname", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("lname", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("email", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("mobile", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("password", ""))),
//                    (MultiPartHelperClass.getMultipartData(new File(user_image), "userLogo")));
//            listner.onResponseInProgress();
//
//            call.enqueue(new Callback<RegResponse>() {
//                @Override
//                public void onResponse(Call<RegResponse> call, Response<RegResponse> response) {
//                    RegResponse updateProfile = response.body();
//                    if (updateProfile.getStatus().equals("200")) {
//                        listner.onResponseCompleted(1);
//                        Log.d(TAG, "Successfully Created");
//                        Toast.makeText(context, "Successfully Created", Toast.LENGTH_SHORT).show();
//
//                    } else if (updateProfile.getStatus().equals("400")) {
//                        Toast.makeText(context, updateProfile.getMsg(), Toast.LENGTH_SHORT).show();
//                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL);
//                    } else {
//                        Toast.makeText(context, "Try Again !", Toast.LENGTH_SHORT).show();
//                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL);
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<RegResponse> call, Throwable t) {
//                    Toast.makeText(context, "Unable to reach servers. Try Again !", Toast.LENGTH_SHORT).show();
//                    listner.onResponseFailed(FailureCodes.API_ERROR);
//                }
//            });
//        } else {
//            Toast.makeText(context, "Please check your network connection...", Toast.LENGTH_SHORT).show();
//            listner.onResponseFailed(FailureCodes.NO_INTERNET);
//        }
//    }
//
//    public static void updateUser(final Context context, Bundle bundle, String user_image, final ResponseProgressListner listner) {
//        if (InternetConnectionCheck.haveNetworkConnection(context)) {
//            Api api = RestManager.instanceOf();
//            Call<UserProfileUpdateResponse> call = null;
//            call = api.updateUserInfo(
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("userId", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("fname", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("lname", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("address", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("mobile", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("email", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("state", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("city", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("pincode", ""))),
//                    (MultiPartHelperClass.getMultipartData(new File(user_image), "userLogo")));
//            listner.onResponseInProgress();
//
//            call.enqueue(new Callback<UserProfileUpdateResponse>() {
//                @Override
//                public void onResponse(Call<UserProfileUpdateResponse> call, Response<UserProfileUpdateResponse> response) {
//                    UserProfileUpdateResponse updateProfile = response.body();
//                    if (updateProfile.getStatus().equals("200")) {
//                        listner.onResponseCompleted(2);
//                        Log.d(TAG, "Your, profile updated successfully");
//                        Toast.makeText(context, "Your, profile updated successfully", Toast.LENGTH_SHORT).show();
//
//                    } else {
//                        Toast.makeText(context, "Try Again !", Toast.LENGTH_SHORT).show();
//                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL);
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<UserProfileUpdateResponse> call, Throwable t) {
//                    Toast.makeText(context, "Unable to reach servers. Try Again !", Toast.LENGTH_SHORT).show();
//                    listner.onResponseFailed(FailureCodes.API_ERROR);
//                }
//            });
//
//        }else {
//        Toast.makeText(context, "Please check your network connection...", Toast.LENGTH_SHORT).show();
//        listner.onResponseFailed(FailureCodes.NO_INTERNET);
//    }
//    }
//
//    public static void addNewVehicle(final Context context, Bundle bundle, String user_image, final ResponseProgressListner listner) {
//        if (InternetConnectionCheck.haveNetworkConnection(context)) {
//            Api api = RestManager.instanceOf();
//            Call<AddVehicleResponse> call = null;
//            call = api.addNewVehicle(
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("id", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("userId", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("brand",""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("model",""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("registerYear",""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("rgNo",""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("varient", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("state", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("kmCover",""))),
//                    (MultiPartHelperClass.getMultipartData(new File(user_image), "vehicleImage")));
//            listner.onResponseInProgress();
//
//            call.enqueue(new Callback<AddVehicleResponse>() {
//                @Override
//                public void onResponse(Call<AddVehicleResponse> call, Response<AddVehicleResponse> response) {
//                    AddVehicleResponse addVehicleResponse = response.body();
//                    if (addVehicleResponse.getStatus().equals("200")) {
//                        listner.onResponseCompleted(1);
//                        Log.d(TAG, ""+addVehicleResponse.getMsg());
//
//                        Toast.makeText(context, " Vehicle Added Successfully", Toast.LENGTH_SHORT).show();
//
//                    }else if (addVehicleResponse.getStatus().equals("500")){
//                        Toast.makeText(context, ""+addVehicleResponse.getMsg(), Toast.LENGTH_SHORT).show();
//                    }
//                    else {
//                        Toast.makeText(context, "Try Again !", Toast.LENGTH_SHORT).show();
//                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL);
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<AddVehicleResponse> call, Throwable t) {
//                    Toast.makeText(context, "Unable to reach servers. Try Again !", Toast.LENGTH_SHORT).show();
//                    listner.onResponseFailed(FailureCodes.API_ERROR);
//                }
//            });
//
//        }else {
//            Toast.makeText(context, "Please check your network connection...", Toast.LENGTH_SHORT).show();
//            listner.onResponseFailed(FailureCodes.NO_INTERNET);
//        }
//    }
//    public static void deleteVehical(final Context context, Bundle bundle, final ResponseProgressListner listner) {
//        if (InternetConnectionCheck.haveNetworkConnection(context)) {
//            Api api = RestManager.instanceOf();
//            Call<DeleteVehicleResponse> call = null;
//            call = api.deleteVehicle(
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("userId", ""))),
//                    (MultiPartHelperClass.getRequestBody(bundle.getString("id", ""))));
//            listner.onResponseInProgress();
//
//            call.enqueue(new Callback<DeleteVehicleResponse>() {
//                @Override
//                public void onResponse(Call<DeleteVehicleResponse> call, Response<DeleteVehicleResponse> response) {
//                    DeleteVehicleResponse addVehicleResponse = response.body();
//                    if (addVehicleResponse.getStatus().equals("200")) {
//                        listner.onResponseCompleted(2);
//                        Log.d(TAG, ""+addVehicleResponse.getMsg());
//                        Toast.makeText(context, "Vehicle successfully deleted", Toast.LENGTH_SHORT).show();
//
//                    }else if (addVehicleResponse.getStatus().equals("500")){
//                    }
//                    else {
//                        Toast.makeText(context, "Try Again !", Toast.LENGTH_SHORT).show();
//                        listner.onResponseFailed(FailureCodes.RESPONSE_NULL);
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<DeleteVehicleResponse> call, Throwable t) {
//                    Toast.makeText(context, "Unable to reach servers. Try Again !", Toast.LENGTH_SHORT).show();
//                    listner.onResponseFailed(FailureCodes.API_ERROR);
//                }
//            });
//
//        }else {
//            Toast.makeText(context, "Please check your network connection...", Toast.LENGTH_SHORT).show();
//            listner.onResponseFailed(FailureCodes.NO_INTERNET);
//        }
//    }


}