package com.webkype.gogiagroceryking.controller.listner;


import com.webkype.gogiagroceryking.model.api_models.category_response.Topcategory;

import java.util.List;

public interface ProductCategoryListner {

    public void getProductCategory(List<Topcategory> topcategoryList);
}
