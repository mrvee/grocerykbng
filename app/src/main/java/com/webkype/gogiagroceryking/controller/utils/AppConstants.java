package com.webkype.gogiagroceryking.controller.utils;

import java.util.HashMap;
import java.util.Map;

public class AppConstants {
    public static final String USER_ID="USER_ID";
    public static final String Cat_ID="CAT_ID";
    public static final String Cat_TYPE="CAT_TYPE";
    public static final String PID_ID="PID";
    public static final String CART_ID="CART_ID";
    public static final String ORDER_ID="ORDER_ID";
    public static final String CURRENT_ADDRESS="current_address";

    public static final String SHIPPING_TYPE="Shipping_type";
    public static final String ADD_WALLET_AMOUNT="add_wallet_amount";

    public static final String AppPrefRence="webkype.groceryking";
    public static final String IS_LAUNCHED_FIRST_TIME="islaunchedfirsttime";
    public static String PRODUCT_CATEGORY = "";

    public static String WALLET_ADD_MONEY = "https://www.gogiagroceryking.com/webservices/customerWalletRecharge.php";
    public static final String FAQ= "https://www.gogiagroceryking.com/faqpage.php";
    public static final String ORDER_PLACE = "https://www.gogiagroceryking.com/webservices/orderPlaceAPI";

    public static String lati;
    public static String longi;

    public static Map<String,String> cartProducts = new HashMap<>();

}
