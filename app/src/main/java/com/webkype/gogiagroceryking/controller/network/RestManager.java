package com.webkype.gogiagroceryking.controller.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestManager {
//    https://www.gogiagroceryking.com/webservices/
    public static final String baseUrl="https://www.gogiagroceryking.com/webservices/";
    private static Retrofit retrofit = null;

    public static Api instanceOf(){
        if (retrofit==null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(Api.class);
    }
}
