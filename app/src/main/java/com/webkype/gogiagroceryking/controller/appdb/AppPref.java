package com.webkype.gogiagroceryking.controller.appdb;

import android.content.Context;
import android.content.SharedPreferences;

import com.webkype.gogiagroceryking.controller.utils.AppConstants;

public class AppPref {
    static SharedPreferences sharedPref = null;

    public static AppPref instance(Context context) {
        if (sharedPref == null) {
            sharedPref = context.getSharedPreferences(AppConstants.AppPrefRence, Context.MODE_PRIVATE);
            return new AppPref();
        }
        else
        return new AppPref();
    }


    public String getValue(String key) {
        String range = sharedPref.getString(key, "");
        return range;
    }

    public void setValue(String key, String value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void setFlagValue( String value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Value", value);
        editor.apply();
    }

    public String getFlagValue() {
        return sharedPref.getString("Value", "Value");
    }
    public void clearAllPreference() {
        sharedPref.edit().clear().commit();
    }

}
