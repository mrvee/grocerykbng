package com.webkype.gogiagroceryking.controller.network;

import com.webkype.gogiagroceryking.model.api_models.EditProfile;
import com.webkype.gogiagroceryking.model.api_models.OrderShippingSlot;
import com.webkype.gogiagroceryking.model.api_models.SignUp;
import com.webkype.gogiagroceryking.model.api_models.UpdateAddress;
import com.webkype.gogiagroceryking.model.api_models.UserAccount;
import com.webkype.gogiagroceryking.model.api_models.add_to_cart_response.AddToCartResponse;
import com.webkype.gogiagroceryking.model.api_models.address.UserAddress;
import com.webkype.gogiagroceryking.model.api_models.area.Area;
import com.webkype.gogiagroceryking.model.api_models.cart_product_response.CartProductResponse;
import com.webkype.gogiagroceryking.model.api_models.cart_qntty.CartQuantitysponse;
import com.webkype.gogiagroceryking.model.api_models.category_response.CategoryResponse;
import com.webkype.gogiagroceryking.model.api_models.city.CityApi;
import com.webkype.gogiagroceryking.model.api_models.delete_cart_product_res.CartDeleteProductrsponse;
import com.webkype.gogiagroceryking.model.api_models.home_response.HomeResponse;
import com.webkype.gogiagroceryking.model.api_models.login.UserLogin;
import com.webkype.gogiagroceryking.model.api_models.orderHist.CustomerOrder;
import com.webkype.gogiagroceryking.model.api_models.orderHistDetail.OrderDetailsModel;
import com.webkype.gogiagroceryking.model.api_models.orderReview.OrderReview;
import com.webkype.gogiagroceryking.model.api_models.pinCode.PinCode;
import com.webkype.gogiagroceryking.model.api_models.products_response.ProductsResponse;
import com.webkype.gogiagroceryking.model.api_models.single_product_detail_resp.ProductDetailResponse;
import com.webkype.gogiagroceryking.model.api_models.state.State;
import com.webkype.gogiagroceryking.model.api_models.update_cart_product_response.UpdateCartProductResponse;
import com.webkype.gogiagroceryking.model.api_models.wallet.UserWallet;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Api {

    @FormUrlEncoded
    @POST("customerLogin.php/")
    Call<UserLogin> getLoginWithEmailIdAndPassword(@Field("email") String email,
                                                   @Field("password") String password);

    /*Change Password*/
    @FormUrlEncoded
    @POST("customerChangePassword.php/")
    Call<SignUp> changePassword(@Field("user_id") String user_id,
                                @Field("oldpassword") String oldpassword,
                                @Field("newpassword") String newpassword);

    /*SignUp*/
    @FormUrlEncoded
    @POST("customerRegistration.php/")
    Call<SignUp> signUp(@Field("first_name") String first_name,
                        @Field("last_name") String last_name,
                        @Field("email") String email,
                        @Field("mobile_no") String mobile_no,
                        @Field("password") String password,
                        @Field("referalcode") String referalcode);

    /*Edit Profile*/
    @FormUrlEncoded
    @POST("customerEdit.php/")
    Call<EditProfile> editProfile(@Field("user_id") String user_id,
                                  @Field("fname") String fname,
                                  @Field("lname") String lname);

    /*User Profile*/
    @FormUrlEncoded
    @POST("customerMyAccount.php/")
    Call<UserAccount> getUserAccount(@Field("user_id") String user_id);

    /*Forgot Password*/
    @FormUrlEncoded
    @POST("forgotPassword.php/")
    Call<SignUp> forgotPass(@Field("email") String email);

    /*User Wallet*/
    @FormUrlEncoded
    @POST("customerWallet.php/")
    Call<UserWallet> getUserWallet(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("homePageAPI.php/")
    Call<HomeResponse> getHomeResponse(@Field("homepage") String from,
                                       @Field("cartid") String cartid);

    @FormUrlEncoded
    @POST("getCategory.php/")
    Call<CategoryResponse> getCategory(@Field("cat_id") String cat_id);

    /*Products List Api*/
    @FormUrlEncoded
    @POST("productSearch.php/")
    Call<ProductsResponse> getProducts(@Field("catid") String catid,
                                       @Field("page") String page,
                                       @Field("cartid") String cartid,
                                       @Field("cityhome") String cityhome,
                                       @Field("ksearch") String ksearch);

    //*******//********//

    @FormUrlEncoded
    @POST("productDetails.php/")
    Call<ProductDetailResponse> getProductDetails(@Field("pid") String pid,
                                                  @Field("cartid") String cartid);

    @FormUrlEncoded
    @POST("addToCart.php/")
    Call<AddToCartResponse> addToCart(@Field("pid") String pid,
                                      @Field("cartid") String cartid,
                                      @Field("qty") String qty,
                                      @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("updateCartProduct.php/")
    Call<UpdateCartProductResponse> updateCartProduct(
            @Field("pid") String pid,
            @Field("cartid") String cartid,
            @Field("option") String option,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("updateCartProduct.php/")
    Call<UpdateCartProductResponse> updateCartProduct(@Field("pid") String pid,
                                                      @Field("cartid") String cartid,
                                                      @Field("option") String option);  //  option= add, remove;  add= for adding a product and remove for removing a product
    /*
//       option= add, remove;
//           add= for adding a product and remove for removing a product
    * */

    @FormUrlEncoded
    @POST("deleteCartProduct.php/")
    Call<CartDeleteProductrsponse> deleteCartProduct(@Field("pid") String pid,
                                                     @Field("cartid") String cartid);

    //    Get Total Cart Qty for Badge
    @FormUrlEncoded
    @POST("totalCartQty.php/")
    Call<CartQuantitysponse> getTotalQty(@Field("cartid") String cartid);

    /*View Cart and Review Cart*/
    @FormUrlEncoded
    @POST("cartProduct.php/")
    Call<CartProductResponse> getCart(@Field("cartid") String cartid);
    /*=========================*/

    /*Get state, city, area, pin, address*/
    @FormUrlEncoded
    @POST("getStateAPI.php/")
    Call<State> getState(@Field("state") String state);

    @FormUrlEncoded
    @POST("getCityAPI.php/")
    Call<CityApi> getCity(@Field("stateName") String stateName);

    @FormUrlEncoded
    @POST("getPincodeAPI.php/")
    Call<PinCode> getPinCode(@Field("cityname") String cityname);

    @FormUrlEncoded
    @POST("getAreaAPI.php/")
    Call<Area> getArea(@Field("pincode") String pincode);

    @FormUrlEncoded
    @POST("customerShippingBilling.php/")
    Call<UserAddress> getAddress(@Field("user_id") String user_id);


    /*Update Address*/
    @FormUrlEncoded
    @POST("updateShippingBilling.php/")
    Call<UpdateAddress> updateAddress(@Field("user_id") String user_id,
                                      @Field("snickname") String snickname,
                                      @Field("sfname") String sfname,
                                      @Field("slname") String slname,
                                      @Field("saddress") String saddress,
                                      @Field("scity") String scity,
                                      @Field("sstate") String sstate,
                                      @Field("spincode") String spincode,
                                      @Field("sarea") String sarea,
                                      @Field("smobile") String smobile,
                                      @Field("slandmark") String slandmark,
                                      @Field("bnickname") String bnickname,
                                      @Field("bfname") String bfname,
                                      @Field("blname") String blname,
                                      @Field("baddress") String baddress,
                                      @Field("bcity") String bcity,
                                      @Field("bstate") String bstate,
                                      @Field("bpincode") String bpincode,
                                      @Field("barea") String barea,
                                      @Field("bmobile") String bmobile,
                                      @Field("blandmark") String blandmark);

    @FormUrlEncoded
    @POST("orderShippingSlotAPI.php")
    Call<OrderShippingSlot> getShipCharges(@Field("user_id") String user_id,
                                           @Field("cartid") String cartid,
                                           @Field("shipping_type_id") String shipping_type_id);

    /*Order Review*/
    @FormUrlEncoded
    @POST("orderReviewAPI.php/")
    Call<OrderReview> getOrderReview(@Field("user_id") String user_id,
                                     @Field("cartid") String cartid,
                                     @Field("cdate") String cdate);

    /*Order History*/
    @FormUrlEncoded
    @POST("customerOrder.php/")
    Call<CustomerOrder> getOrderHist(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("customerOrderDetails.php/")
    Call<OrderDetailsModel> getOrderDetail(@Field("user_id") String user_id,
                                           @Field("order_id") String order_id);




}
