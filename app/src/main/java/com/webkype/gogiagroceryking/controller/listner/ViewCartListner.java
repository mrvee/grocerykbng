package com.webkype.gogiagroceryking.controller.listner;


import java.util.List;

public interface ViewCartListner {

    public void getTotalCartQty(String totalCartQty);
    public void getSubTotal(String subTotal);
    public void getShippingPrice(String shippingPrice);
    public void getCouponDiscount(String couponDiscount);
    public void getTotalCost(String totalCost);
//    public void getCartList(List<Productdetailcart> productdetailcartList);
}
