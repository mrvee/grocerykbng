package com.webkype.gogiagroceryking.controller.listner;

public interface LoginListner {

    public void getUid(String uid);
    public void getFname(String firstName);
    public void getLname(String lastName);
    public void getEmail(String email);
    public void getMobile(String mobile);
}
