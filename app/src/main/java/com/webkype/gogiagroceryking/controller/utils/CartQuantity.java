package com.webkype.gogiagroceryking.controller.utils;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.model.api_models.cart_qntty.CartQuantitysponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartQuantity {
    private static final String TAG = CartQuantity.class.getName();
    private static String cartId = "";

    public static void setQuantity(Context context, TextView textView) {
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        getCartQntty(context, textView, cartId);
    }

    private static void getCartQntty(Context context, TextView textView, String cartId) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            Call<CartQuantitysponse> ProductDetailResponseCall = RestManager.instanceOf().getTotalQty(cartId);
            ProductDetailResponseCall.enqueue(new Callback<CartQuantitysponse>() {
                @Override
                public void onResponse(Call<CartQuantitysponse> call, Response<CartQuantitysponse> response) {
                    if (response != null && response.body().getStatus().equals("200")) {
                        if (response.body().getTotalcartqty() != null) {
                            textView.setVisibility(View.VISIBLE);
                            textView.setText(response.body().getTotalcartqty());
                        } else {
                            textView.setText("0");
                            textView.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<CartQuantitysponse> call, Throwable t) {
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "No network", Toast.LENGTH_SHORT).show();
        }
    }
}
