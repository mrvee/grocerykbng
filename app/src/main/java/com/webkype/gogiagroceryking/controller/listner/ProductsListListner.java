package com.webkype.gogiagroceryking.controller.listner;


import java.util.List;

public interface ProductsListListner {

    public void getCategoryName(String categoryName);
    public void getTotalCartQty(String totalCartQty);
    // public void getCategoryId (String categoryId);
//    public void getProductList(List<Productdetail> productdetailList);
}
