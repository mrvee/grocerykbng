package com.webkype.gogiagroceryking.view.activity.new_design;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.Api;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.UpdateAddress;
import com.webkype.gogiagroceryking.model.api_models.address.Shippingaddress;
import com.webkype.gogiagroceryking.model.api_models.address.UserAddress;
import com.webkype.gogiagroceryking.model.api_models.city.CityApi;
import com.webkype.gogiagroceryking.model.api_models.city.CityModel;
import com.webkype.gogiagroceryking.model.api_models.pinCode.PinCode;
import com.webkype.gogiagroceryking.model.api_models.pinCode.PincodeName;
import com.webkype.gogiagroceryking.model.api_models.state.State;
import com.webkype.gogiagroceryking.model.api_models.state.Statename;
import com.webkype.gogiagroceryking.view.activity.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BillingActivity extends AppCompatActivity {

    @BindView(R.id.backBilling)
    RelativeLayout backBilling;
    @BindView(R.id.addressNameOptional_billing)
    EditText addressNameOptional_billing;
    @BindView(R.id.billingFName_tV)
    EditText billingFName_tV;
    @BindView(R.id.billingLName_tV)
    EditText billingLName_tV;
    @BindView(R.id.billingMobile_tV)
    EditText billingMobile_tV;
    @BindView(R.id.billingHouse_tV)
    EditText billingHouse_tV;


//    @BindView(R.id.billingState_tV)
//    EditText billingState_tV;
    @BindView(R.id.billingCity_tV)
    EditText billingCity_tV;
    @BindView(R.id.billingPinCode_tV)
    EditText billingPinCode_tV;
    @BindView(R.id.areaLayout_billing)
    RelativeLayout areaLayout_billing;
    @BindView(R.id.billingArea_spinner)
    AppCompatSpinner billingArea_spinner;
    @BindView(R.id.billingLandMark_tV)
    EditText billingLandMark_tV;
    @BindView(R.id.saveBilling_tV)
    TextView saveBilling_tV;

    @BindView(R.id.spinner_state)
    AppCompatSpinner spinner_state;
    @BindView(R.id.state_layout)
    RelativeLayout state_layout;

    @BindView(R.id.shippingCity_spinner)
    AppCompatSpinner shippingCity_spinner;
    @BindView(R.id.cityLayout)
    RelativeLayout cityLayout;

    @BindView(R.id.shippingPinCode_spinner)
    AppCompatSpinner shippingPinCode_spinner;
    @BindView(R.id.pincodeLayout)
    RelativeLayout pincodeLayout;


    private String stateName="", cityName, pinCodeName, areaName;
    private String addressNickName, firstName, lastName, mobile, houseAddress, landMark;
    private String addressNickNShipping, firstNameShipping, lastNameShipping, mobileShipping, houseAddressShipping,
            stateNameShipping, cityNameShipping,pinShipping, landMarkShipping;
    private String userID = "", cartID = "";
    private Context context;
    
    private List<String> stateList = new ArrayList<>();
    private List<String> cityList = new ArrayList<>();
    private List<String> pincodeList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);
        ButterKnife.bind(this);
        context = this;

        userID = AppPref.instance(context).getValue(AppConstants.USER_ID);
        cartID = AppPref.instance(context).getValue(AppConstants.CART_ID);
        
        getUserAddress();

        Intent i = getIntent();
        addressNickNShipping = i.getStringExtra("addressNickName");
        firstNameShipping = i.getStringExtra("firstName");
        lastNameShipping = i.getStringExtra("lastName");
        mobileShipping = i.getStringExtra("mobile");
        houseAddressShipping = i.getStringExtra("houseAddress");
        stateNameShipping = i.getStringExtra("stateName");
        cityNameShipping = i.getStringExtra("cityName");
        pinShipping = i.getStringExtra("pin");
        //areaShipping = i.getStringExtra("area");
        landMarkShipping = i.getStringExtra("landMark");

        saveBilling_tV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getBillingData();
            }
        });


        backBilling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }


    private void getUserAddress(){
        Api api = RestManager.instanceOf();
        Call<UserAddress> call = api.getAddress(userID);
        call.enqueue(new Callback<UserAddress>() {
            @Override
            public void onResponse(Call<UserAddress> call, Response<UserAddress> response) {
                UserAddress userAddress = response.body();
                if (userAddress.getStatus().equals("200")) {
                    for (Shippingaddress shippingAdd : userAddress.getShippingaddress()) {
                        addressNameOptional_billing.setText(shippingAdd.getSnickname());
                        billingFName_tV.setText(shippingAdd.getSfname());
                        billingLName_tV.setText(shippingAdd.getSlname());
                        billingMobile_tV.setText(shippingAdd.getSmobile());
                        billingHouse_tV.setText(shippingAdd.getSaddress());
                        billingLandMark_tV.setText(shippingAdd.getSlandmark());
                        billingPinCode_tV.setText(shippingAdd.getSpincode());
                        billingCity_tV.setText(shippingAdd.getScity());
//                        billingState_tV.setText(shippingAdd.getSstate());
//                        for (String address : stateList) {
//                            if (address.equals(shippingAdd.getSstate())) {
//                                int index = stateList.indexOf(shippingAdd.getSstate());
//                                spinner_state.setSelection(index);
//                            }
//                        }
                    getState(shippingAdd.getSstate(),shippingAdd.getScity(),shippingAdd.getSpincode());
                    }
                } else {
                    // null
                }
            }

            @Override
            public void onFailure(Call<UserAddress> call, Throwable t) {

            }
        });
    }

    private void getState(final String sstate, final String scity, final String spincode/*, final String sarea*/) {
        Api api = RestManager.instanceOf();
        Call<State> call = api.getState("state");
        call.enqueue(new Callback<State>() {
            @Override
            public void onResponse(Call<State> call, Response<State> response) {
                State state1 = response.body();
                if (state1.getMsg().equals("Success")) {
                    state_layout.setVisibility(View.VISIBLE);
                    if (state1.getStatename() != null && !state1.getStatename().isEmpty()) {
                        stateList.add("-- Select State --");
                        for (Statename stateName : state1.getStatename()) {
                            stateList.add(stateName.getState());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.support_simple_spinner_dropdown_item, stateList);
                        adapter.setDropDownViewResource(R.layout.support_small_simple_drop_down);
                        spinner_state.setAdapter(adapter);
                        if (sstate != null && !sstate.isEmpty()) {
                            int spinnerPosition = adapter.getPosition(sstate);
                            spinner_state.setSelection(spinnerPosition);
                        }
                        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                stateName = parent.getItemAtPosition(position).toString();
                                View v = spinner_state.getSelectedView();
                                ((TextView) v).setTextSize(14);
                                Log.d("from Size List", "onItemSelected: ====================" + stateName);
                                if (!stateName.equals("-- Select State --")) {
                                    getCity(stateName, scity, spincode/*, sarea*/);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });
                    }
                } else {
                    //null response
                }
            }

            @Override
            public void onFailure(Call<State> call, Throwable t) {
            }
        });
    }
    private void getCity(final String stateName, final String scity, final String spincode/*, final String sarea*/) {
        Api api = RestManager.instanceOf();
        Call<CityApi> call = api.getCity(stateName);
        call.enqueue(new Callback<CityApi>() {
            @Override
            public void onResponse(Call<CityApi> call, Response<CityApi> response) {
                CityApi cityApi = response.body();
                if (cityApi.getMsg().equals("Success")) {
                    cityLayout.setVisibility(View.VISIBLE);
                    cityList.clear();
                    if (cityApi.getCityName() != null && !cityApi.getCityName().isEmpty()) {
                        cityList.add("-- Select City --");
                        for (CityModel cityName : cityApi.getCityName()) {
                            cityList.add(cityName.getCity());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.support_simple_spinner_dropdown_item, cityList);
                        adapter.setDropDownViewResource(R.layout.support_small_simple_drop_down);
                        shippingCity_spinner.setAdapter(adapter);
                        if (scity != null && !scity.isEmpty()) {
                            int spinnerPosition = adapter.getPosition(scity);
                            shippingCity_spinner.setSelection(spinnerPosition);
                        }
                        shippingCity_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                cityName = parent.getItemAtPosition(position).toString();
                                View v = shippingCity_spinner.getSelectedView();
                                ((TextView) v).setTextSize(14);
                                Log.d("from Size List", "onItemSelected: " + cityName);
                                if (!cityName.equals("-- Select City --")) {
                                    getPincode(stateName,cityName, spincode/*, sarea*/);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });
                    }
                } else {
                    cityList.clear();
                    cityLayout.setVisibility(View.GONE);
                    getPincode(stateName,cityName, spincode/*, sarea*/);
                    Toast.makeText(context, cityApi.getMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CityApi> call, Throwable t) {

            }
        });
    }
    private void getPincode(final String stateName, final String scity, final String spincode/*, final String sarea*/) {
        Api api = RestManager.instanceOf();
        Call<PinCode> call = api.getPinCode(scity);
        call.enqueue(new Callback<PinCode>() {
            @Override
            public void onResponse(Call<PinCode> call, Response<PinCode> response) {
                PinCode PinCode = response.body();
                if (PinCode.getMsg().equals("Success")) {
                    pincodeLayout.setVisibility(View.VISIBLE);
                    pincodeList.clear();
                    if (PinCode.getPincode() != null && !PinCode.getPincode().isEmpty()) {
                        pincodeList.add("-- Select City --");
                        for (PincodeName cityName : PinCode.getPincode()) {
                            pincodeList.add(cityName.getPincode());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.support_simple_spinner_dropdown_item, pincodeList);
                        adapter.setDropDownViewResource(R.layout.support_small_simple_drop_down);
                        shippingPinCode_spinner.setAdapter(adapter);
                        if (spincode != null && !spincode.isEmpty()) {
                            int spinnerPosition = adapter.getPosition(spincode);
                            shippingPinCode_spinner.setSelection(spinnerPosition);
                        }
                        shippingPinCode_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                pinCodeName = parent.getItemAtPosition(position).toString();
                                View v = shippingPinCode_spinner.getSelectedView();
                                ((TextView) v).setTextSize(14);
                                Log.d("from Size List", "onItemSelected: " + pinCodeName);
                                if (!pinCodeName.equals("-- Select City --")) {
//                                     getPincode(cityName, spincode/*, sarea*/);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });
                    }
                } else {
                    pincodeList.clear();
                    pincodeLayout.setVisibility(View.GONE);
                    Toast.makeText(context, PinCode.getMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PinCode> call, Throwable t) {

            }
        });
    }


    private void getBillingData() {
        if (billingFName_tV.getText().toString().trim().isEmpty() && billingFName_tV.getText().toString().trim().equals("")) {
            billingFName_tV.setError("First name required");
            billingFName_tV.requestFocus();
            hideKeyboardFrom(billingFName_tV);
            return;
        }
        if (billingLName_tV.getText().toString().trim().isEmpty() && billingLName_tV.getText().toString().trim().equals("")) {
            billingLName_tV.setError("Last name required");
            billingLName_tV.requestFocus();
            hideKeyboardFrom(billingLName_tV);
            return;
        }
        if (billingMobile_tV.getText().toString().trim().isEmpty() && billingMobile_tV.getText().toString().trim().equals("")) {
            billingMobile_tV.setError("Mobile no. required");
            billingMobile_tV.requestFocus();
            hideKeyboardFrom(billingMobile_tV);
            return;
        }
        if (billingMobile_tV.getText().toString().trim().length() < 10 || billingMobile_tV.getText().toString().trim().length() > 10) {
            billingMobile_tV.setError("Enter valid 10 digit Mobile No.");
            billingMobile_tV.requestFocus();
            hideKeyboardFrom(billingMobile_tV);
            return;
        }
        if (billingHouse_tV.getText().toString().trim().isEmpty() && billingHouse_tV.getText().toString().trim().equals("")) {
            billingHouse_tV.setError("Home Address required");
            billingHouse_tV.requestFocus();
            hideKeyboardFrom(billingHouse_tV);
            return;
        }
//
//        if (billingState_tV.getText().toString().trim().isEmpty() && billingState_tV.getText().toString().trim().equals("")) {
//            billingState_tV.setError("State required");
//            billingState_tV.requestFocus();
//            hideKeyboardFrom(billingState_tV);
//            return;
//        }
//        if (stateName.trim().isEmpty()) {
//            Toast.makeText(context, "Please select state", Toast.LENGTH_SHORT).show();
////            hideKeyboardFrom(shippingState_tV);
//            return;
//        }
//
//        if (billingCity_tV.getText().toString().trim().isEmpty() && billingCity_tV.getText().toString().trim().equals("")) {
//            billingCity_tV.setError("City required");
//            billingCity_tV.requestFocus();
//            hideKeyboardFrom(billingCity_tV);
//            return;
//        }
//
//        if (billingPinCode_tV.getText().toString().trim().isEmpty() && billingPinCode_tV.getText().toString().trim().equals("")) {
//            billingPinCode_tV.setError("PinCode required");
//            billingPinCode_tV.requestFocus();
//            hideKeyboardFrom(billingPinCode_tV);
//            return;
//        }

        if (stateName.equals("-- Select State --")) {
            Toast.makeText(context, "Kindly select your state", Toast.LENGTH_SHORT).show();
            spinner_state.performClick();
            return;
        }
        if (cityName.equals("-- Select City --")) {
            Toast.makeText(context, "Kindly select your City.", Toast.LENGTH_LONG).show();
            shippingCity_spinner.performClick();
            return;
        }

        if (pinCodeName.equals("-- Select PinCode --")) {
            Toast.makeText(context, "Kindly select your PinCode.", Toast.LENGTH_LONG).show();
            shippingPinCode_spinner.performClick();
            return;
        }
        
        
        if(billingHouse_tV != null) {
            addressNickName = addressNameOptional_billing.getText().toString().trim();
            firstName = billingFName_tV.getText().toString().trim();
            lastName = billingLName_tV.getText().toString().trim();
            mobile = billingMobile_tV.getText().toString().trim();
            houseAddress = billingHouse_tV.getText().toString().trim();
            landMark = billingLandMark_tV.getText().toString().trim();
//            stateName = billingState_tV.getText().toString().trim();
            cityName = billingCity_tV.getText().toString().trim();
            pinCodeName = billingPinCode_tV.getText().toString().trim();
            submitBillAddress();
        }
    }

    private void submitBillAddress(){
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(BillingActivity.this);
        progressDialog.setMessage("Updating Address...");
        progressDialog.show();
        Api api = RestManager.instanceOf();
        Call<UpdateAddress> call = api.updateAddress(userID, addressNickNShipping,firstNameShipping,
                lastNameShipping, houseAddressShipping, cityNameShipping, stateNameShipping, pinShipping,"", mobileShipping,
                landMarkShipping, addressNickName,firstName,lastName, houseAddress, cityName, stateName, pinCodeName,"",
                mobile, landMark);
        call.enqueue(new Callback<UpdateAddress>() {
            @Override
            public void onResponse(Call<UpdateAddress> call, Response<UpdateAddress> response) {
                UpdateAddress updateAddress = response.body();
                if (updateAddress.getStatus().equals("200")) {
                    progressDialog.dismiss();
                    Toast.makeText(BillingActivity.this, updateAddress.getMsg(), Toast.LENGTH_SHORT).show();
                    AppPref.instance(context).setFlagValue("1");
                    startActivity(new Intent(BillingActivity.this, OrderReviewActivity.class));
                    BillingActivity.this.finish();
                } else {
                    //null
                    progressDialog.dismiss();
                    Toast.makeText(BillingActivity.this, "Oops ! We do not deliver in your area", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UpdateAddress> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(BillingActivity.this, HomeActivity.class);
        intent.putExtra("from","cart");
        startActivity(intent);
        BillingActivity.this.finish();
    }

    private void hideKeyboardFrom(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }
}
