package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.api_models.home_response.Testimonial;

import java.util.List;

public class HappyCustomerPagerAdapter extends PagerAdapter {

    private Context mContext;
    List<Testimonial> testimonialList;

    public HappyCustomerPagerAdapter(Context mContext, List<Testimonial> topcategories) {
        this.mContext = mContext;
        this.testimonialList = topcategories;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = layoutInflater.inflate(R.layout.happy_customer_layout, container, false);
        final Testimonial testimonial = testimonialList.get(position);
        ImageView user_Image = view.findViewById(R.id.iv_groupimage);
        TextView tv_review = view.findViewById(R.id.tv_testi_money);
        TextView tv_customer_from = view.findViewById(R.id.tv_customer_from);

//        RequestOptions requestOptions = new RequestOptions();
//        requestOptions.placeholder(R.drawable.man);

        if (testimonial.getImageurl()!=null){
            Glide.with(mContext)/*.setDefaultRequestOptions(requestOptions)*/.load(testimonialList.get(position).getImageurl()).into(user_Image);
        }
        tv_review.setText(testimonial.getDescription());
        tv_customer_from.setText(testimonial.getCity());


        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        if (testimonialList != null) {
            return testimonialList.size();
        }

        return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
