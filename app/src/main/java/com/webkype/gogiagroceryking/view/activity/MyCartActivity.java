package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.cart_product_response.CartProductResponse;
import com.webkype.gogiagroceryking.model.api_models.cart_product_response.Productdetail;
import com.webkype.gogiagroceryking.model.api_models.update_cart_product_response.UpdateCartProductResponse;
import com.webkype.gogiagroceryking.view.activity.new_design.ShippingActivity;
import com.webkype.gogiagroceryking.view.adapters.ProductListInCartAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCartActivity extends AppCompatActivity {

    private static final String TAG = MyCartActivity.class.getName();
    @BindView(R.id.iv_back)
    ImageView back;


    @BindView(R.id.tv_total_items_in_cart)
    TextView tv_total_items_in_cart;


    @BindView(R.id.tv_mrp_price)
    TextView tv_mrp_price;
    @BindView(R.id.tv_discount)
    TextView tv_discount;
    @BindView(R.id.tv_delivery_charges)
    TextView tv_delivery_charges;
    @BindView(R.id.tv_total_chargis)
    TextView tv_total_chargis;

    @BindView(R.id.tv_total_chargis_bottom)
    TextView tv_total_chargis_bottom;

    @BindView(R.id.nested_scrole_view)
    NestedScrollView nested_scrole_view;
    @BindView(R.id.rl_checkout)
    RelativeLayout rl_checkout;
    @BindView(R.id.ll_empty_cart_message)
    LinearLayout ll_empty_cart_message;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;


    @BindView(R.id.rv_products_in_cart_list)
    RecyclerView rv_cats;
    ProductListInCartAdapter productListInCartAdapter;
    private String cartId = "";
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);
        ButterKnife.bind(this);
        context = this;
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        if (cartId.isEmpty()){
            rl_checkout.setVisibility(View.GONE);
            nested_scrole_view .setVisibility(View.GONE);
            ll_empty_cart_message.setVisibility(View. VISIBLE);
        }
        productListInCartAdapter = new ProductListInCartAdapter(context, new ProductListInCartAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Productdetail productdetail, int clickedFor, int position, String currentPid) {
                Log.d(TAG,"clicked_for"+clickedFor);

                if (clickedFor == ProductListInCartAdapter.REMOVE_CLICK) {
                    viewCart(cartId,ProductListInCartAdapter.REMOVE_CLICK);
                }
                if (clickedFor == ProductListInCartAdapter.VALUE_UPDATED) {
                    viewCart(cartId,ProductListInCartAdapter.VALUE_UPDATED);
                }
            }
        });

        rl_checkout.setOnClickListener(v -> {
//            startActivity(new Intent(context, DatePayActivity.class));
//            startActivity(new Intent(context, OrderReviewActivity.class));
            if (AppPref.instance(context).getValue(AppConstants.USER_ID).isEmpty()){
                startActivity(new Intent(context, LoginActivity.class));
            }else {
                startActivity(new Intent(context, ShippingActivity.class));
            }

        });
        back.setOnClickListener(v -> {
            finish();
        });
        rv_cats.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rv_cats.setAdapter(productListInCartAdapter);
        rv_cats.setFocusable(false);
        viewCart(cartId,0);
    }

    private void viewCart(String cartId,int conditions) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<CartProductResponse> ProductDetailResponseCall = RestManager.instanceOf().getCart(cartId);
            ProductDetailResponseCall.enqueue(new Callback<CartProductResponse>() {
                @Override
                public void onResponse(Call<CartProductResponse> call, Response<CartProductResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        String subTotal = response.body().getSubtotal().toString();
                        String totalCost = response.body().getTotalcost().toString();
                        String totalQntty = response.body().getTotalqty().toString();
                        String shippingChargis = response.body().getShippingprice().toString();
                        String discount = response.body().getCoupondiscount().toString();
                        List<com.webkype.gogiagroceryking.model.api_models.cart_product_response.Productdetail> productdetailList = response.body().getProductdetails();
//                        String totalCharges = response.body().getSubtotal().toString();

                        if (conditions==ProductListInCartAdapter.VALUE_UPDATED){
                            tv_mrp_price.setText("\u20B9"+totalCost);
                            tv_discount.setText("\u20B9"+discount);

                            tv_total_items_in_cart.setText(totalQntty+" items");

                            tv_delivery_charges.setText("\u20B9"+shippingChargis);

                            tv_total_chargis.setText("\u20B9"+subTotal);
                            tv_total_chargis_bottom.setText("\u20B9"+subTotal);
                        }else {
                            tv_mrp_price.setText("\u20B9"+subTotal);
                            tv_discount.setText("\u20B9"+discount);

                            tv_total_items_in_cart.setText(totalQntty+" items");

                            tv_delivery_charges.setText("\u20B9"+shippingChargis);

                            tv_total_chargis.setText("\u20B9"+subTotal);
                            tv_total_chargis_bottom.setText("\u20B9"+subTotal);
                            productListInCartAdapter.setServices(response.body().getProductdetails());
                            if (productdetailList!=null&&productdetailList.size()>0){
                                ll_empty_cart_message.setVisibility(View.GONE);
                            }else {

                            }
                        }
                    }else if (response != null && response.body().getStatus().equals("400")){

                        rl_checkout.setVisibility(View.GONE);
                        nested_scrole_view .setVisibility(View.GONE);
                        ll_empty_cart_message.setVisibility(View. VISIBLE);
                        tv_total_items_in_cart.setText(0+" items");
                    }
                }

                @Override
                public void onFailure(Call<CartProductResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateCart(String pid, String cartId, String action, String user_id) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<UpdateCartProductResponse> ProductDetailResponseCall = RestManager.instanceOf().updateCartProduct(pid, cartId, action, user_id);
            ProductDetailResponseCall.enqueue(new Callback<UpdateCartProductResponse>() {
                @Override
                public void onResponse(Call<UpdateCartProductResponse> call, Response<UpdateCartProductResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        Toast.makeText(context, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UpdateCartProductResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }
}
