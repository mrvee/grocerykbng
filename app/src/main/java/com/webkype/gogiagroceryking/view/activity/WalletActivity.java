package com.webkype.gogiagroceryking.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.listner.UserWalletListner;
import com.webkype.gogiagroceryking.controller.network.ApiManager;
import com.webkype.gogiagroceryking.controller.network.FailureCodes;
import com.webkype.gogiagroceryking.controller.network.ResponseProgressListner;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.wallet.Walletdetail;
import com.webkype.gogiagroceryking.view.activity.new_design.AddBalanceToWalletActivity;
import com.webkype.gogiagroceryking.view.adapters.TransectionsAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletActivity extends AppCompatActivity implements ResponseProgressListner, UserWalletListner {
    @BindView(R.id.iv_back)
    ImageView back;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.tv_total_balance)
    TextView tv_total_balance;


    @BindView(R.id.rl_add_money)
    RelativeLayout rl_add_money;
    @BindView(R.id.rv_transection_list)
    RecyclerView rv_transection_list;

    @BindView(R.id.tv_login_btn)
    TextView tv_login_btn;
    @BindView(R.id.ll_message)
    LinearLayout ll_message;
    @BindView(R.id.nested_scrole_view)
    NestedScrollView nested_scrole_view;

    TransectionsAdapter transectionsAdapter;
    private android.app.AlertDialog dialog;

    private Context context;
    private String backFor = "";
    private ProgressDialog progressDialog;
    private String wallet;
    private String regwallet;
    private int totalBalanc = 0;
    private String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);
        context = this;
        backFor = "";
        userId = AppPref.instance(context).getValue(AppConstants.USER_ID);
        back.setOnClickListener(v -> {
            finish();
        });

        transectionsAdapter = new TransectionsAdapter(context, new TransectionsAdapter.SmallItemClickListener() {
            @Override
            public void onItemClick(Walletdetail model, int clickedFor) {

            }
        });
        rv_transection_list.setFocusable(false);
        rl_add_money.setOnClickListener(v -> {
            showAddMoneyScreenDialog();
        });
        rv_transection_list.setLayoutManager(new LinearLayoutManager(context));
        rv_transection_list.setAdapter(transectionsAdapter);

        tv_login_btn.setOnClickListener(v -> {
            startActivity(new Intent(context, LoginActivity.class));
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (userId.isEmpty()) {
            ll_message.setVisibility(View.VISIBLE);
            nested_scrole_view.setVisibility(View.GONE);
        } else {
            getWallet();
        }
    }

    private void showAddMoneyScreenDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setCancelable(true);

        View view = LayoutInflater.from(context).inflate(R.layout.dialog_add_balance_layout, null, false);
        CardView cardSubmit = view.findViewById(R.id.card_submit);
        ImageView close = view.findViewById(R.id.iv_close);
        TextView tv_wallet_balance = view.findViewById(R.id.tv_wallet_balance);
        EditText et_wallet_amount = view.findViewById(R.id.et_wallet_amount);

        tv_wallet_balance.setText(tv_total_balance.getText().toString());
        close.setOnClickListener(v -> {
            dialog.dismiss();
        });
        cardSubmit.setOnClickListener(v -> {
            String amount =et_wallet_amount.getText().toString();
            if (amount.isEmpty()){
                Toast.makeText(context, "Amount required", Toast.LENGTH_SHORT).show();

            }else if (Integer.parseInt(amount)<=0){
                Toast.makeText(context, "Enter a valid amount", Toast.LENGTH_SHORT).show();

            }else {
                Intent addMoneyIntent = new Intent(context, AddBalanceToWalletActivity.class);
                addMoneyIntent.putExtra(AppConstants.ADD_WALLET_AMOUNT,amount);
                startActivity(addMoneyIntent);
                dialog.dismiss();
            }
        });
        builder.setView(view);
        dialog = builder.create();
        dialog.show();
    }

    private void getWallet() {
        progressDialog = new ProgressDialog(WalletActivity.this);
        progressDialog.setMessage("Fetching Wallet Balance...");
        progressDialog.show();
        ApiManager.getWallet(WalletActivity.this, userId, this, this);
    }

    @Override
    public void getWallet(String wallet) {
        this.wallet = wallet;
    }

    @Override
    public void getRegWallet(String regwallet) {
        this.regwallet = regwallet;
    }

    @Override
    public void getWalletDetail(List<Walletdetail> walletdetailList) {
        transectionsAdapter.setServices(walletdetailList);
    }

    @Override
    public void onResponseInProgress() {
        progressDialog.dismiss();

    }

    @Override
    public void onResponseCompleted(Object response) {
//        progressBar.setVisibility(View.GONE);
        progressDialog.dismiss();

        if (wallet != null && regwallet != null) {
            totalBalanc = Integer.parseInt(wallet) + Integer.parseInt(regwallet);
            tv_total_balance.setText("\u20B9 " + totalBalanc);
        } else {
            tv_total_balance.setText("\u20B9 " + wallet);
        }

    }

    @Override
    public void onResponseFailed(FailureCodes code) {
//        progressBar.setVisibility(View.GONE);
        progressDialog.dismiss();


    }
}
