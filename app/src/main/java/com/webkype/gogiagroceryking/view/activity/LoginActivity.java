package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.Api;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.login.User;
import com.webkype.gogiagroceryking.model.api_models.login.UserLogin;
import com.webkype.gogiagroceryking.view.activity.new_design.ForgotPassActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Context context;

    @BindView(R.id.et_user_id)
    EditText etUserId;
    @BindView(R.id.et_user_pass)
    EditText etUserPass;

    @BindView(R.id.rl_signIn)
    RelativeLayout tv_btn_LoginButton;

    @BindView(R.id.create_Account)
    TextView create_Account;

    @BindView(R.id.forgot_doc)
    TextView forgot_doc;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private String TAG=LoginActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        context = LoginActivity.this;
//        String mUid = AppPref.instance(this).getUid();
        if (!AppPref.instance(context).getValue(AppConstants.USER_ID).isEmpty()) {
            startActivity(new Intent(context, HomeActivity.class));
            finish();
        }
//        else {
//            startActivity(new Intent(context, HomeActivity.class));
//            finish();
//        }

        tv_btn_LoginButton.setOnClickListener(v -> {
            tv_btn_LoginButton.setEnabled(false);
            String uid = etUserId.getText().toString();
            String password = etUserPass.getText().toString();
            if (TextUtils.isEmpty(uid)) {
                Toast.makeText(context, "user id is required", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(password)) {
                Toast.makeText(context, "password required", Toast.LENGTH_SHORT).show();
            } else {
                signInWithEmailAndPassword(uid, password);
            }
            new Handler().postDelayed(() -> tv_btn_LoginButton.setEnabled(true),150);
        });


        create_Account.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
            startActivity(intent);
        });
        forgot_doc.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, ForgotPassActivity.class);
            startActivity(intent);
        });
    }

    private void signInWithEmailAndPassword(String emailID, String password) {

        progressBar.setVisibility(View.VISIBLE);
        Api api = RestManager.instanceOf();
        Call<UserLogin> logCall = api.getLoginWithEmailIdAndPassword(emailID, password);
        logCall.enqueue(new Callback<UserLogin>() {
            @Override
            public void onResponse(Call<UserLogin> call, Response<UserLogin> response) {
                tv_btn_LoginButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
                if (response != null) {
                    if (response.body().getStatus().equals("200")) {
                        User user = response.body().getUser();
                        AppPref.instance(context).setValue(AppConstants.USER_ID,user.getUid());
                        Intent intent = new Intent(context, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (response.body().getStatus().equals("400")) {
                        Toast.makeText(context, ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        Log.d(TAG,"error : singnin 400"+response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserLogin> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.d(TAG,"error : singnin"+t.getMessage());
                tv_btn_LoginButton.setEnabled(true);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        tv_btn_LoginButton.setEnabled(true);
    }
}
