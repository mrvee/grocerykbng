package com.webkype.gogiagroceryking.view.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.api_models.orderReview.Shippingtype;
import com.webkype.gogiagroceryking.view.adapters.AddressSelectAdapter;
import com.webkype.gogiagroceryking.view.adapters.TimeSlotAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DatePayActivity extends AppCompatActivity {
    @BindView(R.id.iv_back)
    ImageView back;

    @BindView(R.id.iv_select_date)
    ImageView iv_select_date;

    @BindView(R.id.rl_select_date)
    RelativeLayout rl_select_date;

    @BindView(R.id.ll_add_new_address)
    LinearLayout ll_add_new_address;

    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.tv_wallet_balance)
    TextView tv_wallet_balance;


    @BindView(R.id.rb_mywallet)
    RadioButton rb_mywallet;
    @BindView(R.id.rb_paytm)
    RadioButton rb_paytm;
    @BindView(R.id.rv_cod)
    RadioButton rv_cod;

    @BindView(R.id.iv_payment_indicate)
    ImageView iv_payment_indicate;

    @BindView(R.id.time_slot)
    RecyclerView time_slot;
    @BindView(R.id.address_slots)
    RecyclerView address_slots;

    @BindView(R.id.card_continue)
    CardView card_continue;

    Context context;

    String dateStr = "04/05/2010";

    String paymethod = "";
    final Calendar myCalendar = Calendar.getInstance();
    private TimeSlotAdapter timeSlotAdapter;
//    private AddressSelectAdapter addressSelectAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_pay);
        ButterKnife.bind(this);
        context = this;
        tv_wallet_balance.setVisibility(View.GONE);
        back.setOnClickListener(v -> {
            finish();
        });
        card_continue.setOnClickListener(v -> {
            String date = tv_date.getText().toString();
            if (date.isEmpty()) {
                Toast.makeText(context, "Please Select Date First", Toast.LENGTH_SHORT).show();
            } else if (paymethod.isEmpty()) {
                Toast.makeText(context, "Please Select Payment Method", Toast.LENGTH_SHORT).show();
            } else {
                startActivity(new Intent(context, ThankYouActivity.class));
            }
        });
        timeSlotAdapter = new TimeSlotAdapter(context, new TimeSlotAdapter.SmallItemClickListener() {
            @Override
            public void onItemClick(Shippingtype model, int clickedFor,String id) {

            }
        });
//        addressSelectAdapter = new AddressSelectAdapter(context, new AddressSelectAdapter.SmallItemClickListener() {
//            @Override
//            public void onItemClick(Object model, int clickedFor) {
//
//            }
//        });
        rb_paytm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    paymethod = "paytm";
                    Glide.with(context).load(getResources().getDrawable(R.drawable.paytm_ic)).into(iv_payment_indicate);
                } else {

                }
            }
        });
        rb_mywallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    iv_payment_indicate.setVisibility(View.GONE);
                    tv_wallet_balance.setVisibility(View.VISIBLE);
                    paymethod = "wallet";
                } else {
                    tv_wallet_balance.setVisibility(View.GONE);
                    iv_payment_indicate.setVisibility(View.VISIBLE);
                }
            }
        });
        rv_cod.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Glide.with(context).load(getResources().getDrawable(R.drawable.cod_ic)).into(iv_payment_indicate);
                    paymethod = "cod";
                } else {

                }
            }
        });

//        address_slots.setLayoutManager(new LinearLayoutManager(context));
//        address_slots.setAdapter(addressSelectAdapter);
        time_slot.setLayoutManager(new LinearLayoutManager(context));
        time_slot.setAdapter(timeSlotAdapter);

        ll_add_new_address.setOnClickListener(v -> {
            startActivity(new Intent(context, AddNewAddressActivity.class));
        });
        rl_select_date.setOnClickListener(v -> {
            setDate();
        });
        iv_select_date.setOnClickListener(v -> {
            setDate();
        });
        rv_cod.setChecked(true);
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    private void setDate() {
        DatePickerDialog dialog = new DatePickerDialog(context, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialog.show();

    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tv_date.setText(sdf.format(myCalendar.getTime()));
    }
}
