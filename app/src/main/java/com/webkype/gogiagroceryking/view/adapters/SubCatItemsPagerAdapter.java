package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.category_response.Topcategory;
import com.webkype.gogiagroceryking.view.activity.SubCategoryActivity;
import com.webkype.gogiagroceryking.view.fragments.SubProductListFragment;

import java.util.ArrayList;
import java.util.List;

public class SubCatItemsPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    List<Topcategory> topcategories = new ArrayList<>();
    SubCategoryActivity subCategoryActivity;

    public SubCatItemsPagerAdapter(FragmentManager fm, SubCategoryActivity subCategoryActivity) {

        super(fm);
        initList();
        this.subCategoryActivity = subCategoryActivity;
    }

    private void initList() {
        notifyDataSetChanged();
    }

    public void setDataList(List<Topcategory> catList) {
        this.topcategories = catList;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int i) {

        if (topcategories != null) {
            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.Cat_ID, topcategories.get(i).getCatid());
            SubProductListFragment fragment = new SubProductListFragment(subCategoryActivity);
            fragment.setArguments(bundle);
            return fragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        if (topcategories != null) {
            return topcategories.size();
        }
        return 0;
    }

//    @Override
//    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
//        return view == obj;
//    }

//
//    @Override
//    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//        View view = (View) object;
//
//        container.removeView(view);
//    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return topcategories.get(position).getCategoryname();
    }
}
