package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.view.activity.AddNewAddressActivity;

import java.util.List;


public class AddressesAdapter extends RecyclerView.Adapter<AddressesAdapter.MyViewHolder> {
    public static final int DELETE_CLICK=102,EDIT_CLICK=101;
    private LayoutInflater inflater;
    private Context context;
    List<Object> offerModelsList;
    private AddressItemClickListener listener;

    public AddressesAdapter(Context context, AddressItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    public void setServices(List<Object> imagelist) {
        this.offerModelsList = imagelist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.address_item, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
//        vh.iv_offers.setImageDrawable(context.getResources().getDrawable(R.drawable.offerone));

//        vh.actionMenuView.setOnClickListener(v->{
//            PopupMenu popupMenu = new PopupMenu(context,vh.actionMenuView);
//            popupMenu.inflate(R.menu.menu_address_options);
//            //adding click listener
//            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                @Override
//                public boolean onMenuItemClick(MenuItem item) {
//                    switch (item.getItemId()) {
//                        case R.id.medit:
//                            Intent intent = new Intent(context, AddNewAddressActivity.class);
//                            intent.putExtra("action","new");
//                            context.startActivity(intent);
//                            Toast.makeText(context, "Edit Clicked", Toast.LENGTH_SHORT).show();
//                            break;
//                        case R.id.mdelete:
//                            Toast.makeText(context, "Delete Clicked", Toast.LENGTH_SHORT).show();
//                            //handle menu2 click
//                            break;
//
//                    }
//                    return false;
//                }
//            });
//            //displaying the popup
//            popupMenu.show();
//
//        });
        vh.bind(null, listener);
    }

//    @Override
//    public int getItemCount() {
//        return topcategoryList != null ? topcategoryList.size() : 0;
//    }

    @Override
    public int getItemCount() {
        return 2;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView actionMenuView, iv_detail;
//        private CardView rl_card_bg;
        private ConstraintLayout constraint_bg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            actionMenuView = itemView.findViewById(R.id.actionMenuView);
//            iv_detail = itemView.findViewById(R.id.iv_detail);
//            constraint_bg = itemView.findViewById(R.id.constraint_bg);
//
//            rv_details = itemView.findViewById(R.id.rv_details);
//            rv_details = itemView.findViewById(R.id.rv_details);
//            rv_details = itemView.findViewById(R.id.rv_details);
        }

        public void bind(Object object, AddressItemClickListener listener) {
            actionMenuView.setOnClickListener(v->{
                PopupMenu popupMenu = new PopupMenu(context,actionMenuView);
                popupMenu.inflate(R.menu.menu_address_options);
                //adding click listener
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.medit:
                                listener.onItemClick(null,EDIT_CLICK);
                                Intent intent = new Intent(context, AddNewAddressActivity.class);
                                intent.putExtra("action","old");
                                intent.putExtra("id","1");
                                context.startActivity(intent);
                                Toast.makeText(context, "Edit Clicked", Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.mdelete:
                                listener.onItemClick(null,DELETE_CLICK);
                                break;

                        }
                        return false;
                    }
                });
                //displaying the popup
                popupMenu.show();

            });
//            rl_card_bg.setOnClickListener(v -> {
//                listener.onItemClick(object, 100);
//            });

        }

    }

    public interface AddressItemClickListener {
        void onItemClick(Object model, int clickedFor);
    }

}
