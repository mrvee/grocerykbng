package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.api_models.home_response.Banner;

import java.util.List;


public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    List<Banner> offerModelsList;

    public OffersAdapter(Context context/*, OnItemClickListener listener*/) {
        inflater = LayoutInflater.from(context);
        this.context = context;
//        this.listener = listener;
    }

    public void setDataImages(List<Banner> imagelist) {
        this.offerModelsList = imagelist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.offer_image_layout, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
        final Banner fourthBanner = offerModelsList.get(i);
        if (fourthBanner.getImageurl()!=null) {
            Glide.with(context).load(fourthBanner.getImageurl()).into(vh.iv_offers);
        }
        vh.cardBg.setOnClickListener(v->{

        });
    }

    @Override
    public int getItemCount() {
        return offerModelsList != null ? offerModelsList.size() : 0;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_offers;
       private CardView cardBg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_offers = itemView.findViewById(R.id.iv_offer_image);
            cardBg = itemView.findViewById(R.id.card_bg);

        }

    }

}
