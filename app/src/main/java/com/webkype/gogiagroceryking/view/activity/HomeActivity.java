package com.webkype.gogiagroceryking.view.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppComonValues;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.controller.utils.CartQuantity;
import com.webkype.gogiagroceryking.model.api_models.UserAccount;
import com.webkype.gogiagroceryking.model.api_models.category_response.CategoryResponse;
import com.webkype.gogiagroceryking.model.api_models.category_response.Topcategory;
import com.webkype.gogiagroceryking.model.api_models.home_response.Banner;
import com.webkype.gogiagroceryking.model.api_models.home_response.HomeResponse;
import com.webkype.gogiagroceryking.model.api_models.home_response.Stripprodetail;
import com.webkype.gogiagroceryking.model.api_models.home_response.Testimonial;
import com.webkype.gogiagroceryking.view.activity.new_design.AccountActivity;
import com.webkype.gogiagroceryking.view.adapters.BankOffersAdapter;
import com.webkype.gogiagroceryking.view.adapters.HappyCustomerPagerAdapter;
import com.webkype.gogiagroceryking.view.adapters.HomeCatListItemAdapter;
import com.webkype.gogiagroceryking.view.adapters.HomeTopPageAdapter;
import com.webkype.gogiagroceryking.view.adapters.OffersAdapter;
import com.webkype.gogiagroceryking.view.adapters.SliderPagerAdapter;
import com.webkype.gogiagroceryking.view.adapters.TopCategoryAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkype.gogiagroceryking.view.adapters.TopCategoryAdapter.CLIK_FULL_ITEM;

public class HomeActivity extends AppCompatActivity {
    private static final String TAG = HomeActivity.class.getName();
    Context context;
    public DrawerLayout drawer;

    @BindView(R.id.card_category)
    CardView card_category;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.progress_bar_cat)
    ProgressBar progressBar_cat;

    //App bar widgets
    @BindView(R.id.tv_current_locatio)
    TextView tv_current_locatio;

    @BindView(R.id.rl_cart_at_to_right)
    RelativeLayout rlTopRightCartImageLayout;

    @BindView(R.id.tv_cart_qnt)
    TextView tv_cart_qnt;

    @BindView(R.id.card_click_to_serach)
    CardView card_click_to_serach;

    //    side menu items


    @BindView(R.id.side_tv_address)
    TextView side_tv_address;

    @BindView(R.id.rl_side_mobile)
    RelativeLayout ll_side_mobile;
    @BindView(R.id.rl_side_my_address)
    RelativeLayout rl_side_address;
    @BindView(R.id.ll_side_all_addresses)
    LinearLayout ll_side_all_address;
    @BindView(R.id.ll_side_my_orders)
    LinearLayout ll_side_my_orders;
    @BindView(R.id.ll_side_cart)
    LinearLayout ll_side_cart;
    @BindView(R.id.ll_side_wallet)
    LinearLayout ll_side_wallet;
    @BindView(R.id.ll_side_offer)
    LinearLayout ll_side_offer;
    @BindView(R.id.ll_side_free_items)
    LinearLayout ll_side_free_items;
    @BindView(R.id.ll_side_winwin)
    LinearLayout ll_side_winwin;
    @BindView(R.id.ll_side_customer_support)
    LinearLayout ll_side_customer_support;
    @BindView(R.id.ll_side_rateus)
    LinearLayout ll_side_rateus;
    @BindView(R.id.ll_side_share)
    LinearLayout ll_side_share;
    @BindView(R.id.ll_side_logout)
    LinearLayout ll_side_logout;
    @BindView(R.id.ll_side_about_us)
    LinearLayout ll_side_about_us;
    @BindView(R.id.ll_side_about_release)
    LinearLayout ll_side_about_release;

    @BindView(R.id.tv_side_cart_qntty)
    TextView tv_side_cart_qntty;
    @BindView(R.id.tv_side_mobile)
    TextView tv_side_mobile;

    //    side menu items

    @BindView(R.id.ll_view_more)
    LinearLayout ll_view_more;
    @BindView(R.id.tv_view_more)
    TextView tv_view_more;
    @BindView(R.id.iv_view_more_indicator)
    ImageView iv_view_more_indicator;
    @BindView(R.id.rv_bank_offers)
    RecyclerView rv_bank_offers;

    @BindView(R.id.rv_second_banner)
    RecyclerView rv_second_banner;

    @BindView(R.id.rv_item_cats)
    RecyclerView rv_item_cats;


    @BindView(R.id.iv_btn_navigation_toggle)
    ImageView iv_btn_navigation_toggle;

    @BindView(R.id.ll_address)
    LinearLayout ll_address;


    @BindView(R.id.rv_top_category)
    RecyclerView rv_top_category;

    @BindView(R.id.vp_third_banner)
    ViewPager vp_third_banner;
    @BindView(R.id.vp_home_top_banners)
    ViewPager vp_home_top_banners;

    @BindView(R.id.vp_happyCustomer)
    ViewPager vp_happyCustomer;

    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.home_banner_indicator)
    CircleIndicator home_top_banner_indicator;

    @BindView(R.id.rv_fourth_banner)
    RecyclerView rv_fourth_banner;

    @BindView(R.id.homeRefresh)
    SwipeRefreshLayout homeRefresh;

    @BindView(R.id.rl_bottom_layout_home)
    RelativeLayout rl_bottom_layout_home;
    @BindView(R.id.tv_bottom_wallet_balance_amt)
    TextView tv_bottom_wallet_balance_amt;


    TextView tv_relode;
    View contentLayout;

    HappyCustomerPagerAdapter happyCustomerPagerAdapter;
    SliderPagerAdapter sliderPagerAdapter;
    TopCategoryAdapter topCategoryAdapter;
    BankOffersAdapter bankOfferAdapter;
    HomeTopPageAdapter homeTopPageAdapter;
    HomeCatListItemAdapter homeCatListItemAdapter;
    private OffersAdapter forthOffersAdapter, thirdOfferAdapter, secondOfferAdapter;
    private boolean doubleBackToExitPressedOnce = false;
    private Dialog dialog;

    private boolean forceToRecstart = false;
    private String cartId = "";
    private String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        context = this;
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        userId = AppPref.instance(context).getValue(AppConstants.USER_ID);

        AppComonValues.variantCartCount.clear();

        if (userId.isEmpty()) {
            tv_side_mobile.setText("Guest");
        } else {
            getUserDetail();
        }
        rlTopRightCartImageLayout.setOnClickListener(v -> {
            startActivity(new Intent(context, MyCartActivity.class));
        });
        card_category.setOnClickListener(v -> {
            startActivity(new Intent(context, CategoriesActivity.class));
        });

        card_click_to_serach.setOnClickListener(v -> {
            startActivity(new Intent(context, ProductSearchActivity.class));
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        tv_relode= drawer.findViewById(R.id.tv_relode);
        contentLayout = drawer.findViewById(R.id.contentLayout);
//        tv_btn_see_all.setOnClickListener(v -> {
//            startActivity(new Intent(context, SubCategoryActivity.class));
//        });
        rv_item_cats.setFocusable(false);
        iv_btn_navigation_toggle.setOnClickListener(v -> {
            Animation aniRotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.clock_wise);
            iv_btn_navigation_toggle.startAnimation(aniRotate);
            iv_btn_navigation_toggle.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    iv_btn_navigation_toggle.setRotation(0);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            drawer.openDrawer(Gravity.LEFT);
        });
        setAllListViews();

        ll_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float angle = iv_view_more_indicator.getRotation();
                if (angle == 90) {
                    bankOfferAdapter.setItems(9);
                    iv_view_more_indicator.setRotation(-90);
                    tv_view_more.setText("View Less");
                } else {
                    iv_view_more_indicator.setRotation(90);
                    bankOfferAdapter.setItems(5);
                    tv_view_more.setText("View More");
                }
            }
        });

        vp_third_banner.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {/*empty*/}

            @Override
            public void onPageSelected(int position) {
//                pageIndicatorView.setSelection(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {/*empty*/}
        });

        homeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHomeData();
                homeRefresh.setRefreshing(false);
            }
        });

        rl_bottom_layout_home.setOnClickListener(v -> {
            startActivity(new Intent(context, WalletActivity.class));
        });
    }

    void showNoInternet(boolean isInternetAvailable){
        LinearLayout ll_no_internet_message = findViewById(R.id.ll_no_internet_message);

        if (isInternetAvailable){
            ll_no_internet_message.setVisibility(View.GONE);
            contentLayout.setVisibility(View.VISIBLE);
            rlTopRightCartImageLayout.setEnabled(true);
            card_category.setEnabled(true);
            card_click_to_serach.setEnabled(true);
            iv_btn_navigation_toggle.setEnabled(true);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }else {
            iv_btn_navigation_toggle.setEnabled(false);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            card_category.setEnabled(false);
            card_click_to_serach.setEnabled(false);
            rlTopRightCartImageLayout.setEnabled(false);
            ll_no_internet_message.setVisibility(View.VISIBLE);
            contentLayout.setVisibility(View.GONE);
        }

        tv_relode.setOnClickListener(v->{
            getHomeData();
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        tv_current_locatio.setText(AppPref.instance(context).getValue(AppConstants.CURRENT_ADDRESS));
        side_tv_address.setText(AppPref.instance(context).getValue(AppConstants.CURRENT_ADDRESS));
        setSideMenuListeners();
        setCartQuantityToTopAndSideMenu();
        getHomeData();
    }

    void setCartQuantityToTopAndSideMenu() {
        CartQuantity.setQuantity(context, tv_cart_qnt);
        CartQuantity.setQuantity(context, tv_side_cart_qntty);
    }

    private void setSideMenuListeners() {
        ll_side_mobile.setOnClickListener(v -> {
            if (userId.isEmpty()) {
                startActivity(new Intent(context, LoginActivity.class));
            } else {
                startActivity(new Intent(context, AccountActivity.class));
            }
        });
        ll_side_cart.setOnClickListener(v -> {
            startActivity(new Intent(context, MyCartActivity.class));
        });
        ll_side_my_orders.setOnClickListener(v -> {
            startActivity(new Intent(context, MyOrdersActivity.class));
        });
        ll_address.setOnClickListener(v -> {
            startActivity(new Intent(context, MyLocationActivity.class));
        });
        ll_side_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, "I invite you to install Grocery King, a simple & Value-for-Money Grocery Shopping App. SignUp using my Referral code " + "GKING" +userId+" and get registration bonus. Refer your Friends to Earn Rewards!!!\nhttps://play.google.com/store/apps/details?id=com.webkype.GroceryKing");
                startActivity(Intent.createChooser(i, "Share Referral Code"));
            }
        });


        ll_side_wallet.setOnClickListener(v -> {
            startActivity(new Intent(context, WalletActivity.class));
        });
        rl_side_address.setOnClickListener(v -> {
            startActivity(new Intent(context, MyLocationActivity.class));
            this.drawer.closeDrawer(GravityCompat.START);
        });
        if (userId.isEmpty()) {
            ll_side_logout.setVisibility(View.GONE);
        }
        ll_side_logout.setOnClickListener(v -> {
            askLogOutDialog();
        });
        ll_side_all_address.setVisibility(View.GONE);
        ll_side_all_address.setOnClickListener(v -> {
            startActivity(new Intent(context, MyAddressActivity.class));
            this.drawer.closeDrawer(GravityCompat.START);
        });

        ll_side_customer_support.setOnClickListener(v -> {
            startActivity(new Intent(context, CustomerSupportActivity.class));
            this.drawer.closeDrawer(GravityCompat.START);
        });

    }

    private void setAllListViews() {
        rv_top_category.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rv_second_banner.setLayoutManager(new LinearLayoutManager(context));
        rv_item_cats.setLayoutManager(new LinearLayoutManager(context));
        rv_fourth_banner.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rv_bank_offers.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));

        homeTopPageAdapter = new HomeTopPageAdapter(context);

        bankOfferAdapter = new BankOffersAdapter(context, new BankOffersAdapter.BankItemClickListener() {
            @Override
            public void onItemClick(Object model, int clickedFor) {
                if (clickedFor == BankOffersAdapter.CLICK_ITEM) {
                    showBankDetail();
                }
            }
        });
        topCategoryAdapter = new TopCategoryAdapter(context, new TopCategoryAdapter.BaskitItemClickListener() {
            @Override
            public void onItemClick(Object model, int clickedFor, float angele) {
                if (clickedFor == CLIK_FULL_ITEM) {
                    Intent intent = new Intent(context, SubCategoryActivity.class);
                    startActivity(intent);
                }
            }
        });
        homeCatListItemAdapter = new HomeCatListItemAdapter(context, new HomeCatListItemAdapter.SmallItemClickListener() {
            @Override
            public void onItemClick(Object model, int clickedFor) {
                if (clickedFor == HomeCatListItemAdapter.VALUE_CHANGED) {
                    setCartQuantityToTopAndSideMenu();
                }
            }
        });

        forthOffersAdapter = new OffersAdapter(context);
        secondOfferAdapter = new OffersAdapter(context);
        thirdOfferAdapter = new OffersAdapter(context);

        sliderPagerAdapter = new SliderPagerAdapter(context);

        rv_bank_offers.setAdapter(bankOfferAdapter);

        rv_top_category.setAdapter(topCategoryAdapter);


        vp_home_top_banners.setAdapter(homeTopPageAdapter);
        home_top_banner_indicator.setViewPager(vp_home_top_banners);
        vp_home_top_banners.setPadding(10, 10, 10, 10);
        vp_home_top_banners.setPageMargin(8);

        rv_second_banner.setAdapter(secondOfferAdapter);
        rv_fourth_banner.setAdapter(forthOffersAdapter);

        rv_item_cats.setAdapter(homeCatListItemAdapter);

        vp_third_banner.setAdapter(sliderPagerAdapter);
        vp_third_banner.setClipToPadding(false);
        vp_third_banner.setPadding(16, 10, 16, 10);
        vp_third_banner.setPageMargin(8);
    }

    private void showBankDetail() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setCancelable(true);

        View view = LayoutInflater.from(context).inflate(R.layout.offer_detail_dialog, null, false);

        ImageView cancel = view.findViewById(R.id.iv_close);
        cancel.setOnClickListener(v -> {
            dialog.dismiss();
        });
        builder.setView(view);
        dialog = builder.create();
        dialog.show();
    }

    private void getHomeData() {
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            showNoInternet(true);
            progressBar.setVisibility(View.VISIBLE);
            Call<HomeResponse> homeResponseCall = RestManager.instanceOf().getHomeResponse("homepage", cartId);
            homeResponseCall.enqueue(new Callback<HomeResponse>() {
                @Override
                public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                    progressBar.setVisibility(View.GONE);
//                    homeRefresh.setRefreshing(false);
                    if (response != null && response.body().getStatus().equals("200")) {
                        List<Banner> topbanners = response.body().getTopbanners();
                        homeTopPageAdapter.setData(topbanners);
                        home_top_banner_indicator.setViewPager(vp_home_top_banners);
                        List<Banner> secondBanner = response.body().getSecondBanner();
                        List<Banner> thirdBanner = response.body().getThirdBanner();
                        List<Banner> fouthBanner = response.body().getFourthBanner();
                        List<Testimonial> testimonials = response.body().getTestimonial();
                        sliderPagerAdapter.setData(thirdBanner);
                        secondOfferAdapter.setDataImages(secondBanner);
                        forthOffersAdapter.setDataImages(fouthBanner);
                        List<Stripprodetail> stripProdList = response.body().getStripprodetails();
                        homeCatListItemAdapter.setServices(stripProdList);
//                        tv_cart_qnt.setText(response.body().);


                        happyCustomerPagerAdapter = new HappyCustomerPagerAdapter(context, testimonials);
                        vp_happyCustomer.setAdapter(happyCustomerPagerAdapter);
                        vp_happyCustomer.setClipToPadding(false);
                        vp_happyCustomer.setPadding(30, 20, 30, 20);
                        vp_happyCustomer.setPageMargin(10);
                        vp_happyCustomer.setCurrentItem(2);
                        indicator.setViewPager(vp_happyCustomer);
                        getHomeTopCategory();
                    }
                }

                @Override
                public void onFailure(Call<HomeResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            showNoInternet(false);
//            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }

    private void getHomeTopCategory() {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            showNoInternet(true);
            progressBar_cat.setVisibility(View.VISIBLE);
            Call<CategoryResponse> homeResponseCall = RestManager.instanceOf().getCategory("0");
            homeResponseCall.enqueue(new Callback<CategoryResponse>() {
                @Override
                public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                    progressBar_cat.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        List<Topcategory> topbanners = response.body().getTopcategory();
                        topCategoryAdapter.setListData(topbanners);
                    }
                }

                @Override
                public void onFailure(Call<CategoryResponse> call, Throwable t) {
                    progressBar_cat.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            showNoInternet(false);
        }
    }

    private void getUserDetail() {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar_cat.setVisibility(View.VISIBLE);
            Call<UserAccount> homeResponseCall = RestManager.instanceOf().getUserAccount(userId);
            homeResponseCall.enqueue(new Callback<UserAccount>() {
                @Override
                public void onResponse(Call<UserAccount> call, Response<UserAccount> response) {
                    progressBar_cat.setVisibility(View.GONE);

                    if (response != null && response.body().getStatus().equals("200")) {
                        UserAccount userAccount = response.body();
                        int walletBalance = 0;
                        if (userAccount.getWallet() != null && userAccount.getRegwallet() != null) {
                            walletBalance = Integer.parseInt(userAccount.getWallet()) + Integer.parseInt(userAccount.getRegwallet());
                        }
                        else if (userAccount.getWallet() != null) {
                            walletBalance = Integer.parseInt(userAccount.getWallet());
                        }
                        tv_bottom_wallet_balance_amt.setText("\u20B9 " + walletBalance);
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<UserAccount> call, Throwable t) {
                    progressBar_cat.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }

    private void askLogOutDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setTitle("Do you want to logout?");

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                forceToRecstart = true;
                onBackPressed();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBackPressed() {

        if (forceToRecstart) {
            AppPref.instance(context).clearAllPreference();
            this.finishAffinity();
            startActivity(new Intent(context, LoginActivity.class));
            AppPref.instance(context).setValue(AppConstants.IS_LAUNCHED_FIRST_TIME, "yes");
        } else if (this.drawer.isDrawerOpen(GravityCompat.START)) {
            this.drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                this.finishAffinity();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }
}
