package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.category_response.CategoryResponse;
import com.webkype.gogiagroceryking.model.api_models.category_response.Topcategory;
import com.webkype.gogiagroceryking.view.activity.SubCategoryActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;


public class TopCategoryAdapter extends RecyclerView.Adapter<TopCategoryAdapter.MyViewHolder> {
    public static final int CLIK_FULL_ITEM = 100, CLIK_FULL_ = 101;

    private LayoutInflater inflater;
    private Context context;
    List<Topcategory> topcategoryList;
    private BaskitItemClickListener listener;

    public TopCategoryAdapter(Context context, TopCategoryAdapter.BaskitItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    public void setListData(List<Topcategory> imagelist) {
        this.topcategoryList = imagelist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.top_category_layout, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
        final Topcategory category = topcategoryList.get(i);
//        vh.iv_offers.setImageDrawable(context.getResources().getDrawable(R.drawable.offerone));

        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        final SmallItemAdapter adapter = new SmallItemAdapter(context, new SmallItemAdapter.SmallItemClickListener() {
            @Override
            public void onItemClick(Topcategory model, int clickedFor) {
                if (clickedFor == SmallItemAdapter.ITEM_CLICK) {
                    Intent intent = new Intent(context, SubCategoryActivity.class);
                    intent.putExtra(AppConstants.Cat_ID, model.getCatid());
                    intent.putExtra(AppConstants.Cat_TYPE, model.getCategoryname());

                    context.startActivity(intent);
                }
            }
        });
        vh.rv_details.setLayoutManager(layoutManager);

        vh.tv_cat_title.setText(category.getCategoryname());
        if (category.getCategoryname() != null) {
            vh.tv_cat_description.setText(category.getCategorydesc());
        }
        vh.tv_off_message.setText("");
        if (category.getImageurl() != null) {
            Glide.with(context).load(category.getImageurl()).into(vh.iv_cat_image);
        }

        vh.constraint_bg.setOnClickListener(v -> {
            float angle = vh.iv_detail_indicator.getRotation();
            if (angle == 90) {
                vh.constraint_bg.setBackgroundColor(context.getResources().getColor(R.color.light_yello));
                vh.rv_details.setAdapter(adapter);
                getSubCatCategory(vh.progress_bar, adapter, category.getCatid());
                vh.iv_detail_indicator.setRotation(-90);
                vh.line_View.setVisibility(View.GONE);
            } else {
                vh.constraint_bg.setBackgroundColor(context.getResources().getColor(R.color.white));
                vh.iv_detail_indicator.setRotation(90);
                vh.rv_details.setAdapter(null);
                vh.line_View.setVisibility(View.VISIBLE);
            }
        });

//        vh.bind(null, listener);
    }

    @Override
    public int getItemCount() {
        return topcategoryList != null ? topcategoryList.size() : 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_off_message, tv_cat_title, tv_cat_description;
        private ImageView iv_cat_image, iv_detail_indicator;
        private RecyclerView rv_details;
        private ProgressBar progress_bar;
        private ConstraintLayout constraint_bg;
        private View line_View;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            rv_details = itemView.findViewById(R.id.rv_details);
            iv_cat_image = itemView.findViewById(R.id.iv_cat_image);
            constraint_bg = itemView.findViewById(R.id.constraint_bg);
            iv_detail_indicator = itemView.findViewById(R.id.iv_detail_indicator);

            tv_cat_title = itemView.findViewById(R.id.tv_cat_title);
            tv_cat_description = itemView.findViewById(R.id.tv_cat_description);
            tv_off_message = itemView.findViewById(R.id.tv_cat_off_message);

            line_View= itemView.findViewById(R.id.line_View);
            progress_bar = itemView.findViewById(R.id.progress_bar);

        }

        public void bind(Object object, BaskitItemClickListener listener) {
            constraint_bg.setOnClickListener(v -> {
                float angle = iv_detail_indicator.getRotation();
                listener.onItemClick(null, CLIK_FULL_ITEM, angle);
            });

        }

    }

    public interface BaskitItemClickListener {
        void onItemClick(Object model, int clickedFor, float angele);
    }

    private void getSubCatCategory(ProgressBar progressBar, SmallItemAdapter smallItemAdapter, String catId) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<CategoryResponse> homeResponseCall = RestManager.instanceOf().getCategory(catId);
            homeResponseCall.enqueue(new Callback<CategoryResponse>() {
                @Override
                public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        List<Topcategory> topbanners = response.body().getTopcategory();
                        smallItemAdapter.setDataList(topbanners);
                    }
                }

                @Override
                public void onFailure(Call<CategoryResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }
}
