package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.api_models.home_response.Banner;

import java.util.List;

public class HomeTopPageAdapter extends PagerAdapter {

    private Context mContext;
    List<Banner> imageUrls;

    public HomeTopPageAdapter(Context mContext) {
        this.mContext = mContext;

    }
    public void setData(List<Banner> topbannerList){
        this.imageUrls = topbannerList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.home_banner_item, container, false);
        String url = imageUrls.get(position).getImageurl();
        ImageView im_slider = view.findViewById(R.id.iv_offer);

        if (url!=null){
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.logo2);
//            requestOptions.error(R.drawable.ic_error);
            Glide.with(mContext).setDefaultRequestOptions(requestOptions).load(url).into(im_slider);
        }
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        if (imageUrls != null) {
            return imageUrls.size();
        }
        return 0;
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
