package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.webkype.gogiagroceryking.R;

import java.util.List;


public class ProductHighLightsAdapter extends RecyclerView.Adapter<ProductHighLightsAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    List<Object> offerModelsList;

    public ProductHighLightsAdapter(Context context/*, OnItemClickListener listener*/) {
        inflater = LayoutInflater.from(context);
        this.context = context;
//        this.listener = listener;
    }

    public void setServices(List<Object> imagelist) {
        this.offerModelsList = imagelist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.product_highlights_item, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;

        vh.rl_bg.setOnClickListener(v -> {
            float angle =vh.iv_indicator.getRotation();
            if (angle==-90){
                vh.iv_indicator.setRotation(90);
            }else {
                vh.iv_indicator.setRotation(-90);
            }

        });
    }

//    @Override
//    public int getItemCount() {
//        return topcategoryList != null ? topcategoryList.size() : 0;
//    }

    @Override
    public int getItemCount() {
        return 6;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_indicator;
        private TextView tv_header,tv_details;
        private RelativeLayout rl_bg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_indicator = itemView.findViewById(R.id.iv_indicator);
            tv_header = itemView.findViewById(R.id.tv_header);
            tv_details = itemView.findViewById(R.id.tv_title);
            rl_bg = itemView.findViewById(R.id.rl_bg);
        }

    }

}
