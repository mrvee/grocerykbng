package com.webkype.gogiagroceryking.view.activity.new_design;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;


import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.view.activity.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentActivity extends AppCompatActivity {

    @BindView(R.id.backPayment)
    ImageView backPayment;

    @BindView(R.id.webView1)
    WebView webView1;
    private String url;

    private String userId="",cartId="",shippingType="";
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        context=this;
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        userId = AppPref.instance(context).getValue(AppConstants.USER_ID);

        if (getIntent()!=null){
            shippingType = getIntent().getStringExtra(AppConstants.SHIPPING_TYPE);
        }
        backPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        url = AppConstants.ORDER_PLACE+"?cartid="+cartId+"&user_id="+userId+"&payment_mode="+ "cod" +"&shipping_type_id="+ shippingType;

        Log.d("a", "onCreate: " + url);
        loadWebViewLoad(webView1);
        AppPref.instance(context).setValue(AppConstants.CART_ID,"");
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void loadWebViewLoad(WebView webview) {
        final ProgressDialog pd = ProgressDialog.show(PaymentActivity.this, "", "Please wait...", true);

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setSupportMultipleWindows(true);
        webview.setWebViewClient(new WebViewClient(){
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(PaymentActivity.this, description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                pd.show();
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                pd.dismiss();
            }
        });
        webview.setWebChromeClient(new WebChromeClient());
        webview.loadUrl(url);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppPref.instance(context).setValue(AppConstants.CART_ID,"");
        startActivity(new Intent(PaymentActivity.this, HomeActivity.class));
        this.finishAffinity();
    }
}
