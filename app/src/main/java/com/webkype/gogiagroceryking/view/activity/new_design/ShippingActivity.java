package com.webkype.gogiagroceryking.view.activity.new_design;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.Api;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.UpdateAddress;
import com.webkype.gogiagroceryking.model.api_models.address.Shippingaddress;
import com.webkype.gogiagroceryking.model.api_models.address.UserAddress;
import com.webkype.gogiagroceryking.model.api_models.city.CityApi;
import com.webkype.gogiagroceryking.model.api_models.city.CityModel;
import com.webkype.gogiagroceryking.model.api_models.pinCode.PinCode;
import com.webkype.gogiagroceryking.model.api_models.pinCode.PincodeName;
import com.webkype.gogiagroceryking.model.api_models.state.State;
import com.webkype.gogiagroceryking.model.api_models.state.Statename;
import com.webkype.gogiagroceryking.view.activity.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShippingActivity extends AppCompatActivity {

    @BindView(R.id.backShipping)
    RelativeLayout backShipping;
    @BindView(R.id.addressNameOptional)
    EditText addressNameOptional;
    @BindView(R.id.shippingFName_tV)
    EditText shippingFName_tV;
    @BindView(R.id.shippingLName_tV)
    EditText shippingLName_tV;
    @BindView(R.id.shippingMobile_tV)
    EditText shippingMobile_tV;
    @BindView(R.id.shippingHouse_tV)
    EditText shippingHouse_tV;

    //    @BindView(R.id.shippingState_tV)
//    EditText shippingState_tV;
//    @BindView(R.id.shippingCity_tV)
//    EditText shippingCity_tV;
//    @BindView(R.id.shippingPinCode_tV)
//    EditText shippingPinCode_tV;

    @BindView(R.id.shippingLandMark_tV)
    EditText shippingLandMark_tV;
    @BindView(R.id.shippingCheck)
    CheckBox shippingCheck;
    @BindView(R.id.saveShipping_tV)
    TextView saveShipping_tV;


    @BindView(R.id.spinner_state)
    AppCompatSpinner spinner_state;
    @BindView(R.id.state_layout)
    RelativeLayout state_layout;

    @BindView(R.id.shippingCity_spinner)
    AppCompatSpinner shippingCity_spinner;
    @BindView(R.id.cityLayout)
    RelativeLayout cityLayout;

    @BindView(R.id.shippingPinCode_spinner)
    AppCompatSpinner shippingPinCode_spinner;
    @BindView(R.id.pincodeLayout)
    RelativeLayout pincodeLayout;


    private String stateName = "", cityName, pinCodeName, areaName;
    private String addressNickName, firstName, lastName, mobile, houseAddress, landMark;
    private Context context;
    private String userID = "", cartID = "";
    private List<String> stateList = new ArrayList<>();
    private List<String> cityList = new ArrayList<>();
    private List<String> pincodeList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping);
        ButterKnife.bind(this);
        context = this;
        userID = AppPref.instance(context).getValue(AppConstants.USER_ID);
        cartID = AppPref.instance(context).getValue(AppConstants.CART_ID);
        backShipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//        getState();
        getUserAddress();

        saveShipping_tV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getShippingData();
            }
        });

        if (AppPref.instance(context).getFlagValue().equals("0")) {

        } else if (AppPref.instance(context).getFlagValue().equals("1")) {
            startActivity(new Intent(ShippingActivity.this, OrderReviewActivity.class));
            ShippingActivity.this.finish();
        }

//        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                stateName = parent.getItemAtPosition(position).toString();
//                View v = spinner_state.getSelectedView();
//                ((TextView) v).setTextSize(14);
//                Log.d("from Size List", "onItemSelected: ====================" + stateName);
//                if (!stateName.equals("-- Select State --")) {
////                                    getCity(stateName, scity, spincode/*, sarea*/);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });

    }


    private void getState(final String sstate, final String scity, final String spincode/*, final String sarea*/) {
        Api api = RestManager.instanceOf();
        Call<State> call = api.getState("state");
        call.enqueue(new Callback<State>() {
            @Override
            public void onResponse(Call<State> call, Response<State> response) {
                State state1 = response.body();
                if (state1.getMsg().equals("Success")) {
                    state_layout.setVisibility(View.VISIBLE);
                    if (state1.getStatename() != null && !state1.getStatename().isEmpty()) {
                        stateList.add("-- Select State --");
                        for (Statename stateName : state1.getStatename()) {
                            stateList.add(stateName.getState());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(ShippingActivity.this, R.layout.support_simple_spinner_dropdown_item, stateList);
                        adapter.setDropDownViewResource(R.layout.support_small_simple_drop_down);
                        spinner_state.setAdapter(adapter);
                        if (sstate != null && !sstate.isEmpty()) {
                            int spinnerPosition = adapter.getPosition(sstate);
                            spinner_state.setSelection(spinnerPosition);
                        }
                        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                stateName = parent.getItemAtPosition(position).toString();
                                View v = spinner_state.getSelectedView();
                                ((TextView) v).setTextSize(14);
                                Log.d("from Size List", "onItemSelected: ====================" + stateName);
                                if (!stateName.equals("-- Select State --")) {
                                    getCity(stateName, scity, spincode/*, sarea*/);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });
                    }
                } else {
                    //null response
                }
            }

            @Override
            public void onFailure(Call<State> call, Throwable t) {
            }
        });
    }

    private void getUserAddress() {
        Api api = RestManager.instanceOf();
        Call<UserAddress> call = api.getAddress(userID);
        call.enqueue(new Callback<UserAddress>() {
            @Override
            public void onResponse(Call<UserAddress> call, Response<UserAddress> response) {
                UserAddress userAddress = response.body();
                if (userAddress.getStatus().equals("200")) {
                    for (Shippingaddress shippingAdd : userAddress.getShippingaddress()) {
                        addressNameOptional.setText(shippingAdd.getSnickname());
                        shippingFName_tV.setText(shippingAdd.getSfname());
                        shippingLName_tV.setText(shippingAdd.getSlname());
                        shippingMobile_tV.setText(shippingAdd.getSmobile());
                        shippingHouse_tV.setText(shippingAdd.getSaddress());
                        shippingLandMark_tV.setText(shippingAdd.getSlandmark());
//                        shippingPinCode_tV.setText(shippingAdd.getSpincode());
//                        shippingCity_tV.setText(shippingAdd.getScity());
//                        shippingState_tV.setText(shippingAdd.getSstate());

//                        for (String address : stateList) {
//                            if (address.equals(shippingAdd.getSstate())) {
//                                int index = stateList.indexOf(shippingAdd.getSstate());
//                                spinner_state.setSelection(index);
//                            }
//                        }
                        getState(shippingAdd.getSstate(),shippingAdd.getScity(),shippingAdd.getSpincode());
                    }
                } else {
                    // null
                }
            }

            @Override
            public void onFailure(Call<UserAddress> call, Throwable t) {

            }
        });


    }

    private void getState() {
        Api api = RestManager.instanceOf();
        Call<State> call = api.getState("state");
        call.enqueue(new Callback<State>() {
            @Override
            public void onResponse(Call<State> call, Response<State> response) {
                State state1 = response.body();
                if (state1.getMsg().equals("Success")) {
                    state_layout.setVisibility(View.VISIBLE);
                    if (state1.getStatename() != null && !state1.getStatename().isEmpty()) {
                        stateList.add("-- Select State --");
                        for (Statename stateName : state1.getStatename()) {
                            stateList.add(stateName.getState());
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<>(ShippingActivity.this, R.layout.support_simple_spinner_dropdown_item, stateList);
                        adapter.setDropDownViewResource(R.layout.support_small_simple_drop_down);
                        spinner_state.setAdapter(adapter);
//                        getUserAddress();
//                        getCity();
                    }
                } else {
                    //null response
                }
            }

            @Override
            public void onFailure(Call<State> call, Throwable t) {
            }
        });
    }

    private void getCity(final String stateName, final String scity, final String spincode/*, final String sarea*/) {
        Api api = RestManager.instanceOf();
        Call<CityApi> call = api.getCity(stateName);
        call.enqueue(new Callback<CityApi>() {
            @Override
            public void onResponse(Call<CityApi> call, Response<CityApi> response) {
                CityApi cityApi = response.body();
                if (cityApi.getMsg().equals("Success")) {
                    cityLayout.setVisibility(View.VISIBLE);
                    cityList.clear();
                    if (cityApi.getCityName() != null && !cityApi.getCityName().isEmpty()) {
                        cityList.add("-- Select City --");
                        for (CityModel cityName : cityApi.getCityName()) {
                            cityList.add(cityName.getCity());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(ShippingActivity.this, R.layout.support_simple_spinner_dropdown_item, cityList);
                        adapter.setDropDownViewResource(R.layout.support_small_simple_drop_down);
                        shippingCity_spinner.setAdapter(adapter);
                        if (scity != null && !scity.isEmpty()) {
                            int spinnerPosition = adapter.getPosition(scity);
                            shippingCity_spinner.setSelection(spinnerPosition);
                        }
                        shippingCity_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                cityName = parent.getItemAtPosition(position).toString();
                                View v = shippingCity_spinner.getSelectedView();
                                ((TextView) v).setTextSize(14);
                                Log.d("from Size List", "onItemSelected: " + cityName);
                                if (!cityName.equals("-- Select City --")) {
                                     getPincode(stateName,cityName, spincode/*, sarea*/);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });
                    }
                } else {
                    cityList.clear();
                    cityLayout.setVisibility(View.GONE);
                    Toast.makeText(ShippingActivity.this, cityApi.getMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CityApi> call, Throwable t) {

            }
        });
    }
    private void getPincode(final String stateName, final String scity, final String spincode/*, final String sarea*/) {
        Api api = RestManager.instanceOf();
        Call<PinCode> call = api.getPinCode(scity);
        call.enqueue(new Callback<PinCode>() {
            @Override
            public void onResponse(Call<PinCode> call, Response<PinCode> response) {
                PinCode PinCode = response.body();
                if (PinCode.getMsg().equals("Success")) {
                    pincodeLayout.setVisibility(View.VISIBLE);
                    pincodeList.clear();
                    if (PinCode.getPincode() != null && !PinCode.getPincode().isEmpty()) {
                        pincodeList.add("-- Select City --");
                        for (PincodeName cityName : PinCode.getPincode()) {
                            pincodeList.add(cityName.getPincode());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(ShippingActivity.this, R.layout.support_simple_spinner_dropdown_item, pincodeList);
                        adapter.setDropDownViewResource(R.layout.support_small_simple_drop_down);
                        shippingPinCode_spinner.setAdapter(adapter);
                        if (spincode != null && !spincode.isEmpty()) {
                            int spinnerPosition = adapter.getPosition(spincode);
                            shippingPinCode_spinner.setSelection(spinnerPosition);
                        }
                        shippingPinCode_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                pinCodeName = parent.getItemAtPosition(position).toString();
                                View v = shippingPinCode_spinner.getSelectedView();
                                ((TextView) v).setTextSize(14);
                                Log.d("from Size List", "onItemSelected: " + pinCodeName);
                                if (!pinCodeName.equals("-- Select City --")) {
//                                     getPincode(cityName, spincode/*, sarea*/);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });
                    }
                } else {
                    pincodeList.clear();
                    pincodeLayout.setVisibility(View.GONE);
                    Toast.makeText(ShippingActivity.this, PinCode.getMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PinCode> call, Throwable t) {

            }
        });
    }


    private void getShippingData() {
        if (shippingFName_tV.getText().toString().trim().isEmpty() && shippingFName_tV.getText().toString().trim().equals("")) {
            shippingFName_tV.setError("First name required");
            shippingFName_tV.requestFocus();
            hideKeyboardFrom(shippingFName_tV);
            return;
        }
        if (shippingLName_tV.getText().toString().trim().isEmpty() && shippingLName_tV.getText().toString().trim().equals("")) {
            shippingLName_tV.setError("Last name required");
            shippingLName_tV.requestFocus();
            hideKeyboardFrom(shippingLName_tV);
            return;
        }
        if (shippingMobile_tV.getText().toString().trim().isEmpty() && shippingMobile_tV.getText().toString().trim().equals("")) {
            shippingMobile_tV.setError("Mobile number required");
            shippingMobile_tV.requestFocus();
            hideKeyboardFrom(shippingMobile_tV);
            return;
        }
        if (shippingMobile_tV.getText().toString().trim().length() < 10 || shippingMobile_tV.getText().toString().trim().length() > 10) {
            shippingMobile_tV.setError("Enter valid 10 digit Mobile No.");
            shippingMobile_tV.requestFocus();
            hideKeyboardFrom(shippingMobile_tV);
            return;
        }
        if (shippingHouse_tV.getText().toString().trim().isEmpty() && shippingHouse_tV.getText().toString().trim().equals("")) {
            shippingHouse_tV.setError("Home Address required");
            shippingHouse_tV.requestFocus();
            hideKeyboardFrom(shippingHouse_tV);
            return;
        }
//        if (shippingState_tV.getText().toString().trim().isEmpty() && shippingState_tV.getText().toString().trim().equals("")) {
//            shippingState_tV.setError("State required");
//            shippingState_tV.requestFocus();
//            hideKeyboardFrom(shippingState_tV);
//            return;
//        }

//        if (shippingCity_tV.getText().toString().trim().isEmpty() && shippingCity_tV.getText().toString().trim().equals("")) {
//            shippingCity_tV.setError("City required");
//            shippingCity_tV.requestFocus();
//            hideKeyboardFrom(shippingCity_tV);
//            return;
//        }
        if (stateName.equals("-- Select State --")) {
            Toast.makeText(context, "Kindly select your state", Toast.LENGTH_SHORT).show();
            spinner_state.performClick();
            return;
        }
        if (cityName.equals("-- Select City --")) {
            Toast.makeText(ShippingActivity.this, "Kindly select your City.", Toast.LENGTH_LONG).show();
            shippingCity_spinner.performClick();
            return;
        }

        if (pinCodeName.equals("-- Select PinCode --")) {
            Toast.makeText(ShippingActivity.this, "Kindly select your PinCode.", Toast.LENGTH_LONG).show();
            shippingPinCode_spinner.performClick();
            return;
        }
        /*if (area.equals("-- Select Area --")) {
            Toast.makeText(Shipping.this, "Kindly select your Area.", Toast.LENGTH_LONG).show();
            shippingArea.performClick();
            return;
        }*/
//        if (shippingPinCode_tV.getText().toString().trim().isEmpty() && shippingPinCode_tV.getText().toString().trim().equals("")) {
//            shippingPinCode_tV.setError("PinCode required");
//            shippingPinCode_tV.requestFocus();
//            hideKeyboardFrom(shippingPinCode_tV);
//            return;
//        }
        if (shippingHouse_tV != null) {
            addressNickName = addressNameOptional.getText().toString().trim();
            firstName = shippingFName_tV.getText().toString().trim();
            lastName = shippingLName_tV.getText().toString().trim();
            mobile = shippingMobile_tV.getText().toString().trim();
            houseAddress = shippingHouse_tV.getText().toString().trim();
            landMark = shippingLandMark_tV.getText().toString().trim();
//            pinCodeName = shippingPinCode_tV.getText().toString().trim();
//            stateName = shippingState_tV.getText().toString().trim();
//            cityName = shippingCity_tV.getText().toString().trim();

            if (shippingCheck.isChecked()) {
                submitShipAddress();
            } else {
                Intent i = new Intent(context, BillingActivity.class);
                i.putExtra("addressNickName", addressNickName);
                i.putExtra("firstName", firstName);
                i.putExtra("lastName", lastName);
                i.putExtra("mobile", mobile);
                i.putExtra("houseAddress", houseAddress);
                i.putExtra("cityName", cityName);
                i.putExtra("stateName", stateName);
                i.putExtra("pin", pinCodeName);
                //i.putExtra("area","");
                i.putExtra("landMark", landMark);
                startActivity(i);
                ShippingActivity.this.finish();
            }
        }
    }

    private void submitShipAddress() {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(ShippingActivity.this);
        progressDialog.setMessage("Updating Address...");
        progressDialog.show();
        Api api = RestManager.instanceOf();
        Call<UpdateAddress> call = api.updateAddress(userID, addressNickName, firstName, lastName,
                houseAddress, cityName, stateName, pinCodeName, "", mobile, landMark, addressNickName, firstName, lastName,
                houseAddress, cityName, stateName, pinCodeName, "", mobile, landMark);
        call.enqueue(new Callback<UpdateAddress>() {
            @Override
            public void onResponse(Call<UpdateAddress> call, Response<UpdateAddress> response) {
                UpdateAddress updateAddress = response.body();
                if (updateAddress.getStatus().equals("200")) {
                    progressDialog.dismiss();
                    Toast.makeText(ShippingActivity.this, updateAddress.getMsg(), Toast.LENGTH_SHORT).show();
                    // TazzaPreference.setValue(ShippingActivity.this,false);
                    AppPref.instance(context).setFlagValue("1");
                    startActivity(new Intent(ShippingActivity.this, OrderReviewActivity.class));
                    ShippingActivity.this.finish();
                } else {
                    //null
                    progressDialog.dismiss();
                    Toast.makeText(ShippingActivity.this, "Oops ! We do not deliver in your area", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UpdateAddress> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(context, HomeActivity.class);
        intent.putExtra("from", "cart");
        startActivity(intent);
        ShippingActivity.this.finish();
    }

    private void hideKeyboardFrom(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }
}
