package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppComonValues;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.add_to_cart_response.AddToCartResponse;
import com.webkype.gogiagroceryking.model.api_models.delete_cart_product_res.CartDeleteProductrsponse;
import com.webkype.gogiagroceryking.model.api_models.products_response.Productdetail;
import com.webkype.gogiagroceryking.model.api_models.products_response.Variantdetail;
import com.webkype.gogiagroceryking.model.api_models.update_cart_product_response.UpdateCartProductResponse;

import java.util.List;
import java.util.StringTokenizer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> {
    private static final String TAG = ProductListAdapter.class.getName();
    public static final int ITEM_CLICK = 100, ADD_CLICL = 101, REMOVE_CLICK = 102, VALUE_UPDATED = 103, VARIENT_CLICK = 104;
    private LayoutInflater inflater;
    private Context context;

    List<Productdetail> productdetails;
    int quantity = 0;
    ItemClickListener listener;
    private String cartId = "";
    private String userId = "";

    public ProductListAdapter(Context context, ItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        userId = AppPref.instance(context).getValue(AppConstants.USER_ID);

        this.listener = listener;
    }

    public void setData(List<Productdetail> imagelist) {
        this.productdetails = imagelist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.product_buy_item_layout, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
        final Productdetail data = productdetails.get(i);
        vh.itemPid = data.getPid();
        if (data.getImageurl() != null) {
            Glide.with(context).load(data.getImageurl()).into(vh.iv_prd_img);
        }
        StringTokenizer costPrice =new StringTokenizer(data.getCostprice(),".");
        StringTokenizer sellingPrice =new StringTokenizer(data.getSellingprice(),".");
        vh.tv_cost_price.setText("\u20B9" + costPrice.nextToken());
        vh.tv_selling_price.setText("\u20B9" + sellingPrice.nextToken());
        if (data.getCostprice().equals(data.getSellingprice())){
            vh.tv_cost_price.setVisibility(View.GONE);
        }
        vh.tv_p_setails.setText(data.getProductname());

        if (data.getDiscount() == 0) {
            vh.tv_off.setVisibility(View.GONE);
        } else {
            vh.tv_off.setVisibility(View.VISIBLE);
            vh.tv_off.setText(data.getDiscount().intValue() + "% OFF");
        }
//--------------

        if (data.getVariantType().equals("0")) {
            vh.tv_remove_product_from_cart_view.setVisibility(View.INVISIBLE);
            vh.spinner.setVisibility(View.GONE);
            AppComonValues.variantCartCount.put(data.getPid(), data.getCartqty());
            vh.cartQntt = Integer.parseInt(data.getCartqty());

            if (vh.cartQntt == 0) {
                vh.iv_sub_more.setVisibility(View.GONE);
                vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.orange));
                vh.tv_add.setTextColor(context.getResources().getColor(R.color.white));
                vh.tv_add.setEnabled(false);
                vh.add_to_cart.setEnabled(true);
                vh.tv_add.setText("Add");

            } else {
                vh.add_to_cart.setEnabled(false);
                vh.iv_sub_more.setVisibility(View.VISIBLE);
                vh.tv_add.setText("" + vh.cartQntt);
                vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
                vh.tv_add.setTextColor(context.getResources().getColor(R.color.black));
            }

            vh.add_to_cart.setOnClickListener(v -> {
                vh.cartQntt++;
                vh.iv_sub_more.setVisibility(View.VISIBLE);
                vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
                vh.tv_add.setTextColor(context.getResources().getColor(R.color.black));
                vh.tv_add.setEnabled(true);
                vh.add_to_cart.setEnabled(false);
                vh.tv_add.setText("" + vh.cartQntt);
                addToCart(vh,data.getVariantType(), data.getPid(),  "1", userId, vh.progressBar);
            });

            vh.iv_add_more.setOnClickListener(v -> {
                vh.cartQntt++;
                vh.add_to_cart.setEnabled(false);
                vh.iv_sub_more.setVisibility(View.VISIBLE);
                vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
                vh.tv_add.setTextColor(context.getResources().getColor(R.color.black));
                vh.iv_add_more.setEnabled(false);
                if (vh.cartQntt <= 1) {
                    addToCart(vh,data.getVariantType(), data.getPid(),  "1", userId, vh.progressBar);
                } else {
                    updateCart(vh,data.getVariantType(), data.getPid(),  "add", userId, vh.progressBar);
                }
            });

            vh.iv_sub_more.setOnClickListener(v -> {
                if (vh.cartQntt <= 1) {
                    vh.cartQntt--;
                    vh.iv_sub_more.setVisibility(View.GONE);
                    vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.orange));
                    vh.tv_add.setTextColor(context.getResources().getColor(R.color.white));
                    vh.add_to_cart.setEnabled(true);
                    vh.tv_add.setText("Add");
                    deleteFromCart(data.getVariantType(), data.getPid(),  vh.progressBar);
                } else {
                    vh.cartQntt--;
                    vh.iv_sub_more.setEnabled(false);
                    updateCart(vh,data.getVariantType(), data.getPid(),  "remove", userId, vh.progressBar);
                }
            });

        }
//------------
        else if (data.getVariantType().equals("1")) {
            for (Variantdetail variantdetail : data.getVariantdetails()) {
                AppComonValues.variantCartCount.put(variantdetail.getPid(), variantdetail.getCartqty());
            }
            vh.tv_remove_product_from_cart_view.setVisibility(View.GONE);
            vh.spinner.setVisibility(View.VISIBLE);
            ArrayAdapter<Variantdetail> list = new ArrayAdapter<>(context, R.layout.spinner_items, data.getVariantdetails());
            vh.spinner.setAdapter(list);

            vh.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Variantdetail variantdetail = data.getVariantdetails().get(position);
                    StringTokenizer costPrice =new StringTokenizer(variantdetail.getCostprice(),".");
                    StringTokenizer sellingPrice =new StringTokenizer(variantdetail.getSellingprice(),".");
                    vh.tv_cost_price.setText("\u20B9" + costPrice.nextToken());
                    vh.tv_selling_price.setText("\u20B9" + sellingPrice.nextToken());
                    vh.tv_p_setails.setText(variantdetail.getProductname());
                    if (variantdetail.getCostprice().equals(variantdetail.getSellingprice())){
                        vh.tv_cost_price.setVisibility(View.GONE);
                    }
                    vh.itemPid = variantdetail.getPid();
                    if (AppComonValues.variantCartCount.get(variantdetail.getPid()) != null) {
                        vh.cartQntt = Integer.parseInt(AppComonValues.variantCartCount.get(variantdetail.getPid()));
                    } else {
                        vh.cartQntt = Integer.parseInt(variantdetail.getCartqty());
                    }

                    if (vh.cartQntt == 0) {
                        vh.iv_sub_more.setVisibility(View.GONE);
                        vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.orange));
                        vh.tv_add.setTextColor(context.getResources().getColor(R.color.white));
                        vh.tv_add.setEnabled(false);
                        vh.add_to_cart.setEnabled(true);
                        vh.tv_add.setText("Add");

                    } else {
                        vh.add_to_cart.setEnabled(false);
                        vh.iv_sub_more.setVisibility(View.VISIBLE);
                        vh.tv_add.setText("" + vh.cartQntt);
                        vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
                        vh.tv_add.setTextColor(context.getResources().getColor(R.color.black));
                    }
                    vh.add_to_cart.setOnClickListener(v -> {
                        vh.cartQntt++;
                        vh.iv_sub_more.setVisibility(View.VISIBLE);
                        vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
                        vh.tv_add.setTextColor(context.getResources().getColor(R.color.black));
                        vh.tv_add.setEnabled(true);
                        vh.add_to_cart.setEnabled(false);
                        vh.tv_add.setText("" + vh.cartQntt);
                        addToCart(vh,data.getVariantType(), variantdetail.getPid(),  "1", userId, vh.progressBar);
                    });

                    vh.iv_add_more.setOnClickListener(v -> {
                        vh.cartQntt++;
                        vh.add_to_cart.setEnabled(false);
                        vh.iv_sub_more.setVisibility(View.VISIBLE);
                        vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
                        vh.tv_add.setTextColor(context.getResources().getColor(R.color.black));
                        if (vh.cartQntt <= 1) {
                            addToCart(vh,data.getVariantType(), variantdetail.getPid(), "1", userId, vh.progressBar);
                        } else {
                            updateCart(vh,data.getVariantType(), variantdetail.getPid(),  "add", userId, vh.progressBar);
                        }

                    });
                    vh.iv_sub_more.setOnClickListener(v -> {
                        if (vh.cartQntt <= 1) {
                            vh.cartQntt--;
                            vh.iv_sub_more.setVisibility(View.GONE);
                            vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.orange));
                            vh.tv_add.setTextColor(context.getResources().getColor(R.color.white));
                            vh.add_to_cart.setEnabled(true);
                            vh.tv_add.setText("Add");
                            vh.iv_sub_more.setEnabled(false);
                            deleteFromCart(data.getVariantType(), variantdetail.getPid(), vh.progressBar);
                        } else {
                            vh.cartQntt--;
                            vh.iv_sub_more.setEnabled(false);
                            updateCart(vh,data.getVariantType(), variantdetail.getPid(), "remove", userId, vh.progressBar);
                        }
                    });

                    vh.bindView(data, listener, vh.itemPid);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            vh.spinner.setSelection(0);
        }


        vh.bindView(data, listener, vh.itemPid);
    }

    @Override
    public int getItemCount() {
        return productdetails != null ? productdetails.size() : 0;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public String itemPid = "";
        public int cartQntt = 0;
        private ImageView iv_sub_more, iv_add_more, iv_prd_img;
        private TextView tv_add, tv_cost_price, tv_selling_price, tv_off, tv_p_setails, tv_remove_product_from_cart_view;
        private ConstraintLayout cl_bg;
        private CardView add_to_cart;
        private AppCompatSpinner spinner;
        private ProgressBar progressBar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_sub_more = itemView.findViewById(R.id.iv_sub_more);
            iv_add_more = itemView.findViewById(R.id.iv_add_more);
            tv_add = itemView.findViewById(R.id.tv_add);
            cl_bg = itemView.findViewById(R.id.cl_bg);
            iv_prd_img = itemView.findViewById(R.id.iv_product);
            tv_selling_price = itemView.findViewById(R.id.tv_selling_price);
            tv_cost_price = itemView.findViewById(R.id.tv_cost_price);
            tv_off = itemView.findViewById(R.id.tv_cat_off);
            tv_p_setails = itemView.findViewById(R.id.tv_p_details);
            tv_remove_product_from_cart_view = itemView.findViewById(R.id.tv_delete_item_from_cart);
            spinner = itemView.findViewById(R.id.spinner_varient);
            add_to_cart = itemView.findViewById(R.id.card_add_to_cart);
            progressBar = itemView.findViewById(R.id.progress_bar);
        }

        public void bindView(final Productdetail productdetail, final ItemClickListener listener, String itempid) {

            cl_bg.setOnClickListener(v -> {
                listener.onItemClick(productdetail, ITEM_CLICK, getAdapterPosition(), itempid);
            });

        }

    }

    private void addToCart(MyViewHolder viewHolder,String varientType, String pid, String qnty, String user_id, final ProgressBar progressBar) {
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<AddToCartResponse> ProductDetailResponseCall = RestManager.instanceOf().addToCart(pid, cartId, qnty, user_id);
            ProductDetailResponseCall.enqueue(new Callback<AddToCartResponse>() {
                @Override
                public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {

                        AppPref.instance(context).setValue(AppConstants.CART_ID, "" + response.body().getBid());
                        Toast.makeText(context, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        listener.onItemClick(null, VALUE_UPDATED, 0, "0");
                        if (varientType.equals("0")) {
                            viewHolder.iv_add_more.setEnabled(true);
                            viewHolder.iv_sub_more.setEnabled(true);
                            viewHolder.cartQntt=1;
                            viewHolder.tv_add.setText("" + viewHolder.cartQntt);
                        } else {
                            AppComonValues.variantCartCount.put(pid, "1");
                            viewHolder.iv_add_more.setEnabled(true);
                            viewHolder.iv_sub_more.setEnabled(true);
                            viewHolder.cartQntt=1;
                            viewHolder.tv_add.setText("" + viewHolder.cartQntt);
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddToCartResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateCart(final MyViewHolder viewHolder,String varientType, String pid, String action, String user_id, final ProgressBar progressBar) {
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<UpdateCartProductResponse> ProductDetailResponseCall = RestManager.instanceOf().updateCartProduct(pid, cartId, action, user_id);
            ProductDetailResponseCall.enqueue(new Callback<UpdateCartProductResponse>() {
                @Override
                public void onResponse(Call<UpdateCartProductResponse> call, Response<UpdateCartProductResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        Toast.makeText(context, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        listener.onItemClick(null, VALUE_UPDATED, 0, "0");
                        if (varientType.equals("0")) {
                            viewHolder.iv_add_more.setEnabled(true);
                            viewHolder.iv_sub_more.setEnabled(true);

                            viewHolder.cartQntt=response.body().getQty();
                            viewHolder.tv_add.setText("" + viewHolder.cartQntt);
                        } else {
                            viewHolder.iv_add_more.setEnabled(true);
                            viewHolder.iv_sub_more.setEnabled(true);
                            viewHolder.cartQntt=response.body().getQty();
                            AppComonValues.variantCartCount.put(pid, "" + response.body().getQty());
                            viewHolder.tv_add.setText("" + viewHolder.cartQntt);
                        }
                    }
                }

                @Override
                public void onFailure(Call<UpdateCartProductResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                    viewHolder.iv_add_more.setEnabled(true);
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
            viewHolder.iv_add_more.setEnabled(true);
        }
    }

    private void deleteFromCart(String varientType, String pid, final ProgressBar progressBar) {
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<CartDeleteProductrsponse> ProductDetailResponseCall = RestManager.instanceOf().deleteCartProduct(pid, cartId);
            ProductDetailResponseCall.enqueue(new Callback<CartDeleteProductrsponse>() {
                @Override
                public void onResponse(Call<CartDeleteProductrsponse> call, Response<CartDeleteProductrsponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        Toast.makeText(context, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        listener.onItemClick(null, VALUE_UPDATED, 0, "0");
                        if (varientType.equals("0")) {
                        } else {
                            AppComonValues.variantCartCount.put(pid, "0");
                        }
                    }
                }

                @Override
                public void onFailure(Call<CartDeleteProductrsponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }


    public interface ItemClickListener {
        void onItemClick(Productdetail productdetail, int clickedFor, int position, String activePid);
    }
}
