package com.webkype.gogiagroceryking.view.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.network.ApiManager;
import com.webkype.gogiagroceryking.controller.network.FailureCodes;
import com.webkype.gogiagroceryking.controller.network.ResponseProgressListner;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

public class SignupActivity extends AppCompatActivity implements ResponseProgressListner {

    @BindView(R.id.alreadyAcoount_tV)
    TextView alreadyAcoount_tV;
    @BindView(R.id.enterFirstNameSignUp_et)
    EditText enterFirstNameSignUp_et;
    @BindView(R.id.enterLastNameSignUp_et)
    EditText enterLastNameSignUp_et;
    @BindView(R.id.enterEmailSignUp_et)
    EditText enterEmailSignUp_et;
    @BindView(R.id.enterMobileSignUp_et)
    EditText enterMobileSignUp_et;
    @BindView(R.id.enterPassSignUp_et)
    EditText enterPassSignUp_et;
    @BindView(R.id.enterCnfrmPassSignUp_et)
    EditText enterCnfrmPassSignUp_et;
    @BindView(R.id.enterReferSignUp_et)
    EditText enterReferSignUp_et;
    @BindView(R.id.signUpBtn_tV)
    Button signUpBtn_tV;

    private ProgressDialog progressDialog;


    private ArrayList<String> filePaths = new ArrayList<>();
    private String userImagePath = "";
    ProgressDialog pd;
    private final int CODE_FOR_READ_PERMISSION = 101;
    private final int CODE_FOR_WRITE_PERMISSION = 102;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        context = this;
        alreadyAcoount_tV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        signUpBtn_tV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });

    }
    private void registerUser() {
        if (enterFirstNameSignUp_et.getText().toString().trim().isEmpty() && enterFirstNameSignUp_et.getText().toString().trim().equals("")) {
            enterFirstNameSignUp_et.requestFocus();
            enterFirstNameSignUp_et.setError("First name shouldn't be empty.");
            return;
        }

        if (enterLastNameSignUp_et.getText().toString().trim().isEmpty() && enterLastNameSignUp_et.getText().toString().trim().equals("")) {
            enterLastNameSignUp_et.requestFocus();
            enterLastNameSignUp_et.setError("Last name shouldn't be empty.");
            return;
        }

        if (enterEmailSignUp_et.getText().toString().trim().isEmpty() && enterEmailSignUp_et.getText().toString().trim().equals("")) {
            enterEmailSignUp_et.requestFocus();
            enterEmailSignUp_et.setError("Email-Id shouldn't be empty.");
            return;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(enterEmailSignUp_et.getText().toString().trim()).matches()) {
            enterEmailSignUp_et.requestFocus();
            enterEmailSignUp_et.setError("Email should be valid.");
            return;
        }

        if (enterMobileSignUp_et.getText().toString().trim().isEmpty() && enterMobileSignUp_et.getText().toString().trim().equals("")) {
            enterMobileSignUp_et.requestFocus();
            enterMobileSignUp_et.setError("Mobile Number shouldn't be empty.");
            return;
        }

        if (enterMobileSignUp_et.getText().toString().trim().length() < 10 || enterMobileSignUp_et.getText().toString().trim().length() > 10) {
            enterMobileSignUp_et.requestFocus();
            enterMobileSignUp_et.setError("Mobile number should be valid.");
            return;
        }

        if (enterPassSignUp_et.getText().toString().trim().isEmpty() && enterPassSignUp_et.getText().toString().trim().equals("")) {
            enterPassSignUp_et.requestFocus();
            enterPassSignUp_et.setError("Password shouldn't be empty.");
            return;
        }
        if (enterPassSignUp_et.getText().toString().trim().length()<6) {
            enterPassSignUp_et.requestFocus();
            enterPassSignUp_et.setError("Password shouldn't be less then 6 characters.");
            return;
        }
        if (enterCnfrmPassSignUp_et.getText().toString().trim().isEmpty() && enterCnfrmPassSignUp_et.getText().toString().trim().equals("")) {
            enterCnfrmPassSignUp_et.requestFocus();
            enterCnfrmPassSignUp_et.setError("Confirm Password shouldn't be empty.");
            return;
        }

        if (!enterCnfrmPassSignUp_et.getText().toString().trim().contentEquals(enterPassSignUp_et.getText().toString().trim())) {
            enterCnfrmPassSignUp_et.requestFocus();
            enterCnfrmPassSignUp_et.setError("Password didn't match.");
            return;

        }
        if (!enterEmailSignUp_et.getText().toString().trim().isEmpty()) {
            final String firstName = enterFirstNameSignUp_et.getText().toString().trim();
            final String lastName = enterLastNameSignUp_et.getText().toString().trim();
            final String email = enterEmailSignUp_et.getText().toString().trim();
            final String mobile = enterMobileSignUp_et.getText().toString().trim();
            final String password = enterPassSignUp_et.getText().toString().trim();
            final String referral = enterReferSignUp_et.getText().toString().trim();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Registering Please Wait...");
            progressDialog.show();
            ApiManager.userSignUp(context, firstName, lastName, email, mobile, password, referral, this);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void onResponseInProgress() {

    }

    @Override
    public void onResponseCompleted(Object response) {
        progressDialog.dismiss();
        startActivity(new Intent(context, LoginActivity.class));
    }

    @Override
    public void onResponseFailed(FailureCodes code) {
        progressDialog.dismiss();
    }



    private boolean isEmailValid(String emailToBeChecked) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return emailToBeChecked.matches(regex);
    }

    /*Image Selection*/
    private void openGallery() {
        FilePickerBuilder.getInstance().setMaxCount(1)
                .setSelectedFiles(filePaths)
                .setActivityTheme(R.style.LibAppTheme)
                .pickPhoto(SignupActivity.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {

                    filePaths.clear();
                    filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));

//                    Glide.with(SignupActivity.this).load(filePaths.get(0)).into(profileImage_iV);
                    userImagePath = filePaths.get(0);
                }
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CODE_FOR_READ_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case CODE_FOR_WRITE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    CODE_FOR_READ_PERMISSION);
            return false;
        } else {
            return true;
        }
    }
}
