package com.webkype.gogiagroceryking.view.activity.new_design;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.listner.UserAccountListner;
import com.webkype.gogiagroceryking.controller.network.Api;
import com.webkype.gogiagroceryking.controller.network.ApiManager;
import com.webkype.gogiagroceryking.controller.network.FailureCodes;
import com.webkype.gogiagroceryking.controller.network.ResponseProgressListner;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.EditProfile;
import com.webkype.gogiagroceryking.view.activity.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity implements ResponseProgressListner, UserAccountListner {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.userImageEdit_iV)
    CircleImageView userImageEdit_iV;
    @BindView(R.id.fNameEdit_et)
    EditText fNameEdit_et;
    @BindView(R.id.lNameEdit_et)
    EditText lNameEdit_et;
    @BindView(R.id.saveEdit_tV)
    TextView saveEdit_tV;
    String firstName, lastName;
    private ProgressDialog progressDialog;
    private String userId = "";
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        context = this;
        userId = AppPref.instance(context).getValue(AppConstants.USER_ID);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        ApiManager.getUserAccount(this, userId, this, this);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Glide.with(this).load("https://i1.wp.com/ecell.sfitengg.org/wp-content/uploads/2016/06/Dummy-image.jpg").into(userImageEdit_iV);

        saveEdit_tV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performEdit();
            }
        });
    }

    private void performEdit() {
        String fName = fNameEdit_et.getText().toString().trim();
        String lName = lNameEdit_et.getText().toString().trim();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        Api api = RestManager.instanceOf();
        Call<EditProfile> call = api.editProfile(userId, fName, lName);
        call.enqueue(new Callback<EditProfile>() {
            @Override
            public void onResponse(Call<EditProfile> call, Response<EditProfile> response) {
                EditProfile editProfile = response.body();
                if (editProfile.getStatus().equals("200")) {
                    Toast.makeText(EditProfileActivity.this, editProfile.getMsg(), Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(EditProfileActivity.this, HomeActivity.class);
//                    intent.putExtra("from", "account");
//                    startActivity(intent);
//                    EditProfileActivity.this.finish();
                } else {
                    //null response
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<EditProfile> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void onResponseInProgress() {

    }

    @Override
    public void onResponseCompleted(Object response) {
        progressDialog.dismiss();
        fNameEdit_et.setText(firstName);
        lNameEdit_et.setText(lastName);
    }

    @Override
    public void onResponseFailed(FailureCodes code) {
        progressDialog.dismiss();
    }

    @Override
    public void getUserFName(String fname) {
        this.firstName = fname;
    }

    @Override
    public void getUserLName(String lname) {
        this.lastName = lname;
    }

    @Override
    public void getUserMail(String email) {

    }

    @Override
    public void getUserMobile(String mobile) {

    }

    @Override
    public void getWallet(String wallet) {

    }

    @Override
    public void getReferCode(String referCode) {

    }
}
