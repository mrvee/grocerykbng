package com.webkype.gogiagroceryking.view.activity.new_design;

import android.content.Context;
import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.Api;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.SignUp;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    @BindView(R.id.et_old_pass)
    EditText etOldPass;
    @BindView(R.id.et_new_password)
    EditText etPass;
    @BindView(R.id.etConfPass)
    EditText etConPass;

    @BindView(R.id.cardSubmit)
    CardView sumbmit;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        context = this;
        iv_back.setOnClickListener(v -> {
            finish();
        });
        sumbmit.setOnClickListener(v -> {
            String email = etOldPass.getText().toString();
            String npass = etPass.getText().toString();
            String cnpass = etConPass.getText().toString();
            if (TextUtils.isEmpty(email)) {
                etOldPass.requestFocus();
                Toast.makeText(context,"Old password required", Toast.LENGTH_SHORT).show();
            }else  if (TextUtils.isEmpty(npass)) {
                etPass.requestFocus();
                Toast.makeText(context,"New password required", Toast.LENGTH_SHORT).show();
            }
            else  if (npass.length()<6) {
                etPass.requestFocus();
                Toast.makeText(context,"Password should be min 6 character", Toast.LENGTH_SHORT).show();
            }
            else  if (!npass.equals(cnpass)) {
                etConPass.requestFocus();
                Toast.makeText(context,"Password did not matched", Toast.LENGTH_SHORT).show();
            }
            else {
                senVerification(email,npass);
            }
        });
    }
    private void senVerification(String oldPass,String nPass) {
        progressBar.setVisibility(View.VISIBLE);
        Api api = RestManager.instanceOf();
        Call<SignUp> responseCall = api.changePassword(AppPref.instance(context).getValue(AppConstants.USER_ID),oldPass,nPass);
        responseCall.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                progressBar.setVisibility(View.GONE);
                if (response != null) {
                    if (response.body().getStatus().equals("200")) {
                        etOldPass.setText("");
                        etPass.setText("");
                        etConPass.setText("");
                        Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else if (response.body().getStatus().equals("400")) {
                        Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });


    }
}
