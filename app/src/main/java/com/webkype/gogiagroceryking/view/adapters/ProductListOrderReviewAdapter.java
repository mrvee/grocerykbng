package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.api_models.orderReview.ProductdetailReview;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListOrderReviewAdapter extends RecyclerView.Adapter<ProductListOrderReviewAdapter.ViewHolder> {

    private Context context;
    private List<ProductdetailReview> productdetailList = new ArrayList<>();

    public ProductListOrderReviewAdapter(Context context, List<ProductdetailReview> productdetailList){
        this.context = context;
        this.productdetailList = productdetailList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_review_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        final ProductdetailReview productdetailReview = productdetailList.get(position);
        Glide.with(context).load(productdetailReview.getImageurl()).into(holder.reviewOrderImage);
        holder.reviewOrderName.setText(productdetailReview.getProductname());
        holder.originalOrderPrice.setText("₹ "+productdetailReview.getCostprice());
        strikeThroughText(holder.originalOrderPrice);
        holder.discountedOrderPrice.setText("₹ "+productdetailReview.getSellingprice());
        holder.reviewOrderQty.setText("Quantity : "+productdetailReview.getQty());
    }

    private void strikeThroughText(TextView Price){
        Price.setPaintFlags(Price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public int getItemCount() {
        return (productdetailList != null ? productdetailList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.reviewOrderImage)
        ImageView reviewOrderImage;
        @BindView(R.id.reviewOrderName)
        TextView reviewOrderName;
        @BindView(R.id.originalOrderPrice)
        TextView originalOrderPrice;
        @BindView(R.id.discountedOrderPrice)
        TextView discountedOrderPrice;
        @BindView(R.id.reviewOrderQty)
        TextView reviewOrderQty;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
