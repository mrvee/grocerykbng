package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.webkype.gogiagroceryking.R;

import java.util.List;


public class AddressSelectAdapter extends RecyclerView.Adapter<AddressSelectAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    List<Object> offerModelsList;
    private SmallItemClickListener listener;
    private int seletedItem=-1;

    public AddressSelectAdapter(Context context, AddressSelectAdapter.SmallItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    public void setServices(List<Object> imagelist) {
        this.offerModelsList = imagelist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.address_select_item, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
        if (seletedItem==i)
        { vh.iv_select_time_image.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
        }else {
            vh.iv_select_time_image.setImageDrawable(context.getResources().getDrawable(R.drawable.over_shape));
        }

        vh.ll_bg.setOnClickListener(v->{
            if (seletedItem==i){
            }else {
                seletedItem=i;
                notifyDataSetChanged();
            }
        });

        vh.bind(null, listener);
    }

//    @Override
//    public int getItemCount() {
//        return topcategoryList != null ? topcategoryList.size() : 0;
//    }

    @Override
    public int getItemCount() {
        return 2;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_select_time_image;
        private RelativeLayout ll_bg;
        private ConstraintLayout constraint_bg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_bg = itemView.findViewById(R.id.rl_item_bg);
//            iv_detail = itemView.findViewById(R.id.iv_detail);
//            constraint_bg = itemView.findViewById(R.id.constraint_bg);
//
//            rv_details = itemView.findViewById(R.id.rv_details);
//            rv_details = itemView.findViewById(R.id.rv_details);
            iv_select_time_image = itemView.findViewById(R.id.iv_select_time);
        }

        public void bind(Object object, SmallItemClickListener listener) {
//            ll_bg.setOnClickListener(v -> {
//                listener.onItemClick(object, 100);
//            });

        }

    }

    public interface SmallItemClickListener {
        void onItemClick(Object model, int clickedFor);
    }

}
