package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.controller.utils.CartQuantity;
import com.webkype.gogiagroceryking.model.api_models.category_response.CategoryResponse;
import com.webkype.gogiagroceryking.model.api_models.category_response.Topcategory;
import com.webkype.gogiagroceryking.view.adapters.SubCatItemsPagerAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubCategoryActivity extends AppCompatActivity {
    private static final String TAG = SubCategoryActivity.class.getName();

    @BindView(R.id.iv_back)
    ImageView back;

    @BindView(R.id.rl_righr)
    RelativeLayout iv_cart;

    @BindView(R.id.iv_serch)
    ImageView iv_serch;

    @BindView(R.id.tv_title_cat)
    TextView tv_title_cat;

    @BindView(R.id.et_search_key)
    EditText et_search_key;

    @BindView(R.id.tv_cart_qnt)
    TextView tv_cart_qnt;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.vp_cat_items)
    ViewPager vp_cats;

    @BindView(R.id.tabs)
    TabLayout tabs;
    SubCatItemsPagerAdapter SubCatItemsPagerAdapter;
    Context context;
    private String categoryId = "";
    private String catName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        ButterKnife.bind(this);
        context = this;
        if (getIntent() != null) {
            categoryId = getIntent().getStringExtra(AppConstants.Cat_ID);
            catName = getIntent().getStringExtra(AppConstants.Cat_TYPE);
            tv_title_cat.setText(catName);
        } else {
//            tv_title_cat.setVisibility(View.GONE);
//            tabs.setVisibility(View.GONE);
//            et_search_key.setVisibility(View.VISIBLE);
        }
        iv_serch.setOnClickListener(v -> {
            String searchKey = et_search_key.getText().toString();
            if (searchKey.isEmpty()) {
                Toast.makeText(context, "Please enter some key first", Toast.LENGTH_SHORT).show();
            }else {

            }
        });
        back.setOnClickListener(v -> {
            finish();
        });

        iv_cart.setOnClickListener(v -> {
            startActivity(new Intent(context, MyCartActivity.class));
        });
        SubCatItemsPagerAdapter = new SubCatItemsPagerAdapter(getSupportFragmentManager(),SubCategoryActivity.this);
        tabs.setupWithViewPager(vp_cats);
        vp_cats.setAdapter(SubCatItemsPagerAdapter);
        vp_cats.setFocusable(false);
        vp_cats.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
//                Toast.makeText(context, "Page " + i, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        if (!categoryId.isEmpty()) {
            getHomeTopCategory(categoryId);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        CartQuantity.setQuantity(context, tv_cart_qnt);
    }

    public void cartAPI(){
        CartQuantity.setQuantity(context, tv_cart_qnt);
    }

    private void getHomeTopCategory(String cid) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<CategoryResponse> homeResponseCall = RestManager.instanceOf().getCategory(cid);
            homeResponseCall.enqueue(new Callback<CategoryResponse>() {
                @Override
                public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        List<Topcategory> topbanners = response.body().getTopcategory();
                        SubCatItemsPagerAdapter.setDataList(topbanners);
                    }
                }

                @Override
                public void onFailure(Call<CategoryResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }
}
