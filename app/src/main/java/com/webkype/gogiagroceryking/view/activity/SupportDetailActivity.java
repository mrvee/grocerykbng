package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.view.fragments.RaiseIssueBottomFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SupportDetailActivity extends AppCompatActivity {
    @BindView(R.id.backProfile)
    ImageView ivBack;

    @BindView(R.id.tv_question)
    TextView tv_question;

    @BindView(R.id.card_raiseissue)
    CardView card_raiseissue;


    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_detail);
        context = this;
        ButterKnife.bind(this);

        if (getIntent() != null) {
            String q = getIntent().getStringExtra("q");
            tv_question.setText(q);
        }
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        card_raiseissue.setOnClickListener(v -> {
            RaiseIssueBottomFragment issueBottomFragment = new RaiseIssueBottomFragment();
            issueBottomFragment.show(getSupportFragmentManager(), issueBottomFragment.getTag());
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment frameLayout : getSupportFragmentManager().getFragments()) {
            frameLayout.onActivityResult(requestCode, resultCode, data);
        }
    }
}
