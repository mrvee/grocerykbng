package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.api_models.wallet.Walletdetail;

import java.util.List;


public class TransectionsAdapter extends RecyclerView.Adapter<TransectionsAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    List<Walletdetail> offerModelsList;
    private SmallItemClickListener listener;
    private int seletedItem = -1;

    public TransectionsAdapter(Context context, TransectionsAdapter.SmallItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    public void setServices(List<Walletdetail> imagelist) {
        this.offerModelsList = imagelist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.transection_item_layout, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
        final Walletdetail walletdetail =offerModelsList.get(i);

        final String ruppIcon="\u20B9";
            vh.tvbalanceRemained.setText("Credit : "+ruppIcon+" "+walletdetail.getCredit());
            vh.tvbalanceDeducted.setText("Debit : "+ruppIcon+" "+walletdetail.getDebit());
            vh.tv_dated.setText(walletdetail.getAdddate());
            vh.tv_paid_for.setText(walletdetail.getType());
            vh.tv_order.setText("Order no : "+walletdetail.getOrder());
            vh.tvbalanceRemained.setTextColor(context.getResources().getColor(R.color.green));
            vh.tvbalanceDeducted.setTextColor(context.getResources().getColor(R.color.black));

        vh.bind(null, listener);
    }

    @Override
    public int getItemCount() {
        return offerModelsList != null ? offerModelsList.size() : 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_left_icon;
        private TextView tvbalanceDeducted, tvbalanceRemained,tv_dated,tv_paid_for,tv_order;
        private LinearLayout ll_bg;
        private ConstraintLayout constraint_bg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_bg = itemView.findViewById(R.id.ll_item_bg);
            tv_dated = itemView.findViewById(R.id.tv_dated);
            tv_paid_for = itemView.findViewById(R.id.tv_paid_for);
            tvbalanceDeducted = itemView.findViewById(R.id.tv_balance_deducted);
            tvbalanceRemained = itemView.findViewById(R.id.tv_balance_remained);
            tv_order = itemView.findViewById(R.id.tv_order);
        }

        public void bind(Walletdetail object, SmallItemClickListener listener) {
//            ll_bg.setOnClickListener(v -> {
//                listener.onItemClick(object, 100);
//            });

        }

    }

    public interface SmallItemClickListener {
        void onItemClick(Walletdetail model, int clickedFor);
    }

}
