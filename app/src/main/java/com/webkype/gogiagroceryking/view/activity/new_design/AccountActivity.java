package com.webkype.gogiagroceryking.view.activity.new_design;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.listner.UserAccountListner;
import com.webkype.gogiagroceryking.controller.network.ApiManager;
import com.webkype.gogiagroceryking.controller.network.FailureCodes;
import com.webkype.gogiagroceryking.controller.network.ResponseProgressListner;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.view.activity.HomeActivity;
import com.webkype.gogiagroceryking.view.activity.LoginActivity;
import com.webkype.gogiagroceryking.view.activity.MyOrdersActivity;
import com.webkype.gogiagroceryking.view.activity.WalletActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AccountActivity extends AppCompatActivity implements ResponseProgressListner, UserAccountListner {

    @BindView(R.id.backAccount)
    ImageView backAccount;

    @BindView(R.id.userImageProfile_iV)
    CircleImageView userImageProfile_iV;
    @BindView(R.id.userNameProfile_tV)
    TextView userNameProfile_tV;
    @BindView(R.id.userEmailProfile_tV)
    TextView userEmailProfile_tV;
    @BindView(R.id.walletAmountProfile)
    TextView walletAmountProfile;
    @BindView(R.id.referCodeProfile)
    TextView referCodeProfile;

    @BindView(R.id.editProfile_rL)
    RelativeLayout editProfile_rL;
    @BindView(R.id.wallet_rL)
    RelativeLayout wallet_rL;
    @BindView(R.id.changePassword_rL)
    RelativeLayout changePassword_rL;
    @BindView(R.id.myOrder_rL)
    RelativeLayout myOrder_rL;
    @BindView(R.id.myReferral_rL)
    RelativeLayout myReferral_rL;
    @BindView(R.id.logout_rL)
    RelativeLayout logout_rL;

    private ProgressDialog progressDialog;
    private String fname, lname, email, mobile, userWallet, userReferralCode;
    private Dialog dialog;
    private boolean forceToRecstart = false;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        ButterKnife.bind(this);
        context = this;
        Glide.with(this).load("https://i1.wp.com/ecell.sfitengg.org/wp-content/uploads/2016/06/Dummy-image.jpg").into(userImageProfile_iV);

        getUserProfile();
        editProfile_rL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AccountActivity.this, EditProfileActivity.class));
            }
        });

        wallet_rL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountActivity.this, WalletActivity.class));
            }
        });

        changePassword_rL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AccountActivity.this, ChangePasswordActivity.class));
            }
        });

        myOrder_rL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AccountActivity.this, MyOrdersActivity.class));
            }
        });

        myReferral_rL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, "I invite you to install Grocery King, a simple & Value-for-Money Grocery Shopping App. SignUp using my Referral code " + "GRKing" + AppPref.instance(AccountActivity.this).getValue(AppConstants.USER_ID) + " and get registration bonus. Refer your Friends to Earn Rewards!!!");

//                i.putExtra(Intent.EXTRA_TEXT, "I invite you to install Grocery King, a simple & Value-for-Money Grocery Shopping App. SignUp using my Referral code " + "GRKing" +AppPref.instance(AccountActivity.this).getValue(AppConstants.USER_ID)+" and get registration bonus. Refer your Friends to Earn Rewards!!!/*\nhttps://play.google.com/store/apps/details?id=com.webkype.*/");
                startActivity(Intent.createChooser(i, "Share Referral Code"));
            }
        });


        logout_rL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askLogOutDialog();
            }
        });

        backAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getUserProfile() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Profile...");
        progressDialog.show();
        ApiManager.getUserAccount(this, AppPref.instance(AccountActivity.this).getValue(AppConstants.USER_ID), this, this);
    }

    private void askLogOutDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setTitle("Do you want to logout?");

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                AppPref.instance(AccountActivity.this).clearAllPreference();
                startActivity(new Intent(AccountActivity.this, LoginActivity.class));
                Toast.makeText(AccountActivity.this, "You are logged out", Toast.LENGTH_SHORT).show();
                forceToRecstart = true;
                onBackPressed();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (forceToRecstart) {
            AppPref.instance(context).clearAllPreference();
            this.finishAffinity();
            startActivity(new Intent(context, HomeActivity.class));
        } else {
            super.onBackPressed();
            this.finish();
        }
    }

    @Override
    public void onResponseInProgress() {

    }

    @Override
    public void onResponseCompleted(Object response) {
        progressDialog.dismiss();
        userNameProfile_tV.setText(fname + " " + lname);
        userEmailProfile_tV.setText(email);
        // walletAmountProfile.setText("\u20B9 "+userWallet);
        referCodeProfile.setText(userReferralCode);
    }

    @Override
    public void onResponseFailed(FailureCodes code) {
        progressDialog.dismiss();
    }

    @Override
    public void getUserFName(String fname) {
        this.fname = fname;
    }

    @Override
    public void getUserLName(String lname) {
        this.lname = lname;
    }

    @Override
    public void getUserMail(String email) {
        this.email = email;
    }

    @Override
    public void getUserMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public void getWallet(String wallet) {

    }

    @Override
    public void getReferCode(String referCode) {
        this.userReferralCode = referCode;
    }

}
