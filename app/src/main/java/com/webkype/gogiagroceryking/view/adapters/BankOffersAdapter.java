package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webkype.gogiagroceryking.R;

import java.util.List;


public class BankOffersAdapter extends RecyclerView.Adapter<BankOffersAdapter.MyViewHolder> {
    public final static int CLICK_ITEM = 100;
    private LayoutInflater inflater;
    private Context context;
    List<Object> offerModelsList;
    private int items = 5;
    private BankItemClickListener listener;

    public BankOffersAdapter(Context context ,BankOffersAdapter.BankItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    public void setServices(List<Object> imagelist) {
        this.offerModelsList = imagelist;
        notifyDataSetChanged();
    }

    public void setItems(int items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.offers_layout, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
//        vh.iv_offers.setImageDrawable(context.getResources().getDrawable(R.drawable.offerone));
        vh.rl_card_bg.setOnClickListener(v -> {

        });

        vh.bind(null, listener);

    }

//    @Override
//    public int getItemCount() {
//        return topcategoryList != null ? topcategoryList.size() : 0;
//    }

    @Override
    public int getItemCount() {
        return items;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_offers, iv_detail;
        private CardView rl_card_bg;
        private ConstraintLayout constraint_bg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            rl_card_bg = itemView.findViewById(R.id.card_bg);
//            iv_detail = itemView.findViewById(R.id.iv_detail);
//            constraint_bg = itemView.findViewById(R.id.constraint_bg);
//            rv_details = itemView.findViewById(R.id.rv_details);
//            rv_details = itemView.findViewById(R.id.rv_details);
//            rv_details = itemView.findViewById(R.id.rv_details);

        }

        public void bind(Object object, BankItemClickListener listener) {
            rl_card_bg.setOnClickListener(v -> {
                listener.onItemClick(null, CLICK_ITEM);
            });

        }

    }

    public interface BankItemClickListener {
        void onItemClick(Object model, int clickedFor);
    }

}
