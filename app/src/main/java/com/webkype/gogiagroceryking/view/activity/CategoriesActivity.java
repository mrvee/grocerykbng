package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.model.api_models.category_response.CategoryResponse;
import com.webkype.gogiagroceryking.model.api_models.category_response.Topcategory;
import com.webkype.gogiagroceryking.view.adapters.TopCategoryAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkype.gogiagroceryking.view.adapters.TopCategoryAdapter.CLIK_FULL_ITEM;

public class CategoriesActivity extends AppCompatActivity {


    private static final String TAG = CategoriesActivity.class.getName();
    @BindView(R.id.iv_back)
    ImageView back;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.rv_cats)
    RecyclerView rv_cats;

    TopCategoryAdapter topCategoryAdapter;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        ButterKnife.bind(this);
        context = this;
        topCategoryAdapter = new TopCategoryAdapter(context, new TopCategoryAdapter.BaskitItemClickListener() {
            @Override
            public void onItemClick(Object model, int clickedFor, float angele) {
                if (clickedFor == CLIK_FULL_ITEM) {

                }
            }
        });

        back.setOnClickListener(v -> {
            finish();
        });
        rv_cats.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rv_cats.setAdapter(topCategoryAdapter);
        getHomeTopCategory();
    }

    private void getHomeTopCategory() {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<CategoryResponse> homeResponseCall = RestManager.instanceOf().getCategory("0");
            homeResponseCall.enqueue(new Callback<CategoryResponse>() {
                @Override
                public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        List<Topcategory> topbanners = response.body().getTopcategory();
                        topCategoryAdapter.setListData(topbanners);
                    }
                }

                @Override
                public void onFailure(Call<CategoryResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }

}
