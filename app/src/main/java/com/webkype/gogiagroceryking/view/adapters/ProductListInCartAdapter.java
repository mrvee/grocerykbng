package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.add_to_cart_response.AddToCartResponse;
import com.webkype.gogiagroceryking.model.api_models.cart_product_response.Productdetail;
import com.webkype.gogiagroceryking.model.api_models.delete_cart_product_res.CartDeleteProductrsponse;
import com.webkype.gogiagroceryking.model.api_models.update_cart_product_response.UpdateCartProductResponse;

import java.util.List;
import java.util.StringTokenizer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProductListInCartAdapter extends RecyclerView.Adapter<ProductListInCartAdapter.MyViewHolder> {
    private static final String TAG = ProductListInCartAdapter.class.getName();
    public static final int ITEM_CLICK = 100, VALUE_UPDATED = 101, REMOVE_CLICK = 102, SUB_CAT_CLICK = 103, VARIENT_CLICK = 104;
    private LayoutInflater inflater;
    private Context context;

    List<Productdetail> productdetailList;
    int quantity = 0;
    ItemClickListener listener;
    private String cartId = "";
    private String userId = "";

    public ProductListInCartAdapter(Context context, ItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        userId = AppPref.instance(context).getValue(AppConstants.USER_ID);

        this.listener = listener;
    }

    public void setServices(List<Productdetail> imagelist) {
        this.productdetailList = imagelist;
        notifyDataSetChanged();
    }

    private void removeItemFromList(int position) {

        this.productdetailList.remove(position);
        notifyDataSetChanged();
        if (productdetailList==null){
            listener.onItemClick(this.productdetailList.get(position),REMOVE_CLICK,position,"0");
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.product_buy_item_layout, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
        final Productdetail data = productdetailList.get(i);
        vh.tv_delete_off.setVisibility(View.VISIBLE);
        vh.spinner.setVisibility(View.GONE);
        vh.itemPid = data.getPid();
        if (data.getImageurl() != null) {
            Glide.with(context).load(data.getImageurl()).into(vh.iv_product);
        }
        int cartQnt = Integer.parseInt(data.getQty());
        vh.cartQntt = cartQnt;

        StringTokenizer costPrice =new StringTokenizer(data.getCostprice(),".");
        StringTokenizer sellingPrice =new StringTokenizer(data.getSellingprice(),".");
        vh.tv_cost_price.setText("\u20B9" + costPrice.nextToken());
        vh.tv_selling_price.setText("\u20B9" + sellingPrice.nextToken());
        if (data.getCostprice().equals(data.getSellingprice())){
            vh.tv_cost_price.setVisibility(View.GONE);
        }
        vh.tv_p_setails.setText(data.getProductname());

        if (data.getDiscount().equals("0")) {
            vh.tv_cat_off.setVisibility(View.GONE);
        } else {
            vh.tv_cat_off.setVisibility(View.VISIBLE);
            vh.tv_cat_off.setText(Integer.parseInt(data.getDiscount()) + "% OFF");
        }

        if (cartQnt == 0) {
            vh.iv_sub_more.setVisibility(View.GONE);
            vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.orange));
            vh.tv_add.setTextColor(context.getResources().getColor(R.color.white));
            vh.tv_add.setEnabled(false);
            vh.add_to_cart.setEnabled(true);
            vh.tv_add.setText("Add");

        } else {
            vh.add_to_cart.setEnabled(false);
            vh.iv_sub_more.setVisibility(View.VISIBLE);
            vh.tv_add.setText("" + cartQnt);
            vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
            vh.tv_add.setTextColor(context.getResources().getColor(R.color.black));
        }

        vh.add_to_cart.setOnClickListener(v -> {
            vh.cartQntt++;
            vh.iv_sub_more.setVisibility(View.VISIBLE);
            vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
            vh.tv_add.setTextColor(context.getResources().getColor(R.color.black));
            vh.tv_add.setEnabled(true);
            vh.add_to_cart.setEnabled(false);
            addToCart(vh,data.getPid(), cartId, "1", userId, vh.progressBar, i);
        });

        vh.iv_add_more.setOnClickListener(v -> {
            vh.cartQntt++;
            vh.add_to_cart.setEnabled(false);
            vh.iv_sub_more.setVisibility(View.VISIBLE);
//            vh.tv_add.setText("" + cartQnt);
            vh.iv_add_more.setEnabled(false);
            vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
            vh.tv_add.setTextColor(context.getResources().getColor(R.color.black));
            updateCart(vh,data.getPid(), cartId, "add", userId, vh.progressBar, i);
        });

        vh.iv_sub_more.setOnClickListener(v -> {
            if (vh.cartQntt <= 1) {
                vh.cartQntt--;
                vh.iv_sub_more.setVisibility(View.GONE);
                vh.add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.orange));
                vh.tv_add.setTextColor(context.getResources().getColor(R.color.white));
                vh.add_to_cart.setEnabled(true);
                vh.tv_add.setText("Add");
                deleteFromCart(data.getPid(), cartId, vh.progressBar, i);
            } else {
                vh.cartQntt--;
                vh.tv_add.setText("" + vh.cartQntt);
                updateCart(vh,data.getPid(), cartId, "remove", userId, vh.progressBar, i);
            }
        });

        vh.tv_delete_off.setOnClickListener(v -> {
                deleteFromCart(data.getPid(), cartId, vh.progressBar, i);
        });

        vh.bindView(data, listener, vh.itemPid);
    }

    @Override
    public int getItemCount() {
        return productdetailList != null ? productdetailList.size() : 0;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public String itemPid = "";
        public int cartQntt = 0;
        private ImageView iv_sub_more, iv_add_more, iv_product;
        private TextView tv_add, tv_cost_price, tv_selling_price, tv_cat_off, tv_p_setails, tv_delete_off;
        private ConstraintLayout cl_bg;
        private CardView add_to_cart;
        private AppCompatSpinner spinner;
        private ProgressBar progressBar;
        public ItemClickListener listener;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_delete_off= itemView.findViewById(R.id.tv_delete_item_from_cart);
            iv_sub_more = itemView.findViewById(R.id.iv_sub_more);
            iv_add_more = itemView.findViewById(R.id.iv_add_more);
            tv_add = itemView.findViewById(R.id.tv_add);
            cl_bg = itemView.findViewById(R.id.cl_bg);
            iv_product = itemView.findViewById(R.id.iv_product);
            tv_selling_price = itemView.findViewById(R.id.tv_selling_price);
            tv_cost_price = itemView.findViewById(R.id.tv_cost_price);
            tv_cat_off = itemView.findViewById(R.id.tv_cat_off);
            tv_p_setails = itemView.findViewById(R.id.tv_p_details);
            spinner = itemView.findViewById(R.id.spinner_varient);
            add_to_cart = itemView.findViewById(R.id.card_add_to_cart);
            progressBar = itemView.findViewById(R.id.progress_bar);
        }

        public void bindView(final Productdetail productdetail, final ItemClickListener listener, String itempid) {
            this.listener = listener;
            cl_bg.setOnClickListener(v -> {
                listener.onItemClick(productdetail, ITEM_CLICK, getAdapterPosition(), itempid);
            });

        }

    }

    private void addToCart(final MyViewHolder viewHolder,String pid, String cartId, String qnty, String user_id, final ProgressBar progressBar, int position) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<AddToCartResponse> ProductDetailResponseCall = RestManager.instanceOf().addToCart(pid, cartId, qnty, user_id);
            ProductDetailResponseCall.enqueue(new Callback<AddToCartResponse>() {
                @Override
                public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        AppPref.instance(context).setValue(AppConstants.CART_ID, "" + response.body().getBid());
                        Toast.makeText(context, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        listener.onItemClick(null,VALUE_UPDATED,position,"0");
                            viewHolder.iv_add_more.setEnabled(true);
                            viewHolder.cartQntt=1;
                            viewHolder.tv_add.setText("" + viewHolder.cartQntt);

                    }
                }

                @Override
                public void onFailure(Call<AddToCartResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateCart(final MyViewHolder viewHolder,String pid, String cartId, String action, String user_id, final ProgressBar progressBar, int position) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<UpdateCartProductResponse> ProductDetailResponseCall = RestManager.instanceOf().updateCartProduct(pid, cartId, action, user_id);
            ProductDetailResponseCall.enqueue(new Callback<UpdateCartProductResponse>() {
                @Override
                public void onResponse(Call<UpdateCartProductResponse> call, Response<UpdateCartProductResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        Toast.makeText(context, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        Productdetail productdetail = productdetailList.get(position);
                        productdetail.setQty("" + response.body().getQty());
                        productdetailList.set(position, productdetail);
                        listener.onItemClick(null,VALUE_UPDATED,position,"0");
                        notifyDataSetChanged();
                        viewHolder.iv_add_more.setEnabled(true);
                        viewHolder.iv_sub_more.setEnabled(true);
                        viewHolder.cartQntt = response.body().getQty();
                        viewHolder.tv_add.setText("" + viewHolder.cartQntt);
                    }
                }

                @Override
                public void onFailure(Call<UpdateCartProductResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteFromCart(String pid, String cartId, final ProgressBar progressBar, final int position) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<CartDeleteProductrsponse> ProductDetailResponseCall = RestManager.instanceOf().deleteCartProduct(pid, cartId);
            ProductDetailResponseCall.enqueue(new Callback<CartDeleteProductrsponse>() {
                @Override
                public void onResponse(Call<CartDeleteProductrsponse> call, Response<CartDeleteProductrsponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        Toast.makeText(context, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        listener.onItemClick(null,VALUE_UPDATED,position,"0");
                        removeItemFromList(position);
                    }
                }

                @Override
                public void onFailure(Call<CartDeleteProductrsponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }

    public interface ItemClickListener {
        void onItemClick(Productdetail productdetail, int clickedFor, int position, String activePid);
    }
}
