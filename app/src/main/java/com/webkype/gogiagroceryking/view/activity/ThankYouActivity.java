package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.view.adapters.AddressesAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ThankYouActivity extends AppCompatActivity {
    @BindView(R.id.iv_back)
    ImageView back;

    @BindView(R.id.tv_continue_shopping)
    TextView tv_continue_shopping;
    @BindView(R.id.tv_check_order_stat)
    TextView tv_check_order_stat;


//    @BindView(R.id.rv_my_addresses)
//    RecyclerView rv_my_addresses;
//    AddressesAdapter topCategoryAdapter;
    Context context;
    private String backFor = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        ButterKnife.bind(this);
        context = this;
        backFor = "";
//        topCategoryAdapter = new AddressesAdapter(context, new AddressesAdapter.AddressItemClickListener() {
//            @Override
//            public void onItemClick(Object model, int clickedFor) {
//
//            }
//        });
        tv_check_order_stat.setOnClickListener(v -> {
            backFor = "orderstat";
            onBackPressed();
        });
        tv_continue_shopping.setOnClickListener(v -> {
            backFor = "homeCatListItemAdapter";
            onBackPressed();

        });

        back.setOnClickListener(v -> {
            finish();
        });
//        rv_my_addresses.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
//        rv_my_addresses.setAdapter(topCategoryAdapter);
//        rv_my_addresses.setFocusable(false);
    }

    @Override
    public void onBackPressed() {
        if (backFor.isEmpty()) {
            super.onBackPressed();
        } else if (backFor.equals("homeCatListItemAdapter")) {
            this.finishAffinity();
            startActivity(new Intent(context, HomeActivity.class));
        }else if (backFor.equals("orderstat")){
            this.finishAffinity();
            Intent intent=new Intent(context, MyOrdersActivity.class);
            intent.putExtra("from","thanks");
            startActivity(intent);
        }
    }
}
