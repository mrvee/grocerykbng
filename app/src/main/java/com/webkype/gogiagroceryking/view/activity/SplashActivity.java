package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.view.activity.new_design.IntroActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        context = this;
        Glide.with(context).load(getResources().getDrawable(R.drawable.back_image));
        progressBar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                progressBar.setVisibility(View.GONE);
                startActivity(new Intent(context, LoginActivity.class));
//                if (AppPref.instance(context).getValue(AppConstants.USER_ID).isEmpty()) {
//                    startActivity(new Intent(context, LoginActivity.class));
//                    finish();
//                } else {
//                    startActivity(new Intent(context, HomeActivity.class));
//                    finish();
//                }

            }
        }, 2000);
    }
}
