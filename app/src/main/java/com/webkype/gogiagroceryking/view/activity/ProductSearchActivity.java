package com.webkype.gogiagroceryking.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.controller.utils.CartQuantity;
import com.webkype.gogiagroceryking.model.api_models.products_response.Productdetail;
import com.webkype.gogiagroceryking.model.api_models.products_response.ProductsResponse;
import com.webkype.gogiagroceryking.view.adapters.ProductListAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductSearchActivity extends AppCompatActivity {
    private final String TAG = ProductSearchActivity.class.getName();

    @BindView(R.id.iv_back)
    ImageView back;

    @BindView(R.id.iv_serch)
    ImageView iv_serch;

    @BindView(R.id.et_search_key)
    EditText et_search_key;

    @BindView(R.id.tv_cart_qnt)
    TextView tv_cart_qnt;

    @BindView(R.id.tv_no_product_found)
    TextView tv_no_product_found;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.product_list)
    RecyclerView recyclerView;

    Context context;
    private String cartId = "";
    private String catName = "";

    ProductListAdapter catItemListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_search);
        ButterKnife.bind(this);
        context = this;
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        back.setOnClickListener(v -> {
            finish();
        });

        catItemListAdapter = new ProductListAdapter(context, new ProductListAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Productdetail productdetail, int clickedFor, int position, String activePid) {
                if (clickedFor == ProductListAdapter.ITEM_CLICK) {
                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra(AppConstants.PID_ID, activePid);
                    context.startActivity(intent);
                }
                if (clickedFor == ProductListAdapter.VALUE_UPDATED) {
                    CartQuantity.setQuantity(context, tv_cart_qnt);
                }
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(catItemListAdapter);

        et_search_key.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchKey = et_search_key.getText().toString();
                    if (searchKey.isEmpty()) {
                        Toast.makeText(context, "Please enter some key first", Toast.LENGTH_SHORT).show();
                    }else {
                        getProductsList("",cartId,catItemListAdapter,searchKey);
                       hideKeyboard(ProductSearchActivity.this);
                    }
                    return true;
                }
                return false;
            }
        });
        iv_serch.setOnClickListener(v -> {
            String searchKey = et_search_key.getText().toString();
            if (searchKey.isEmpty()) {
                Toast.makeText(context, "Please enter some key first", Toast.LENGTH_SHORT).show();
            }else {
                getProductsList("",cartId,catItemListAdapter,searchKey);
            }
        });

    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void getProductsList(String cid, String cartId, ProductListAdapter catItemListAdapter, String serchKey) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<ProductsResponse> homeResponseCall = RestManager.instanceOf().getProducts(cid,"",cartId,"",serchKey);
            homeResponseCall.enqueue(new Callback<ProductsResponse>() {
                @Override
                public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        List<Productdetail> topbanners = response.body().getProductdetails();
                        catItemListAdapter.setData(topbanners);
                        if (topbanners==null){
                            catItemListAdapter.setData(null);
                            tv_no_product_found.setVisibility(View.VISIBLE);
                            tv_no_product_found.setText("No product found");
                        }else {
                            tv_no_product_found.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProductsResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        CartQuantity.setQuantity(context, tv_cart_qnt);
    }

}
