package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.api_models.home_response.Banner;

import java.util.List;

public class SliderPagerAdapter extends PagerAdapter {

    private Context mContext;
    List<Banner> imageUrls;

    public SliderPagerAdapter(Context mContext/*, List<Banner> testimonialList*/) {
        this.mContext = mContext;
//        this.imageUrls = testimonialList;
    }

    public void setData(List<Banner> thirdBanner) {
        this.imageUrls = thirdBanner;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_slider, container, false);
        ImageView im_slider = view.findViewById(R.id.imageView);

        CardView card_bg = view.findViewById(R.id.card_bg);
        String url = imageUrls.get(position).getImageurl();
        if (url != null) {
            Glide.with(mContext).load(url).into(im_slider);
        }
        card_bg.setOnClickListener(v -> { });
        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        if (imageUrls != null) {
            return imageUrls.size();
        }
        return 0;

    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
