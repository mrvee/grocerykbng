package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.SupportModel;
import com.webkype.gogiagroceryking.view.adapters.SupportQuestionListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomerSupportActivity extends AppCompatActivity {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.rv_qns)
    RecyclerView rv_support;
    SupportQuestionListAdapter questionListAdapter;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_support);
        context = this;
        ButterKnife.bind(this);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        questionListAdapter = new SupportQuestionListAdapter(context);
        rv_support.setLayoutManager(new LinearLayoutManager(context));
        rv_support.setAdapter(questionListAdapter);
        questionListAdapter.setData(getList());
    }

    private List<SupportModel> getList() {
        List<SupportModel> list = new ArrayList<>();
        list.add(new SupportModel("1", "How can i make payment to grofferking?"));
        list.add(new SupportModel("2", "How can i make payment to grofferking?"));
        list.add(new SupportModel("3", "How can i make payment to grofferking?"));
        list.add(new SupportModel("4", "How can i make payment to grofferking?"));
        list.add(new SupportModel("5", "How can i make payment to grofferking?"));
        list.add(new SupportModel("6", "How can i make payment to grofferking?"));
        list.add(new SupportModel("7", "How can i make payment to grofferking?"));
        list.add(new SupportModel("8", "How can i make payment to grofferking?"));
        list.add(new SupportModel("9", "How can i make payment to grofferking?"));
        list.add(new SupportModel("10", "How can i make payment to grofferking?"));
        list.add(new SupportModel("11", "How can i make payment to grofferking?"));
        return list;
    }
}
