package com.webkype.gogiagroceryking.view.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.webkype.gogiagroceryking.R;


import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

public class RaiseIssueBottomFragment extends BottomSheetDialogFragment {

    @BindView(R.id.visitStatusSpinner)
    Spinner visitStatusSpinner;
    @BindView(R.id.rl_select_document)
    RelativeLayout rl_select_document;

    @BindView(R.id.tv_document)
    TextView tv_document;
    @BindView(R.id.iv_document)
    ImageView iv_document;

    @BindView(R.id.card_submit)
    CardView card_submit;

    private Context context;

    private static final int CAMERA_REQUEST = 1888;
    private String userDocumentPath = "";
    private int SELECT_IMAGE_REQ = 100;
    private ArrayList<String> filePaths = new ArrayList<>();
    private final int CODE_FOR_READ_PERMISSION = 101;
    private final int CODE_FOR_WRITE_PERMISSION = 102;

    public static RaiseIssueBottomFragment newInstance() {

        Bundle args = new Bundle();

        RaiseIssueBottomFragment fragment = new RaiseIssueBottomFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.raise_isuue_layout, container, false);
        context = getContext();
        ButterKnife.bind(this, view);

        LinkedList<String> cat = new LinkedList<>();
        cat.add("--- Available ---");
        cat.add("Booking 1");
        cat.add("Booking 1");
        ArrayAdapter vadapter = new ArrayAdapter(context, R.layout.spinner_items, cat);
        visitStatusSpinner.setAdapter(vadapter);
        rl_select_document.setOnClickListener(v -> {
            requestStoragePermission();
        });
        card_submit.setOnClickListener(v -> {
            dismiss();
            Toast.makeText(context, "Issue Submitted", Toast.LENGTH_SHORT).show();
        });
        return view;
    }

    private void requestStoragePermission() {
        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            openChooser();
                            // Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        } else if (!report.areAllPermissionsGranted()) {

                            Toast.makeText(context, "Please Grant All Permissions Upload Document",
                                    Toast.LENGTH_LONG).show();
                        }
                        // check for permanent denial of any permission
//                        if (report.isAnyPermissionPermanentlyDenied()) {
//                            // show alert dialog navigating to Settings
//                            requestStoragePermission();
//                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(context, "Error occurred! " + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void openChooser() {
        final CharSequence options[] = new CharSequence[]{"Capture Image", "Pick Image From Gallery"};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (i == 0) {
                    //open Camera...................................
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);

                } else if (i == 1) {
                    //open Gallery...............................
                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setSelectedFiles(filePaths)
                            .setActivityTheme(R.style.LibAppTheme)
                            .pickPhoto(getActivity());
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {

                    filePaths.clear();
                    filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                    tv_document.setVisibility(View.GONE);
                    iv_document.setVisibility(View.VISIBLE);
                    Glide.with(context).load(filePaths.get(0)).into(iv_document);
                    userDocumentPath = filePaths.get(0);
                    Toast.makeText(context, userDocumentPath, Toast.LENGTH_SHORT).show();
                }
                break;

            case CAMERA_REQUEST: {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    filePaths.clear();
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    Uri tempUri = getImageUri(context, photo);
                    String path = getRealPathFromURI(tempUri);
                    filePaths.add(path);
                    if (filePaths.size() != 0) {
                        tv_document.setVisibility(View.GONE);
                        iv_document.setVisibility(View.VISIBLE);
                        Glide.with(context).load(filePaths.get(0)).into(iv_document);
                        userDocumentPath = filePaths.get(0);
                        Toast.makeText(getContext(), userDocumentPath, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}
