package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.view.adapters.AddressesAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyAddressActivity extends AppCompatActivity {
    @BindView(R.id.iv_back)
    ImageView back;

    @BindView(R.id.ll_add_new_address)
    LinearLayout ll_add_new_address;

    @BindView(R.id.rv_my_addresses)
    RecyclerView rv_my_addresses;
    AddressesAdapter baskitOfferAdapter;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_address);
        ButterKnife.bind(this);
        context = this;
        baskitOfferAdapter = new AddressesAdapter(context, new AddressesAdapter.AddressItemClickListener() {
            @Override
            public void onItemClick(Object model, int clickedFor) {
                if (clickedFor==AddressesAdapter.DELETE_CLICK){
                    askToDeletAddress();
                }

            }
        });
        ll_add_new_address.setOnClickListener(v -> {
            Intent intent = new Intent(context, AddNewAddressActivity.class);
            intent.putExtra("action", "new");
            startActivity(intent);
        });

        back.setOnClickListener(v -> {
            finish();
        });
        rv_my_addresses.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rv_my_addresses.setAdapter(baskitOfferAdapter);
        rv_my_addresses.setFocusable(false);
    }

    private void askToDeletAddress() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setTitle("Do you want to remove the this address?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                deletAddress();
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    private void deletAddress() {
        Toast.makeText(context, "Deleted Successfully", Toast.LENGTH_SHORT).show();
    }
}
