package com.webkype.gogiagroceryking.view.activity.new_design;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddBalanceToWalletActivity extends AppCompatActivity {
    @BindView(R.id.backPayment)
    ImageView backPayment;
    @BindView(R.id.tv_activity_header)
    TextView tv_activity_header;

    @BindView(R.id.webView1)
    WebView webView1;
    private String url;

    private String userId="",amount="";
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_balance_to_wallet);
        ButterKnife.bind(this);
        context=this;
        amount = AppPref.instance(context).getValue(AppConstants.CART_ID);
        userId = AppPref.instance(context).getValue(AppConstants.USER_ID);

        if (getIntent()!=null){
            amount = getIntent().getStringExtra(AppConstants.ADD_WALLET_AMOUNT);
        }

        tv_activity_header.setText("Add Balance");
        backPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        url = AppConstants.WALLET_ADD_MONEY+"?user_id="+userId+"&amount="+amount;
        Log.d("a", "onCreate: " + url);
        loadWebViewLoad(webView1);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void loadWebViewLoad(WebView webview) {
        final ProgressDialog pd = ProgressDialog.show(context, "", "Please wait...", true);

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setSupportMultipleWindows(true);
        webview.setWebViewClient(new WebViewClient(){
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(context, description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                pd.show();
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                pd.dismiss();
            }
        });
        webview.setWebChromeClient(new WebChromeClient());
        webview.loadUrl(url);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
