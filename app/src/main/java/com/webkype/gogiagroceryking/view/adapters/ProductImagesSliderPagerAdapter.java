package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;

import java.util.List;

public class ProductImagesSliderPagerAdapter extends PagerAdapter {

    private Context mContext;
    List<String> imageUrls;

    public ProductImagesSliderPagerAdapter(Context mContext/*, List<Object testimonialList*/) {
        this.mContext = mContext;
//        this.testimonialList = testimonialList;
    }

    public void setData(List<String> imagesList) {
                this.imageUrls = imagesList;
                notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.product_images_image_layout, container, false);
        ImageView iv_product_image = view.findViewById(R.id.iv_product_image);
        String url = imageUrls.get(position);
        if (url != null) {
            Glide.with(mContext).load(url).into(iv_product_image);
        }
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        if (imageUrls != null) {
            return imageUrls.size();
        }
        return 0;
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
