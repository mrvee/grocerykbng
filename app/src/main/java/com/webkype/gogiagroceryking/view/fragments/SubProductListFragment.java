package com.webkype.gogiagroceryking.view.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppComonValues;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.products_response.Productdetail;
import com.webkype.gogiagroceryking.model.api_models.products_response.ProductsResponse;
import com.webkype.gogiagroceryking.view.activity.ProductDetailActivity;
import com.webkype.gogiagroceryking.view.activity.SubCategoryActivity;
import com.webkype.gogiagroceryking.view.adapters.ProductListAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubProductListFragment extends Fragment {
    private final String TAG = SubProductListFragment.class.getName();
    @BindView(R.id.rv_sub_cat)
    RecyclerView recyclerView;
    private View view;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private String cat_id = "";
    private String cartId = "";
    private String serchKey="";
    private Context context;
    ProductListAdapter catItemListAdapter;
    SubCategoryActivity subCategoryActivity;
    public SubProductListFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public SubProductListFragment(SubCategoryActivity subCategoryActivity) {
       this.subCategoryActivity =subCategoryActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sub_product_list, container, false);
        ButterKnife.bind(this, view);
        AppComonValues.variantCartCount.clear();
        context = getContext();
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        if (getArguments() != null) {
            cat_id = getArguments().getString(AppConstants.Cat_ID);
        }
        catItemListAdapter = new ProductListAdapter(getContext(), new ProductListAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Productdetail productdetail, int clickedFor, int position,String activePid) {
                if (clickedFor == ProductListAdapter.ITEM_CLICK) {
                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra(AppConstants.PID_ID, activePid);
                    context.startActivity(intent);
                }
                if (clickedFor == ProductListAdapter.VALUE_UPDATED) {
                    showVarientsDialog();
                    subCategoryActivity.cartAPI();
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(catItemListAdapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getProductsList(cat_id,cartId,catItemListAdapter,serchKey);
    }

    private void showVarientsDialog() {

    }

    private void getProductsList(String cid, String cartId, ProductListAdapter catItemListAdapter, String serchKey) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<ProductsResponse> homeResponseCall = RestManager.instanceOf().getProducts(cid,"",cartId,"",serchKey);
            homeResponseCall.enqueue(new Callback<ProductsResponse>() {
                @Override
                public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        List<Productdetail> topbanners = response.body().getProductdetails();
                        catItemListAdapter.setData(topbanners);
                    }
                }

                @Override
                public void onFailure(Call<ProductsResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }
}
