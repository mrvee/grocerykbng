package com.webkype.gogiagroceryking.view.activity.new_design;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.Api;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.OrderShippingSlot;
import com.webkype.gogiagroceryking.model.api_models.orderReview.OrderReview;
import com.webkype.gogiagroceryking.model.api_models.orderReview.ShippingaddressReview;
import com.webkype.gogiagroceryking.model.api_models.orderReview.Shippingtype;
import com.webkype.gogiagroceryking.view.adapters.ProductListOrderReviewAdapter;
import com.webkype.gogiagroceryking.view.adapters.TimeSlotAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderReviewActivity extends AppCompatActivity {
    private static final String TAG = OrderReviewActivity.class.getName();
    @BindView(R.id.backOrderReview)
    ImageView backOrderReview;

    @BindView(R.id.editAddress_iV)
    ImageView editAddress_iV;
    @BindView(R.id.fullNameReview_tV)
    TextView fullNameReview_tV;
    @BindView(R.id.addressReview_tV)
    TextView addressReview_tV;
    @BindView(R.id.mobileReview_tV)
    TextView mobileReview_tV;

    @BindView(R.id.orderReviewRecycler)
    RecyclerView orderReviewRecycler;

    private ProductListOrderReviewAdapter recyclerAdapterOrderReview;

    @BindView(R.id.subTotalReview_tV)
    TextView subTotalReview_tV;
    @BindView(R.id.shipChargeReview_tV)
    TextView shipChargeReview_tV;
    @BindView(R.id.totalPayableReview_tV)
    TextView totalPayableReview_tV;
    /*May be Hidden*/
    @BindView(R.id.couponLayout)
    RelativeLayout couponLayout;
    @BindView(R.id.shipCouponReview_tV)
    TextView shipCouponReview_tV;
    @BindView(R.id.registrationLayout)
    RelativeLayout registrationLayout;
    @BindView(R.id.shipRegistrationReview_tV)
    TextView shipRegistrationReview_tV;
    @BindView(R.id.walletLayout)
    RelativeLayout walletLayout;
    @BindView(R.id.shipWalletReview_tV)
    TextView shipWalletReview_tV;
    /*============*/


    @BindView(R.id.tv_select_time)
    TextView tv_select_time;

    @BindView(R.id.calView)
    RelativeLayout calView;
    @BindView(R.id.textDate)
    TextView textDate;
    @BindView(R.id.calender)
    ImageView calender;
    @BindView(R.id.deliverySlotRecycler)
    RecyclerView deliverySlotRecycler;
    private TimeSlotAdapter timeSlotAdapter;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.online_radioButton)
    RadioButton online_radioButton;
    @BindView(R.id.cod_radioButton)
    RadioButton cod_radioButton;
    @BindView(R.id.paytm_radioButton)
    RadioButton paytm_radioButton;

    @BindView(R.id.placeOrder_tV)
    TextView placeOrder_tV;

    @BindView(R.id.slotText)
    public TextView slotText;

    @BindView(R.id.progress_bar)
    ProgressBar progressDialog;


    @BindView(R.id.scrollView)
    NestedScrollView scrollView;

    private String date, payType = "", slotId = "";
    private Context context;
    private String userID = "", cartID = "";
    private boolean isfistTimeOpened = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_review);
        ButterKnife.bind(this);
        context = this;
        isfistTimeOpened = true;
        userID = AppPref.instance(context).getValue(AppConstants.USER_ID);
        cartID = AppPref.instance(context).getValue(AppConstants.CART_ID);
        deliverySlotRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        orderReviewRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        showData();
        backOrderReview.setOnClickListener(v -> {
            finish();
        });
        calView.setOnClickListener(v -> {
            openCalenderView();
        });

        tv_select_time.setOnClickListener(v -> {
            openCalenderView();
        });

        editAddress_iV.setOnClickListener(v -> {
            AppPref.instance(context).setFlagValue("0");
            Intent intent = new Intent(context, ShippingActivity.class);
            startActivity(intent);
        });

        timeSlotAdapter = new TimeSlotAdapter(context, new TimeSlotAdapter.SmallItemClickListener() {
            @Override
            public void onItemClick(Shippingtype model, int clickedFor, String paymentSlot) {
                if (clickedFor == TimeSlotAdapter.SLOT_SELECTED) {
                    slotId = paymentSlot;
                }
            }
        });

        placeOrder_tV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (slotId.isEmpty()) {
                    Toast.makeText(OrderReviewActivity.this, "Please select time slot", Toast.LENGTH_SHORT).show();
                    openCalenderView();
                } else if (!cod_radioButton.isChecked()) {
                    Toast.makeText(OrderReviewActivity.this, "Please select pay option", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intentPayment = new Intent(OrderReviewActivity.this, PaymentActivity.class);
                    intentPayment.putExtra(AppConstants.SHIPPING_TYPE, slotId);
                    startActivity(intentPayment);
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    Log.d(TAG, ": x :" + scrollX+"y : "+scrollY);
                    if (isViewVisible(tv_select_time)) {
                        if (isfistTimeOpened) {
                            openCalenderView();
                            isfistTimeOpened = false;
                        }
                        Log.d(TAG, ": focus :" + isViewVisible(tv_select_time));
                    } else {
                        Log.d(TAG, ": focus :" + isViewVisible(tv_select_time));
                    }
                }
            });
        }
    }

    private boolean isViewVisible(View view) {
        Rect scrollBounds = new Rect();
        scrollView.getDrawingRect(scrollBounds);

        float top = view.getY();
        float bottom = top + view.getHeight();

        if (scrollBounds.top < top && scrollBounds.bottom > bottom) {
            return true;
        } else {
            return false;
        }
    }

    private void openCalenderView() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderReviewActivity.this);
        View dialogView = LayoutInflater.from(OrderReviewActivity.this).inflate(R.layout.preview_calender_dialog, null);
        dialogBuilder.setView(dialogView);
        final CalendarView simpleCalendarView = dialogView.findViewById(R.id.simpleCalendarView);
        TextView okButton = dialogView.findViewById(R.id.okButton);
        simpleCalendarView.setDate(System.currentTimeMillis());
        simpleCalendarView.setShowWeekNumber(true);
        simpleCalendarView.setMinDate(Calendar.getInstance().getTimeInMillis());
        simpleCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                if (month < 9) {
                    date = year + "-0" + (month + 1) + "-" + dayOfMonth;
                } else {
                    if (dayOfMonth < 10) {
                        date = year + "-" + (month + 1) + "-0" + dayOfMonth;
                    } else {
                        date = year + "-" + (month + 1) + "-" + dayOfMonth;
                    }
                }
                Log.d("tag", "onSelectedDayChange: " + year + " month is: " + month + "date is: " + dayOfMonth);
            }
        });


        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if (date == null) {
                    Date c = Calendar.getInstance().getTime();

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String formattedDate = df.format(c);
                    date = formattedDate;

                    showShippingSlot(date);
                    textDate.setText("Delivery Date : " + date);
                } else {
                    showShippingSlot(date);
                    textDate.setText("Delivery Date : " + date);
                }
                Log.d("tag", "onClick: " + date);
            }
        });
    }

    private void showShippingSlot(final String date) {

        progressDialog.setVisibility(View.VISIBLE);
        Api api = RestManager.instanceOf();
        Call<OrderReview> call = api.getOrderReview(userID,
                cartID, date);
        call.enqueue(new Callback<OrderReview>() {
            @Override
            public void onResponse(Call<OrderReview> call, Response<OrderReview> response) {
                OrderReview orderReview = response.body();
                if (orderReview.getStatus().equals("200")) {
                    progressDialog.setVisibility(View.GONE);
                    if (orderReview.getShippingtype() != null && !orderReview.getShippingtype().isEmpty()) {

                        deliverySlotRecycler.setAdapter(timeSlotAdapter);
                    }
                    timeSlotAdapter.setServices(orderReview.getShippingtype());
                  /*  if(orderReview.getShippingprice() != null && !orderReview.getShippingprice().isEmpty() && !orderReview.getShippingprice().equals("0") ){
                        shipChargeReview_tV.setText("₹ "+orderReview.getShippingprice());
                    } else{
                        shipChargeReview_tV.setText("FREE");
                    }*/

                } else {
                    //null
                    progressDialog.setVisibility(View.GONE);
                    deliverySlotRecycler.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<OrderReview> call, Throwable t) {
                progressDialog.setVisibility(View.GONE);

            }
        });
    }

    public void showOrderShippingCharges(final String shipId) {
        progressDialog.setVisibility(View.VISIBLE);
        Api api = RestManager.instanceOf();
        Call<OrderShippingSlot> call = api.getShipCharges(userID,
                cartID, shipId);
        call.enqueue(new Callback<OrderShippingSlot>() {
            @Override
            public void onResponse(Call<OrderShippingSlot> call, Response<OrderShippingSlot> response) {
                OrderShippingSlot orderShippingSlot = response.body();
                if (orderShippingSlot.getStatus().equals("200")) {
                    progressDialog.setVisibility(View.GONE);


                    subTotalReview_tV.setText("₹ " + orderShippingSlot.getSubtotal());
                    if (orderShippingSlot.getCoupondiscount() != null && !orderShippingSlot.getCoupondiscount().equals("0")) {
                        shipCouponReview_tV.setText("₹ " + orderShippingSlot.getCoupondiscount());
                    } else {
                        couponLayout.setVisibility(View.GONE);
                    }

                    if (orderShippingSlot.getShippingprice() != null && !orderShippingSlot.getShippingprice().isEmpty() && !orderShippingSlot.getShippingprice().equals("0")) {
                        shipChargeReview_tV.setText("₹ " + orderShippingSlot.getShippingprice());
                    } else {
                        shipChargeReview_tV.setText("FREE");
                    }

                    if (orderShippingSlot.getUserWallet() != null && !orderShippingSlot.getUserWallet().isEmpty() && !orderShippingSlot.getUserWallet().equals("0")) {
                        shipWalletReview_tV.setText("₹ " + orderShippingSlot.getUserWallet());
                    } else {
                        walletLayout.setVisibility(View.GONE);
                    }

                    if (orderShippingSlot.getRegWalletDiscount() != null && !orderShippingSlot.getRegWalletDiscount().isEmpty() && !orderShippingSlot.getRegWalletDiscount().equals("0")) {
                        shipRegistrationReview_tV.setText("₹ " + orderShippingSlot.getRegWalletDiscount());
                    } else {
                        registrationLayout.setVisibility(View.GONE);
                    }

                    totalPayableReview_tV.setText("₹ " + orderShippingSlot.getTotalcost());
                } else {
                    //null
                    progressDialog.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<OrderShippingSlot> call, Throwable t) {
                progressDialog.setVisibility(View.GONE);

            }
        });
    }

    private void showData() {
        progressDialog.setVisibility(View.VISIBLE);
        Api api = RestManager.instanceOf();
        Call<OrderReview> call = api.getOrderReview(userID,
                cartID, "");
        call.enqueue(new Callback<OrderReview>() {
            @Override
            public void onResponse(Call<OrderReview> call, Response<OrderReview> response) {
                OrderReview orderReview = response.body();
                if (orderReview.getStatus().equals("200")) {
                    progressDialog.setVisibility(View.GONE);
                    if (orderReview.getShippingaddress() != null && !orderReview.getShippingaddress().isEmpty()) {
                        for (ShippingaddressReview shippingAddressReview : orderReview.getShippingaddress()) {
                            fullNameReview_tV.setText(shippingAddressReview.getCustomername());
                            addressReview_tV.setText(shippingAddressReview.getCustomeraddress());
                            mobileReview_tV.setText("Mobile No : " + shippingAddressReview.getCustomermobile());
                        }
                    }

                    subTotalReview_tV.setText("₹ " + orderReview.getSubtotal());
                    if (orderReview.getCoupondiscount() != null && !orderReview.getCoupondiscount().equals("0")) {
                        shipCouponReview_tV.setText("₹ " + orderReview.getCoupondiscount());
                    } else {
                        couponLayout.setVisibility(View.GONE);
                    }

                    if (orderReview.getShippingprice() != null && !orderReview.getShippingprice().isEmpty() && !orderReview.getShippingprice().equals("0")) {
                        shipChargeReview_tV.setText("₹ " + orderReview.getShippingprice());
                    } else {
                        shipChargeReview_tV.setText("FREE");
                    }

                    if (orderReview.getUserWallet() != null && !orderReview.getUserWallet().isEmpty() && !orderReview.getUserWallet().equals("0")) {
                        shipWalletReview_tV.setText("₹ " + orderReview.getUserWallet());
                        walletLayout.setVisibility(View.VISIBLE);
                    } else {
                        walletLayout.setVisibility(View.GONE);
                    }

                    if (orderReview.getRegWalletDiscount() != null && !orderReview.getRegWalletDiscount().isEmpty() && !orderReview.getRegWalletDiscount().equals("0")) {
                        shipRegistrationReview_tV.setText("₹ " + orderReview.getRegWalletDiscount());
                    } else {
                        registrationLayout.setVisibility(View.GONE);
                    }

                    totalPayableReview_tV.setText("₹ " + orderReview.getTotalcost());

                    if (orderReview.getProductdetails() != null && !orderReview.getProductdetails().isEmpty()) {
                        recyclerAdapterOrderReview = new ProductListOrderReviewAdapter(context, orderReview.getProductdetails());
                        orderReviewRecycler.setAdapter(recyclerAdapterOrderReview);
                        recyclerAdapterOrderReview.notifyDataSetChanged();
                    }

                } else {
                    progressDialog.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<OrderReview> call, Throwable t) {
                progressDialog.setVisibility(View.GONE);

            }
        });
    }
}
