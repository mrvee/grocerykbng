package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.api_models.category_response.Topcategory;

import java.util.List;


public class SmallItemAdapter extends RecyclerView.Adapter<SmallItemAdapter.MyViewHolder> {
    public static final int ITEM_CLICK=100;
    private LayoutInflater inflater;
    private Context context;
    List<Topcategory> topcategoryList;
    private SmallItemClickListener listener;

    public SmallItemAdapter(Context context, SmallItemAdapter.SmallItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    public void setDataList(List<Topcategory> imagelist) {
        this.topcategoryList = imagelist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.item_grocery, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
        final Topcategory topcategory = topcategoryList.get(i);
        if (topcategory.getImageurl()!=null){
            Glide.with(context).load(topcategory.getImageurl()).into(vh.iv_cat_image);
        }
        vh.tv_cat_title.setText(topcategory.getCategoryname());
        vh.tv_cat_off_message.setText(topcategory.getCategorydesc());
        vh.bind(topcategory, listener);
    }

    @Override
    public int getItemCount() {
        return topcategoryList != null ? topcategoryList.size() : 0;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_offers, iv_cat_image;
        private TextView tv_cat_title,tv_cat_off_message;
        private RelativeLayout rl_card_bg;
        private ConstraintLayout constraint_bg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            rl_card_bg = itemView.findViewById(R.id.rl_card_bg);
            tv_cat_title = itemView.findViewById(R.id.tv_cat_title);
            tv_cat_off_message = itemView.findViewById(R.id.tv_cat_off_message);
            iv_cat_image = itemView.findViewById(R.id.iv_cat_image);
        }

        public void bind(Topcategory object, SmallItemClickListener listener) {
            rl_card_bg.setOnClickListener(v -> {
                listener.onItemClick(object, ITEM_CLICK);
            });
        }

    }

    public interface SmallItemClickListener {
        void onItemClick(Topcategory model, int clickedFor);
    }

}
