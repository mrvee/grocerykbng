package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.orderHistDetail.OrderDetailsModel;
import com.webkype.gogiagroceryking.model.api_models.orderHistDetail.UserOrderDetails;
import com.webkype.gogiagroceryking.view.adapters.ProductListOrderReviewAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends AppCompatActivity {

    private static final String TAG = OrderDetailsActivity.class.getName();
    @BindView(R.id.iv_back)
    ImageView back;

    @BindView(R.id.tv_cancel_order)
    TextView tv_cancel_order;

    @BindView(R.id.tv_placed_on)
    TextView tv_placed_on;
    @BindView(R.id.tv_delivery_date)
    TextView tv_delivery_date;
    @BindView(R.id.tv_payment_method)
    TextView tv_payment_method;
    @BindView(R.id.tv_pay_amt_message)
    TextView tv_pay_amt_message;
    @BindView(R.id.tv_total_items_incart_amt)
    TextView tv_total_quantity_in_cart;
    @BindView(R.id.tv_wallet_discount)
    TextView tv_wallet_discount;
    @BindView(R.id.tv_delivery_charges)
    TextView tv_delivery_charges;
    @BindView(R.id.tv_selling_price)
    TextView tv_to_be_paid_price;
    @BindView(R.id.tv_total_savings)
    TextView tv_total_savings;

    @BindView(R.id.tv_addres_type)
    TextView tv_addres_type;

    @BindView(R.id.tv_address)
    TextView tv_address;


    @BindView(R.id.iv_1)
    ImageView ivPlaced;
    @BindView(R.id.iv_2)
    ImageView ivPacked;
    @BindView(R.id.iv_3)
    ImageView ivOnTheWay;
    @BindView(R.id.iv_4)
    ImageView ivDelivered;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.rv_products_in_order)
    RecyclerView rv_products_in_order;
    ProductListOrderReviewAdapter recyclerAdapterOrderReview;
    Context context;
    private String backFor = "", userID = "";
    private String orderId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);
        context = this;
        backFor = "";
        userID = AppPref.instance(context).getValue(AppConstants.USER_ID);
        if (getIntent() != null) {
            orderId = getIntent().getStringExtra(AppConstants.ORDER_ID);
        }

        back.setOnClickListener(v -> {
            finish();
        });
//        topCategoryAdapter = new AddressesAdapter(context, new AddressesAdapter.AddressItemClickListener() {
//            @Override
//            public void onItemClick(Object model, int clickedFor) {
//
//            }
//        });
//        tv_check_order_stat.setOnClickListener(v -> {
//            backFor = "orderstat";
//            onBackPressed();
//        });

        rv_products_in_order.setLayoutManager(new LinearLayoutManager(context));
        tv_cancel_order.setOnClickListener(v -> {
            showCancelDialog();
        });

        getOrderDetail();
    }

    private void setStatus(int status) {
        if (status == 1) {
            ivPlaced.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
        }
        if (status == 2) {
            ivPlaced.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
            ivPacked.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
        }

        if (status == 3) {
            ivPlaced.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
            ivPacked.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
            ivOnTheWay.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
        }
        if (status == 4) {
            ivPlaced.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
            ivPacked.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
            ivOnTheWay.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
            ivDelivered.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
        }

    }

    private void showCancelDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setTitle("Do you want to cancel Order?");
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(context, "Order Canceld Sucessfully", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void getOrderDetail() {
        progressBar.setVisibility(View.VISIBLE);

        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            Call<OrderDetailsModel> ProductDetailResponseCall = RestManager.instanceOf().getOrderDetail(userID, orderId);
            ProductDetailResponseCall.enqueue(new Callback<OrderDetailsModel>() {
                @Override
                public void onResponse(Call<OrderDetailsModel> call, Response<OrderDetailsModel> response) {
                    progressBar.setVisibility(View.GONE);

                    OrderDetailsModel detailsModel = response.body();
                    if (detailsModel != null && detailsModel.getStatus().equals("200")) {
                        detailsModel.getOrderdetails();
                        for (UserOrderDetails orderDetail : detailsModel.getOrderdetails()) {
//
                            tv_placed_on.setText("Placed On, " + orderDetail.getOrderdate());
                            tv_delivery_charges.setText("\u20B9" + orderDetail.getShippingcost());
                            tv_wallet_discount.setText("\u20B9" + orderDetail.getUserwallet_discount());
                            tv_pay_amt_message.setText("You have to pay \u20B9 " + orderDetail.getOrdertotal() + " to delivery executive");
                            tv_to_be_paid_price.setText("\u20B9" + orderDetail.getOrdertotal());
                            tv_total_quantity_in_cart.setText(orderDetail.getQuantity());

                            tv_addres_type.setText(orderDetail.getSfname() + " " + orderDetail.getSlname());
                            tv_address.setText(orderDetail.getSaddress()
                                    + "\nLandmark : " + orderDetail.getSlankmark()
                                    + "\nCity : " + orderDetail.getScity()
                                    + "\nState : " + orderDetail.getSstate()
                                    + "\nPinCode : " + orderDetail.getSpincode()
                                    + "\nMob : " + orderDetail.getSphone());

                            if (orderDetail.getOrderstatus().equals("Order Placed")) {
                                setStatus(1);
                            } else if (orderDetail.getOrderstatus().equals("Order Packed")) {
                                setStatus(2);
                            } else if (orderDetail.getOrderstatus().equals("Order on the way")) {
                                setStatus(3);
                            } else if (orderDetail.getOrderstatus().equals("Order Delivered")) {
                                setStatus(4);
                            }
                            if (orderDetail.getProdetails() != null && !orderDetail.getProdetails().isEmpty()) {
                                recyclerAdapterOrderReview = new ProductListOrderReviewAdapter(context, orderDetail.getProdetails());
                                rv_products_in_order.setAdapter(recyclerAdapterOrderReview);
                                recyclerAdapterOrderReview.notifyDataSetChanged();
                            }
                        }

                    } else {

                    }
                }

                @Override
                public void onFailure(Call<OrderDetailsModel> call, Throwable t) {
                    Log.d(TAG, t.getMessage());
                    progressBar.setVisibility(View.GONE);

                }
            });
        } else {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(context, "No network", Toast.LENGTH_SHORT).show();
        }
    }
}
