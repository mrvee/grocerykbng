package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.api_models.orderReview.Shippingtype;
import com.webkype.gogiagroceryking.view.activity.new_design.OrderReviewActivity;

import java.util.List;


public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.MyViewHolder> {
    public static final int SLOT_SELECTED=100;
    private LayoutInflater inflater;
    private Context context;
    List<Shippingtype> shippingtypeList;
    private SmallItemClickListener listener;
    private int seletedItem=-1;

    public TimeSlotAdapter(Context context, TimeSlotAdapter.SmallItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    public void setServices(List<Shippingtype> imagelist) {
        this.shippingtypeList = imagelist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.time_slot_item, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
        final Shippingtype shippingtype = shippingtypeList.get(i);

        if (shippingtype.getShipcheck().equals("0")) {
            holder.tv_slot_name.setText(shippingtype.getShiptype() + " (Slot Full)");
            holder.tv_slot_name.setTextColor(context.getResources().getColor(R.color.red));
            holder.ll_bg.setEnabled(false);
        } else if (shippingtype.getShipcheck().equals("1")) {
            holder.tv_slot_name.setText(shippingtype.getShiptype());
            holder.tv_slot_name.setTextColor(context.getResources().getColor(R.color.black));
            holder.ll_bg.setEnabled(true);
        }

        if (seletedItem==i)
        { vh.iv_select_time_image.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
        }else {
            vh.iv_select_time_image.setImageDrawable(context.getResources().getDrawable(R.drawable.over_shape));
        }

        vh.ll_bg.setOnClickListener(v->{
            if (seletedItem==i){
            }else {
                seletedItem=i;
                listener.onItemClick(null,SLOT_SELECTED,""+seletedItem);
                notifyDataSetChanged();
                ((OrderReviewActivity) context).slotText.setText(shippingtype.getShipid());
                ((OrderReviewActivity) context).showOrderShippingCharges(shippingtype.getShipid());
            }
        });

        vh.bind(shippingtype, listener);
    }

    @Override
    public int getItemCount() {
        return shippingtypeList != null ? shippingtypeList.size() : 0;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_select_time_image;
        private LinearLayout ll_bg;
        private TextView tv_slot_name;
        private ConstraintLayout constraint_bg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_bg = itemView.findViewById(R.id.ll_item_bg);
            tv_slot_name = itemView.findViewById(R.id.tv_slot_name);
            iv_select_time_image = itemView.findViewById(R.id.iv_select_time);
        }

        public void bind(Shippingtype object, SmallItemClickListener listener) {

        }

    }

    public interface SmallItemClickListener {
        void onItemClick(Shippingtype model, int clickedFor,String selectedId);
    }

}
