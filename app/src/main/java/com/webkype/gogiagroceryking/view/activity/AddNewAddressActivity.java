package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.view.adapters.AddressesAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddNewAddressActivity extends AppCompatActivity {
    @BindView(R.id.iv_back)
    ImageView back;

    @BindView(R.id.rb_others)
    RadioButton rb_others;

    @BindView(R.id.til_ownlabl)
    TextInputLayout til_ownlabl;

    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etstreet)
    EditText etstreet;
    @BindView(R.id.etflat)
    EditText etflat;
    @BindView(R.id.etLocal)
    EditText etLocal;

    @BindView(R.id.rg_address_type)
    RadioGroup rg_address_type;
    @BindView(R.id.rg_person_header)
    RadioGroup rg_person_header;

    @BindView(R.id.card_continue)
    CardView card_continue;
    
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);
        ButterKnife.bind(this);
        context = this;
     
        back.setOnClickListener(v->{finish();});
        card_continue.setOnClickListener(v->{finish();
            Toast.makeText(context, "Added Sucessfully ", Toast.LENGTH_SHORT).show();
        });
        rb_others.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    til_ownlabl.setVisibility(View.VISIBLE);
                }else {
                    til_ownlabl.setVisibility(View.GONE);
                }
            }
        });
    }
}
