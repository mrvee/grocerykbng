package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.api_models.orderHist.OrderDetail;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyOredersAdapter extends RecyclerView.Adapter<MyOredersAdapter.MyViewHolder> {
    public static final int CANCELORDER = 1007;
    private LayoutInflater inflater;
    private Context context;
    List<OrderDetail> offerModelsList;
    private SmallItemClickListener listener;

    public MyOredersAdapter(Context context, MyOredersAdapter.SmallItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    public void setServices(List<OrderDetail> imagelist) {
        this.offerModelsList = imagelist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.myorder_item, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
        final OrderDetail orderDetail = offerModelsList.get(i);

        vh.tvTotalPayble.setText("\u20B9 " + orderDetail.getOrdertotal());
        vh.tvOrderdOn.setText("Placed On, " + orderDetail.getOrderdate());
        vh.tvOrderId.setText("Delivery Charges : " + orderDetail.getShippingcost()
                + "\nOrder ID : " + orderDetail.getOrderid()
                + "\nStatus : " + orderDetail.getOrderstatus()
                + "\nTotal Items : " + orderDetail.getQuantity());
        if (orderDetail.getOrderstatus().equals("Order Placed")) {
            vh.setStatus(context, 1);
        }else if (orderDetail.getOrderstatus().equals("Order Packed")) {
            vh.setStatus(context, 2);
        }else if (orderDetail.getOrderstatus().equals("Order on the way")) {
            vh.setStatus(context, 3);
        }else if (orderDetail.getOrderstatus().equals("Order Delivered")) {
            vh.setStatus(context, 4);
        }
//        vh.tvScheduledFor.setText(orderDetail.getOrdertotal());

        vh.bind(orderDetail, listener);
    }

    @Override
    public int getItemCount() {
        return offerModelsList != null ? offerModelsList.size() : 0;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_offers, iv_detail;
        private TextView tvOrderdOn, tvOrderId, tvTotalPayble, tvScheduledFor;

        @BindView(R.id.iv_1)
        ImageView ivPlaced;
        @BindView(R.id.iv_2)
        ImageView ivPacked;
        @BindView(R.id.iv_3)
        ImageView ivOnTheWay;
        @BindView(R.id.iv_4)
        ImageView ivDelivered;

        private CardView rl_card_bg;
        private ConstraintLayout constraint_bg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            rl_card_bg = itemView.findViewById(R.id.card_yes);
//            iv_detail = itemView.findViewById(R.id.iv_detail);
//            constraint_bg = itemView.findViewById(R.id.constraint_bg);
//
            tvOrderdOn = itemView.findViewById(R.id.tv_placed_on);
            tvScheduledFor = itemView.findViewById(R.id.tv_scheduled_for_date);
            tvOrderId = itemView.findViewById(R.id.text_info);
            tvTotalPayble = itemView.findViewById(R.id.tv_total_chargis);
        }

        public void bind(OrderDetail object, SmallItemClickListener listener) {
            rl_card_bg.setOnClickListener(v -> {
                listener.onItemClick(object, CANCELORDER);
            });

        }

        void setStatus(Context context, int status) {
            if (status == 1) {
                ivPlaced.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
            }
            if (status == 2) {
                ivPlaced.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
                ivPacked.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
            }

            if (status == 3) {
                ivPlaced.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
                ivPacked.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
                ivOnTheWay.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
            }
            if (status == 4) {
                ivPlaced.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
                ivPacked.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
                ivOnTheWay.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
                ivDelivered.setImageDrawable(context.getResources().getDrawable(R.drawable.check_icon));
            }

        }
    }

    public interface SmallItemClickListener {
        void onItemClick(OrderDetail model, int clickedFor);
    }

}
