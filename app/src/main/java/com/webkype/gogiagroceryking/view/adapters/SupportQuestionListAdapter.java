package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.model.SupportModel;
import com.webkype.gogiagroceryking.view.activity.SupportDetailActivity;

import java.util.List;

public class SupportQuestionListAdapter extends RecyclerView.Adapter<SupportQuestionListAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private List<SupportModel> models;

    public SupportQuestionListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setData(List<SupportModel> list) {
        this.models = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.question_item_row, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final MyViewHolder holder = (MyViewHolder) myViewHolder;
        final SupportModel model = models.get(i);
        holder.tv_View_Quotation.setText(model.getQuestion());
//        holder.tv_ans.setVisibility(View.GONE);
        holder.rl_bg.setOnClickListener(v -> {
//            float rotation =  holder.iv_detail.getRotation();
//            if (rotation==90){
//                holder.tv_ans.setVisibility(View.VISIBLE);
//                holder.iv_detail.setRotation(-90);
//            }else {
//                holder.tv_ans.setVisibility(View.GONE);
//                holder.iv_detail.setRotation(90);
//            }
            Intent intent = new Intent(context, SupportDetailActivity.class);
            intent.putExtra("q", model.getQuestion());
            intent.putExtra("id", model.getId());
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return models != null ? models.size() : 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_View_Quotation,tv_ans;
        private RelativeLayout rl_bg;
        private ImageView iv_detail;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_View_Quotation = itemView.findViewById(R.id.tv_question);
            rl_bg = itemView.findViewById(R.id.rl_bg);
//            tv_ans =itemView.findViewById(R.id.tv_ans);
//            iv_detail =itemView.findViewById(R.id.iv_detail_indicator);
        }
    }
}
