package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.controller.utils.CartQuantity;
import com.webkype.gogiagroceryking.model.api_models.add_to_cart_response.AddToCartResponse;
import com.webkype.gogiagroceryking.model.api_models.delete_cart_product_res.CartDeleteProductrsponse;
import com.webkype.gogiagroceryking.model.api_models.single_product_detail_resp.Product;
import com.webkype.gogiagroceryking.model.api_models.single_product_detail_resp.ProductDetailResponse;
import com.webkype.gogiagroceryking.model.api_models.update_cart_product_response.UpdateCartProductResponse;
import com.webkype.gogiagroceryking.view.adapters.ProductHighLightsAdapter;
import com.webkype.gogiagroceryking.view.adapters.ProductImagesSliderPagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailActivity extends AppCompatActivity {
    private static final String TAG = ProductDetailActivity.class.getName();
    @BindView(R.id.iv_back)
    ImageView back;
    @BindView(R.id.iv_serch)
    ImageView iv_serch;

    @BindView(R.id.vp_images)
    ViewPager vp_Images;

    @BindView(R.id.rl_cart_at_to_right)
    RelativeLayout rlTopRightCartImageLayout;
    @BindView(R.id.tv_cart_qnt)
    TextView tv_cart_qnt;


    @BindView(R.id.card_bg)
    CardView add_to_cart;
    @BindView(R.id.iv_sub_more)
    ImageView iv_sub_more;
    @BindView(R.id.iv_add_more)
    ImageView iv_add_more;
    @BindView(R.id.tv_add)
    TextView tv_add;

    @BindView(R.id.tv_off)
    TextView tv_off;
    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_selling_price)
    TextView tv_selling_price;

    @BindView(R.id.tv_mrp_price)
    TextView tv_cost_price;

    @BindView(R.id.tv_item_unit)
    TextView tv_item_unit;
    @BindView(R.id.tv_long_description)
    TextView tv_long_description;

     @BindView(R.id.tv_shortdesc)
    TextView tv_shortdesc;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.current_image_indicator)
    CircleIndicator circleIndicator;

    @BindView(R.id.rv_features)
    RecyclerView rv_features;
    @BindView(R.id.tabs)
    TabLayout tabs;
    private final String tabTitles[] = new String[]{"HighLights", "Info"};

    ProductHighLightsAdapter highLightsAdapter;
    ProductImagesSliderPagerAdapter productImagesSliderPagerAdapter;
    Context context;
    private int cartQntt = 0;
    private String pid = "", cartId = "";
    private String useriD="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);
        context = this;
        productImagesSliderPagerAdapter = new ProductImagesSliderPagerAdapter(context);
        highLightsAdapter = new ProductHighLightsAdapter(context);
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        useriD = AppPref.instance(context).getValue(AppConstants.USER_ID);
        if (getIntent() != null) {
            pid = getIntent().getStringExtra(AppConstants.PID_ID);
        }

        back.setOnClickListener(v -> {
            finish();
        });

        rlTopRightCartImageLayout.setOnClickListener(v -> {
            startActivity(new Intent(context, MyCartActivity.class));
        });

        iv_serch.setOnClickListener(v -> {
//           startActivity(new Intent(context,SearchActivity.class));
        });
        tabs.addTab(tabs.newTab().setText(tabTitles[0]));
        tabs.addTab(tabs.newTab().setText(tabTitles[1]));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        rv_features.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rv_features.setAdapter(highLightsAdapter);
        rv_features.setFocusable(false);
        vp_Images.setAdapter(productImagesSliderPagerAdapter);
        circleIndicator.setViewPager(vp_Images);
        vp_Images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (i == 0) {

                } else {
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        add_to_cart.setOnClickListener(v -> {
            cartQntt++;
            iv_sub_more.setVisibility(View.VISIBLE);
            add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
            tv_add.setTextColor(context.getResources().getColor(R.color.black));
            tv_add.setEnabled(true);
            add_to_cart.setEnabled(false);
            tv_add.setText("" + cartQntt);
            addToCart(pid,"1",useriD);
        });

        iv_add_more.setOnClickListener(v -> {
            cartQntt++;
            add_to_cart.setEnabled(false);
            iv_sub_more.setVisibility(View.VISIBLE);
            add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
            tv_add.setTextColor(context.getResources().getColor(R.color.black));
            iv_add_more.setEnabled(false);
            if (cartQntt <=1){
                addToCart(pid,"1",useriD);
            }else {
                updateCart(pid,"add",useriD);

            }
        });

        iv_sub_more.setOnClickListener(v -> {
            if (cartQntt <= 1) {
                cartQntt--;
                iv_sub_more.setVisibility(View.GONE);
                add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.orange));
                tv_add.setTextColor(context.getResources().getColor(R.color.white));
                add_to_cart.setEnabled(true);
                tv_add.setText("Add");
                deleteFromCart(pid);
            } else {
                cartQntt--;
                iv_sub_more.setEnabled(false);
                updateCart(pid,"remove",useriD);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        setCartQuantityToTopAndSideMenu();
        getProductDetail(pid, cartId);
    }

    void setCartQuantityToTopAndSideMenu(){
        CartQuantity.setQuantity(context,tv_cart_qnt);
    }


    private void getProductDetail(String pid, String cartId) {
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<ProductDetailResponse> ProductDetailResponseCall = RestManager.instanceOf().getProductDetails(pid, cartId);
            ProductDetailResponseCall.enqueue(new Callback<ProductDetailResponse>() {
                @Override
                public void onResponse(Call<ProductDetailResponse> call, Response<ProductDetailResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getMsg().equals("Success")) {
                        Product product = response.body().getProduct();

                        tv_title.setText(product.getProductname());
                        tv_shortdesc.setText(product.getShortdescription());
                        tv_long_description.setText(product.getLongdescription());

                        StringTokenizer costPrice =new StringTokenizer(product.getCostprice(),".");
                        StringTokenizer sellingPrice =new StringTokenizer(product.getSellingprice(),".");

                        tv_cost_price.setText("\u20B9" + costPrice.nextToken());
                        tv_selling_price.setText("\u20B9" + sellingPrice.nextToken());
                        if (product.getCostprice().equals(product.getSellingprice())){
                            tv_cost_price.setVisibility(View.GONE);
                        }
                        if (Float.parseFloat(product.getDiscount()) == 0) {
                            tv_off.setVisibility(View.GONE);
                        } else {
                            tv_off.setVisibility(View.VISIBLE);
                            try {
//                                tv_off.setText(new DecimalFormat("##.00").format(product.getDiscount()) + "% OFF");
                                tv_off.setText(product.getDiscount() + "% OFF");
                            }catch (NumberFormatException e){

                            }
                        }
                        String[] imagePaths = product.getImagepath().split(",");
                        ArrayList<String> list = new ArrayList<>(Arrays.asList(imagePaths));

                        productImagesSliderPagerAdapter.setData(list);
                        tv_item_unit.setText(product.getQty()+"g");
                        cartQntt = Integer.parseInt(product.getCartqty());

                        if (cartQntt ==0){
                            iv_sub_more.setVisibility(View.GONE);
                            add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.orange));
                            tv_add.setTextColor(context.getResources().getColor(R.color.white));
                            tv_add.setEnabled(false);
                            add_to_cart.setEnabled(true);
                            tv_add.setText("Add");

                        }else {
                            add_to_cart.setEnabled(false);
                            iv_sub_more.setVisibility(View.VISIBLE);
                            tv_add.setText("" + cartQntt);
                            add_to_cart.setCardBackgroundColor(context.getResources().getColor(R.color.white));
                            tv_add.setTextColor(context.getResources().getColor(R.color.black));
                        }
                    }else {
                        Toast.makeText(context, "Detail not found", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ProductDetailResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }
    private void addToCart(String pid, String qnty, String user_id) {
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<AddToCartResponse> ProductDetailResponseCall = RestManager.instanceOf().addToCart(pid, cartId,qnty,user_id);
            ProductDetailResponseCall.enqueue(new Callback<AddToCartResponse>() {
                @Override
                public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        AppPref.instance(context).setValue(AppConstants.CART_ID,""+response.body().getBid());
                        Toast.makeText(context, ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        setCartQuantityToTopAndSideMenu();
                        iv_add_more.setEnabled(true);
                        iv_sub_more.setEnabled(true);
                        cartQntt=1;
                        tv_add.setText("" + cartQntt);
                    }
                }

                @Override
                public void onFailure(Call<AddToCartResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }
    private void updateCart(String pid, String action, String user_id) {
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<UpdateCartProductResponse> ProductDetailResponseCall = RestManager.instanceOf().updateCartProduct(pid, cartId,action,user_id);
            ProductDetailResponseCall.enqueue(new Callback<UpdateCartProductResponse>() {
                @Override
                public void onResponse(Call<UpdateCartProductResponse> call, Response<UpdateCartProductResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        Toast.makeText(context, ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        setCartQuantityToTopAndSideMenu();
                        iv_add_more.setEnabled(true);
                        iv_sub_more.setEnabled(true);

                        cartQntt = response.body().getQty();
                        tv_add.setText("" + cartQntt);
                    }
                }

                @Override
                public void onFailure(Call<UpdateCartProductResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }
    private void deleteFromCart(String pid) {
        cartId = AppPref.instance(context).getValue(AppConstants.CART_ID);
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            progressBar.setVisibility(View.VISIBLE);
            Call<CartDeleteProductrsponse> ProductDetailResponseCall = RestManager.instanceOf().deleteCartProduct(pid, cartId);
            ProductDetailResponseCall.enqueue(new Callback<CartDeleteProductrsponse>() {
                @Override
                public void onResponse(Call<CartDeleteProductrsponse> call, Response<CartDeleteProductrsponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        Toast.makeText(context, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        setCartQuantityToTopAndSideMenu();
                        cartQntt =0;
                    }
                }

                @Override
                public void onFailure(Call<CartDeleteProductrsponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, t.getMessage());
                }
            });
        } else {
            Toast.makeText(context, "Try again later", Toast.LENGTH_SHORT).show();
        }
    }
}
