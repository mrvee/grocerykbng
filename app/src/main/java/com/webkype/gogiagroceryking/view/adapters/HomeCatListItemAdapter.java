package com.webkype.gogiagroceryking.view.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.home_response.Productdetail;
import com.webkype.gogiagroceryking.model.api_models.home_response.Stripprodetail;
import com.webkype.gogiagroceryking.view.activity.ProductDetailActivity;

import java.util.List;


public class HomeCatListItemAdapter extends RecyclerView.Adapter<HomeCatListItemAdapter.MyViewHolder> {
    public static final int ITEM_CLICK = 100, VALUE_CHANGED = 101;
    private LayoutInflater inflater;
    private Context context;
    List<Stripprodetail> offerModelsList;
    private SmallItemClickListener listener;

    public HomeCatListItemAdapter(Context context, HomeCatListItemAdapter.SmallItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    public void setServices(List<Stripprodetail> imagelist) {
        this.offerModelsList = imagelist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.home_product_cat_list_item, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        final MyViewHolder vh = (MyViewHolder) holder;
        final Stripprodetail data = offerModelsList.get(i);
        vh.tv_item_header.setText(data.getStripname());
        vh.tv_btn_see_all.setVisibility(View.GONE);
        final LinearLayoutManager linearLayoutManager =new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        vh.productList.setLayoutManager(linearLayoutManager);
        vh.productList.setFocusable(false);
        final ProductListBestSellingAdapter bestSellingAdapter = new ProductListBestSellingAdapter(context, new ProductListBestSellingAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Productdetail productdetail, int clickedFor, int position, String activePid) {
                if(clickedFor== ProductListBestSellingAdapter.ITEM_CLICK){
                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra(AppConstants.PID_ID, activePid);
                    context.startActivity(intent);
                }
                if(clickedFor== ProductListBestSellingAdapter.VALUE_CHANGED){
                 listener.onItemClick(null,VALUE_CHANGED);
                }
            }
        });
        bestSellingAdapter.setDataList(data.getProductdetails());
        vh.productList.setAdapter(bestSellingAdapter);
        vh.bind(data, listener);
    }

    @Override
    public int getItemCount() {
        return offerModelsList != null ? offerModelsList.size() : 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_offers, iv_image;
        private TextView tv_btn_see_all, tv_item_header;
        private ConstraintLayout constraint_bg;
        private RecyclerView productList;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_item_header = itemView.findViewById(R.id.tv_item_header);
            tv_btn_see_all = itemView.findViewById(R.id.tv_btn_see_all);
            productList = itemView.findViewById(R.id.rv_selling_list);
        }

        public void bind(final Object object, final SmallItemClickListener listener) {
            tv_btn_see_all.setOnClickListener(v -> {
                listener.onItemClick(object, 101);
            });

        }

    }

    public interface SmallItemClickListener {
        void onItemClick(Object model, int clickedFor);
    }

}
