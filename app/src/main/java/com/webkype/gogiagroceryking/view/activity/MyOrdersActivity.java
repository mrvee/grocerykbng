package com.webkype.gogiagroceryking.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.appdb.AppPref;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.controller.utils.AppConstants;
import com.webkype.gogiagroceryking.model.api_models.orderHist.CustomerOrder;
import com.webkype.gogiagroceryking.model.api_models.orderHist.OrderDetail;
import com.webkype.gogiagroceryking.view.adapters.MyOredersAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrdersActivity extends AppCompatActivity {
    private static final String TAG = MyOrdersActivity.class.getName();
    @BindView(R.id.iv_back)
    ImageView back;
    @BindView(R.id.rv_my_orders)
    RecyclerView rvMyOreders;

    @BindView(R.id.ll_message)
    LinearLayout ll_message;
    @BindView(R.id.tv_login_btn)
    TextView tv_login_btn;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private MyOredersAdapter myOrdersAdapter;

    private Context context;
    private String from = "";
    private String userID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        ButterKnife.bind(this);
        context = this;
        userID = AppPref.instance(context).getValue(AppConstants.USER_ID);

        if (getIntent() != null) {
            from = getIntent().getStringExtra("from");
        }
        myOrdersAdapter = new MyOredersAdapter(context, new MyOredersAdapter.SmallItemClickListener() {
            @Override
            public void onItemClick(OrderDetail model, int clickedFor) {
                if (clickedFor == MyOredersAdapter.CANCELORDER) {
                    Intent orderdetailIntent = new Intent(context, OrderDetailsActivity.class);
                    orderdetailIntent.putExtra(AppConstants.ORDER_ID, model.getOrderid());
                    startActivity(orderdetailIntent);
                }
            }
        });

        back.setOnClickListener(v -> {
            onBackPressed();
        });
        rvMyOreders.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rvMyOreders.setAdapter(myOrdersAdapter);

        if (userID.isEmpty()) {
            ll_message.setVisibility(View.VISIBLE);
        } else {
            ll_message.setVisibility(View.GONE);
            getCartQntty(userID);
        }
        tv_login_btn.setOnClickListener(v -> {
            from = "login";
            Intent orderdetailIntent = new Intent(context, LoginActivity.class);
            startActivity(orderdetailIntent);
            onBackPressed();
        });


    }

    @Override
    public void onBackPressed() {

        if (from == null) {
            super.onBackPressed();
        } else if (from.equals("thanks")) {
            finish();
            startActivity(new Intent(context, HomeActivity.class));
        } else if (from.equals("login")) {
            this.finishAffinity();
        }
    }

    private void getCartQntty(String userId) {
        progressBar.setVisibility(View.VISIBLE);
        if (InternetConnectionCheck.haveNetworkConnection(context)) {
            Call<CustomerOrder> ProductDetailResponseCall = RestManager.instanceOf().getOrderHist(userId);
            ProductDetailResponseCall.enqueue(new Callback<CustomerOrder>() {
                @Override
                public void onResponse(Call<CustomerOrder> call, Response<CustomerOrder> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response != null && response.body().getStatus().equals("200")) {
                        myOrdersAdapter.setServices(response.body().getOrderdetails());

                    } else {

                    }
                }

                @Override
                public void onFailure(Call<CustomerOrder> call, Throwable t) {
                    Log.d(TAG, t.getMessage());
                    progressBar.setVisibility(View.GONE);

                }
            });
        } else {
            Toast.makeText(context, "No network", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);

        }
    }
}
