package com.webkype.gogiagroceryking.view.activity.new_design;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.webkype.gogiagroceryking.R;
import com.webkype.gogiagroceryking.controller.network.Api;
import com.webkype.gogiagroceryking.controller.network.InternetConnectionCheck;
import com.webkype.gogiagroceryking.controller.network.RestManager;
import com.webkype.gogiagroceryking.model.api_models.SignUp;
import com.webkype.gogiagroceryking.view.activity.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassActivity extends AppCompatActivity {

    @BindView(R.id.backForgot)
    ImageView backForgot;
    @BindView(R.id.enterMailForgot_et)
    EditText enterMailForgot_et;
    @BindView(R.id.submitForgot_btn)
    Button submitForgot_btn;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        ButterKnife.bind(this);

        backForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        submitForgot_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                if (enterMailForgot_et.getText().toString().equals("")){
                    enterMailForgot_et.requestFocus();
                    Toast.makeText(ForgotPassActivity.this, "Please enter registered email address", Toast.LENGTH_SHORT).show();
                } else {
                    String email = enterMailForgot_et.getText().toString().trim();
                    forgotPassApi(email);
                }
            }
        });
    }
    
    private void forgotPassApi(String email){
        if (InternetConnectionCheck.haveNetworkConnection(this)){
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
            Api api = RestManager.instanceOf();
            Call<SignUp> call = api.forgotPass(email);
            call.enqueue(new Callback<SignUp>() {
                @Override
                public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                    progressDialog.dismiss();
                    SignUp signUp = response.body();
                    if (signUp.getStatus().equals("200")){
                        enterMailForgot_et.setText("");
                        startActivity(new Intent(ForgotPassActivity.this, LoginActivity.class));
                        finish();
                        Toast.makeText(ForgotPassActivity.this, "Password is successfully sent to your registered email address. Kindly check your registered email inbox or spam folder.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ForgotPassActivity.this, "Please enter registered email address", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SignUp> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(ForgotPassActivity.this, "Unable to reach servers. Try Again !", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(this, "Please check your network connection...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
